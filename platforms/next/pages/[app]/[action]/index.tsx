/**
 * File: /pages/[app]/[action]/index.tsx
 * Project: @platform/next
 * File Created: 02-02-2024 15:53:19
 * Author: Lalit rajak
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export { getServerSideProps } from 'multiplatform.one/next';
import { AppActionScreen } from 'app/screens/app/Action';

export default AppActionScreen;
