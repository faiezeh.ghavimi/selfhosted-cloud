#!/bin/sh

docker swarm init --advertise-addr $(ip route get 8.8.8.8 | grep -oP 'src \K\S+')
docker network create --driver overlay private
docker network create --driver overlay public
