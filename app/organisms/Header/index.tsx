/**
 * File: /organisms/Header/index.tsx
 * Project: app
 * File Created: 22-01-2024 10:16:58
 * Author: Lavanya Katari
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Profile } from 'app/components/Profile';
import React from 'react';
import { XStack, Paragraph } from 'ui';

export const Header = () => {
  return (
    <XStack jc="flex-end" backgroundColor="$gray7Light" ai="center" space width="100%" height={40}>
      <XStack flexWrap="nowrap" flexShrink={1} ai="center" jc="flex-end">
        <Paragraph>@admin</Paragraph>
        <Profile names={['Admin']} />
      </XStack>
    </XStack>
  );
};
