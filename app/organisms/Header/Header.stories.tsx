/**
 * File: /organisms/Header/Header.stories.tsx
 * Project: app
 * File Created: 22-01-2024 10:16:58
 * Author: Lavanya Katari
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Header } from './index';
import React from 'react';

export default {
  title: 'organisms/Header',
  component: Header,
  parameters: {
    status: {
      type: 'beta',
    },
  },
};

export const main = (args) => <Header {...args} />;
