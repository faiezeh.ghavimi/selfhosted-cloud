/**
 * File: /components/SimpleTabs/index.tsx
 * Project: app
 * File Created: 29-01-2024 12:56:08
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import type { TabsContentProps, TabsProps, TabsListProps } from 'ui';
import { Separator, Tabs, YStack, isWeb } from 'ui';

type SimpleTabsProps = TabsProps & {
  tabs: React.ReactNode[];
  orientation?: 'horizontal' | 'vertical';
  tabStyle?: TabsListProps;
  contentStyle?: TabsContentProps;
};

export function SimpleTabs({ tabs, orientation, tabStyle, children, ...props }: SimpleTabsProps) {
  const orientationView = orientation || 'horizontal';

  const tabsList = tabs && tabs.length > 0 ? tabs : [];

  return (
    <YStack
      flex={1}
      ai="stretch"
      jc="center"
      paddingHorizontal="$4"
      {...(isWeb && {
        position: 'unset' as any,
      })}
    >
      <Tabs
        orientation={orientationView}
        flexDirection={orientationView === 'horizontal' ? 'column' : 'row'}
        width="100%"
        height="auto"
        overflow="hidden"
        borderColor="$borderColor"
        {...props}
        jc="center"
        ai="center"
      >
        <Tabs.List
          disablePassBorderRadius="bottom"
          aria-label="Manage your account"
          {...tabStyle}
          separator={<Separator vertical={orientationView === 'vertical'} />}
        >
          {tabsList.map((tab, index) => (
            <React.Fragment key={index}>{tab}</React.Fragment>
          ))}
        </Tabs.List>
        <Separator vertical={orientationView === 'vertical'} />
        {children}
      </Tabs>
    </YStack>
  );
}

export const TabsContent = (props: TabsContentProps) => {
  return (
    <Tabs.Content
      backgroundColor="$background"
      padding="$2"
      alignItems="center"
      justifyContent="center"
      flex={1}
      {...props}
    >
      {props.children}
    </Tabs.Content>
  );
};
