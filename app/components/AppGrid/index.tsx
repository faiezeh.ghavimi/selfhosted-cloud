/**
 * File: /components/AppGrid/index.tsx
 * Project: app
 * File Created: 22-01-2024 12:59:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { useState } from 'react';
import { YStack, XStack, SimpleImage, H5 } from 'ui';
import { AppSheet } from 'app/components/AppSheet';
import { useRouter } from 'solito/router';

export interface AppGridProps {
  logo: string;
  name: string;
  // style?: React.CSSProperties;
}

export const AppGrid = ({ name, logo, ...props }: AppGridProps) => {
  const [sheetVisible, setSheetVisible] = useState(false);
  const router = useRouter();

  const handleRightClick = (event) => {
    if (event.button === 2) {
      event.preventDefault();
      setSheetVisible(true);
    }
  };

  const handleAppAction = (action: string) => {
    return router.push(`/${name}/${action}`);
  };

  return (
    <XStack m={10} p={10} ai="center" height={120} width={120} jc="center" {...props}>
      <YStack
        flexWrap="wrap"
        jc="center"
        ai="center"
        // @ts-ignore
        onContextMenu={handleRightClick}
      >
        <SimpleImage src={logo} alt={name} height={70} width={70} />
        <H5 color="red" overflow="visible" mt="$3">
          {name}
        </H5>
      </YStack>
      {sheetVisible && <AppSheet setVisible={setSheetVisible} onAction={handleAppAction} />}
    </XStack>
  );
};
