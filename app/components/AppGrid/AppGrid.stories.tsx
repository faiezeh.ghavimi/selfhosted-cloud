/**
 * File: /components/AppGrid/AppGrid.stories.tsx
 * Project: app
 * File Created: 22-01-2024 15:16:51
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { AppGrid } from './index';
import { YStack } from 'ui';

export default {
  title: 'components/AppGrid',
  component: AppGrid,
};

export const main = (args) => {
  return (
    <YStack height={200} width={600}>
      <AppGrid {...args} />
    </YStack>
  );
};

main.args = {
  name: 'AppCardGrid2',
  description: 'AppCardGrid',
  logo: require('app/assets/app2.png'),
};
