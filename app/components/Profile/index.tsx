/**
 * File: /components/Profile/index.tsx
 * Project: app
 * File Created: 23-01-2024 10:10:46
 * Author: Lavanya Katari
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, { useState } from 'react';
import { YStack, Button, Paragraph, H3, SimplePopover, Separator } from 'ui';
import { CircleUser } from '@tamagui/lucide-icons';

export interface ProfileProps {
  names: string[] | undefined;
}

export const Profile = ({ names }: ProfileProps) => {
  const [showCard, setShowCard] = useState(false);
  const [isLogin, setIsLogin] = React.useState(false);

  const handleButtonClick = () => {
    setShowCard(!showCard);
  };

  return (
    <YStack>
      <SimplePopover
        trigger={
          <Button
            height={40}
            hoverStyle={{
              backgroundColor: '$gray10Dark',
            }}
            focusStyle={{
              backgroundColor: '$gray10Dark',
              outlineColor: '$gray10Dark',
            }}
            backgroundColor="$gray10Dark"
            borderRadius={40}
            size={40}
            icon={<CircleUser />}
            onClick={handleButtonClick}
          />
        }
        contentStyle={{ width: 300, p: 40, height: 390 }}
      >
        {names?.map((name, index) => (
          <YStack
            key={index}
            animation="quick"
            enterStyle={{ y: -200 }}
            width="100%"
            ai="center"
            jc="space-between"
            space
            $md={{ flexDirection: 'column', height: 'auto' }}
            flexShrink={1}
            flexWrap="nowrap"
          >
            <H3 color="black">{name}</H3>
            <Separator width="85%" borderColor="yellow" borderBottomWidth="$1.5" />
            <YStack space padding="$3">
              <Paragraph fontWeight="bold" color="black">
                Profile
              </Paragraph>
              <Paragraph fontWeight="bold" color="black">
                Settings
              </Paragraph>
              <Paragraph fontWeight="bold" color="black">
                Preferences
              </Paragraph>
            </YStack>
            <Separator width="85%" borderColor="yellow" borderBottomWidth="$1.5" />
            <Button
              backgroundColor="$gray8Light"
              color="black"
              width={100}
              height={40}
              onPress={() => setIsLogin(!isLogin)}
            >
              {isLogin ? 'Logout' : 'Login'}
            </Button>
          </YStack>
        ))}
      </SimplePopover>
    </YStack>
  );
};
