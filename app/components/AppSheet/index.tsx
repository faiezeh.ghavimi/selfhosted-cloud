/**
 * File: /components/AppSheet/index.tsx
 * Project: app
 * File Created: 02-02-2024 11:02:54
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { useState } from 'react';
import { Sheet, Tabs, YStack, SizableText } from 'ui';
import { SimpleTabs } from 'app/components/SimpleTabs';
import { AppUninstall } from '../AppUninstall';

export interface AppSheetProps {
  setVisible: (visible: boolean) => void;
  onAction: (action: string) => void;
}

export const AppSheet = ({ setVisible, onAction }: AppSheetProps) => {
  const [position, setPosition] = useState(0);

  const tabsData = ['Config', 'Users', 'Backups', 'Advanced', 'Start', 'Stop', 'Uninstall'];
  const tabs = ['Uninstall'];

  const horizontalTabs = () => (
    <>
      {tabsData.map((tabName, index) => (
        <Tabs.Tab key={index} value={`tab${index + 1}`} onPress={() => onAction(tabName)} minWidth="$15">
          <SizableText fontFamily="$body">{tabName}</SizableText>
        </Tabs.Tab>
      ))}
    </>
  );

  const renderTabContent = (tabName: string) => {
    if (tabName === 'Uninstall') {
      return (
        <>
          <AppUninstall setVisible={setVisible} name="YourAppName1" servers={[]} appName={undefined} />

          {/* Add more instances as needed */}
        </>
      );
    }

    return <div>{tabName} content</div>;
  };

  return (
    <Sheet
      modal
      open={true}
      onOpenChange={setVisible}
      snapPoints={[40]}
      position={position}
      onPositionChange={setPosition}
      dismissOnSnapToBottom
    >
      <Sheet.Overlay />
      <Sheet.Frame ai="center" jc="center" backgroundColor="$gray3Light">
        <Sheet.Handle />
        <YStack ai="center" jc="center" space>
          <SimpleTabs tabs={[horizontalTabs()]} />
          {tabs.map((tabName, index) => (
            <div key={index} id={tabName}>
              {renderTabContent(tabName)}
            </div>
          ))}
        </YStack>
      </Sheet.Frame>
    </Sheet>
  );
};
