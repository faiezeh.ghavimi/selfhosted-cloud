/**
 * File: /components/AppUninstall/index.tsx
 * Project: app
 * File Created: 12-02-2024 03:08:40
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ArrowDown } from '@tamagui/lucide-icons';
import React from 'react';
import { Link } from 'solito/link';
import { Button, YStack, SelectSimple, Select, XGroup } from 'ui';

export interface InstallButtonProps {
  servers: string[];
  appName: string | undefined;

  setVisible: (visible: boolean) => void;

  name: string;
}

export const AppUninstall = ({ servers, appName }: InstallButtonProps) => {
  const [toggleServerList, setToggleServerList] = React.useState(false);
  const [selectedServer, setSelectedServer] = React.useState(servers[0]);
  return (
    <YStack alignItems="flex-end">
      <XGroup>
        <XGroup.Item>
          <Link href={`/apps/${appName}/install`}>
            <Button
              borderStyle="solid"
              borderWidth={0}
              borderRightWidth={2}
              borderColor="black"
              borderTopRightRadius={0}
              borderBottomRightRadius={0}
              color="white"
              backgroundColor="$blue9Light"
            >
              UnInstall
            </Button>
          </Link>
        </XGroup.Item>
        <XGroup.Item>
          <Button
            onPress={() => setToggleServerList(!toggleServerList)}
            color="white"
            backgroundColor="$blue9Light"
            icon={<ArrowDown />}
          />
        </XGroup.Item>
      </XGroup>

      <SelectSimple
        placeholder="Select Server"
        onValueChange={(value) => selectedServer !== value && setSelectedServer(value)}
        display={toggleServerList ? 'flex' : 'none'}
        style={{ position: 'absolute', top: 50, zIndex: 100 }}
      >
        {servers.map((server, i) => (
          <Select.Item key={i} index={i} value={server}>
            <Select.ItemText>{server}</Select.ItemText>
          </Select.Item>
        ))}
      </SelectSimple>
    </YStack>
  );
};
