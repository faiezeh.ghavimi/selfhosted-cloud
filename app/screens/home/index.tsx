/**
 * File: /screens/home/index.tsx
 * Project: app
 * File Created: 10-10-2023 06:39:34
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { YStack, XStack } from 'ui';
import { withAuthenticated } from '@multiplatform.one/keycloak';
import { withDefaultLayout } from 'app/layouts/Default';
import { AppGrid } from 'app/components/AppGrid';
import { Header } from 'app/organisms/Header';
import { SystemMetrics } from 'app/components/SystemMetrics';
import { gql } from 'gql';

gql(`
  query {
    prismaUsers {
      id
    }
  }
`);

const apps = [
  { logo: require('app/assets/app1.png'), name: 'QuickChat' },
  { logo: require('app/assets/app2.png'), name: 'SnapTask' },
  { logo: require('app/assets/app3.png'), name: 'GrooveBox' },
  { logo: require('app/assets/app4.png'), name: 'PicPalette' },
  { logo: require('app/assets/app5.png'), name: 'FitTrack' },
  { logo: require('app/assets/app6.png'), name: 'AgendaSync' },
  { logo: require('app/assets/app7.png'), name: 'NoteNest' },
  { logo: require('app/assets/app1extra.png'), name: 'FlowFocus' },
  { logo: require('app/assets/app9.png'), name: 'MindMapper' },
  { logo: require('app/assets/app10.png'), name: 'CloudFile' },
  { logo: require('app/assets/app11.png'), name: 'LearnLang' },
  { logo: require('app/assets/app12.png'), name: 'History360' },
  { logo: require('app/assets/app13.png'), name: 'MathMaster' },
  { logo: require('app/assets/app14.png'), name: 'sciSpace' },
  { logo: require('app/assets/app15.png'), name: 'ArtisticMind' },
  { logo: require('app/assets/app16.png'), name: 'RunnerRush' },
  { logo: require('app/assets/app17.png'), name: 'PuzzleQuest' },
  { logo: require('app/assets/app18.png'), name: 'CityBuilder' },
  { logo: require('app/assets/app19.png'), name: 'CardClash' },
  { logo: require('app/assets/app20.png'), name: 'LinkUp' },
  { logo: require('app/assets/app21.png'), name: 'MeetMe' },
  { logo: require('app/assets/app22.png'), name: 'AlumniConnect' },
  { logo: require('app/assets/app23.png'), name: 'TradeCircle' },
  { logo: require('app/assets/app24.png'), name: 'CoderHub' },
  { logo: require('app/assets/app25.png'), name: 'CodeMerge' },
  { logo: require('app/assets/app26.png'), name: 'BugSquash' },
  { logo: require('app/assets/app27.png'), name: 'DeployFast' },
  { logo: require('app/assets/app28.png'), name: 'WidgetWorks' },
  { logo: require('app/assets/app29.png'), name: 'APIBuilder' },
  { logo: require('app/assets/app30.png'), name: 'QuickClean' },
  { logo: require('app/assets/app31.png'), name: 'EncryptIt' },
  { logo: require('app/assets/app32.png'), name: 'SafeBrowse' },
  { logo: require('app/assets/app33.png'), name: 'PasswordVault' },
  { logo: require('app/assets/app34.png'), name: 'SpaceSaver' },
  { logo: require('app/assets/app35.png'), name: 'DriverGenius' },
  { logo: require('app/assets/app36.png'), name: 'SystemScan' },
];

function HomeScreen() {
  return (
    <YStack backgroundColor="backgroundStrong">
      <SystemMetrics />

      <Header />
      <XStack flexWrap="wrap">
        {apps.map((app, index) => (
          <AppGrid key={index} logo={app.logo} name={app.name} />
        ))}
      </XStack>
    </YStack>
  );
}
export default withAuthenticated(withDefaultLayout(HomeScreen));
