/**
 * File: /screens/app/Action/index.tsx
 * Project: app
 * File Created: 02-02-2024 16:02:54
 * Author: Lalit rajak
 * -----
 * BitSpur (c) Copyright 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { createParam } from 'solito';
import { useRouter } from 'solito/router';
import { YStack, Text, Button } from 'tamagui';

const { useParam } = createParam<{ app: string; action: string }>();

export function AppActionScreen() {
  const [app] = useParam('app');
  const [action] = useParam('action');
  const router = useRouter();

  return (
    <YStack fullscreen ai="center" jc="center" m="$10" space>
      <Text fontSize="$10" fontFamily="$silkscreen">
        App : {app}, Action : {action}
      </Text>
      <Button onPress={() => router.push('/')}>Go to Home</Button>
    </YStack>
  );
}
