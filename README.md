# selfhosted-cloud

> your cloud operating system

## Concepts

- **App** - an application deployed on the server
- **Deployment** - a version of an app (docker swarm stack)
- **Package** - an app packaged for deployment

## File Structure

```
/var/
├── runtime/
│   └── <DEPLOYMENT>/
│       └── default/ - Stores runtime data
├── config/
│   └── <DEPLOYMENT>/ - Contains deployment configuration, essentially a duplicate of the installed chart
│       └── environments/
│           └── default/ - The standard environment holding the templated chart
```

## Notes

- Do not directly log `ctx` to the console or the application will freeze. (e.g. `console.log(ctx)`).
