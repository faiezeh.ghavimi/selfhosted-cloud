/*
  Warnings:

  - Added the required column `serviceName` to the `PublishedPort` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "PublishedPort" ADD COLUMN     "serviceName" TEXT NOT NULL;
