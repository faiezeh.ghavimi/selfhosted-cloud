/*
 *  File: /docker/swarm/resolver.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DOCKER_EVENTS, DockerEvent } from '../events';
import {
  DockerSwarmInitArgs,
  DockerSwarmInitResult,
  DockerSwarmLeaveResult,
  DockerSwarmLeaveArgs,
  DockerSwarmJoinArgs,
  DockerSwarmJoinResult,
  DockerSwarmUnLockResult,
  DockerSwarmUnlockArgs,
  DockerSwarmUpdateArgs,
  DockerSwarmUpdateResult,
  DockerSwarmJoinTokenResult,
  DockerSwarmJoinTokenArgs,
  DockerSwarmCaArgs,
  DockerSwarmCaResult,
  DockerSwarmUnlockKeyArgs,
  DockerSwarmUnLockKeyResult,
} from './dto';
import { DockerSwarmService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Mutation, Resolver, Args, Subscription, Root } from 'type-graphql';

@Authorized()
@Injectable()
@Resolver()
export class DockerSwarmResolver {
  constructor(private readonly dockerSwarm: DockerSwarmService) {}

  @Mutation(() => DockerSwarmInitResult)
  async dockerSwarmInit(@Args(() => DockerSwarmInitArgs) args?: DockerSwarmInitArgs) {
    return this.dockerSwarm.init(args);
  }

  @Mutation(() => DockerSwarmLeaveResult)
  async dockerSwarmLeave(@Args(() => DockerSwarmLeaveArgs) args?: DockerSwarmLeaveArgs) {
    return this.dockerSwarm.leave(args);
  }

  @Mutation(() => DockerSwarmJoinResult)
  async dockerSwarmJoin(@Args(() => DockerSwarmJoinArgs) args?: DockerSwarmJoinArgs) {
    return this.dockerSwarm.join(args);
  }

  @Mutation(() => DockerSwarmUnLockResult)
  async dockerSwarmUnlock(@Args(() => DockerSwarmUnlockArgs) args: DockerSwarmUnlockArgs) {
    return this.dockerSwarm.unlock(args);
  }

  @Mutation(() => DockerSwarmUnLockKeyResult)
  async dockerSwarmUnlockKey(@Args(() => DockerSwarmUnlockKeyArgs) args: DockerSwarmUnlockKeyArgs) {
    return this.dockerSwarm.unlockKey(args);
  }

  @Mutation(() => DockerSwarmUpdateResult)
  async dockerSwarmUpdate(@Args(() => DockerSwarmUpdateArgs) args?: DockerSwarmUpdateArgs) {
    return this.dockerSwarm.update(args);
  }

  @Mutation(() => DockerSwarmJoinTokenResult)
  async dockerSwarmJoinToken(@Args(() => DockerSwarmJoinTokenArgs) args?: DockerSwarmJoinTokenArgs) {
    return this.dockerSwarm.joinToken(args);
  }

  @Mutation(() => DockerSwarmCaResult)
  async dockerSwarmCa(@Args(() => DockerSwarmCaArgs) args?: DockerSwarmCaArgs) {
    return this.dockerSwarm.Ca(args);
  }

  @Subscription(() => DockerEvent, {
    topics: DOCKER_EVENTS,
    filter: ({ payload }: { payload: DockerEvent }) => payload.scope === 'swarm',
  })
  async dockerSwarmEvents(@Root() payload: DockerEvent) {
    return payload;
  }
}
