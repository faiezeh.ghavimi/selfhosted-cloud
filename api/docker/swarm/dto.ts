/*
 *  File: /docker/swarm/dto.ts
 *  Project: api
 *  File Created: 18-01-2024 10:37:21
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Field, ArgsType, Int, ObjectType } from 'type-graphql';
import { Flag } from '../types';

@ArgsType()
export class DockerSwarmInitArgs {
  @Field(() => String, { nullable: true, description: 'advertised address (format: "<ip|interface>[:port]")' })
  advertiseAddr?: string;

  @Field(() => Boolean, {
    nullable: true,
    description: 'enable manager autolocking (requiring an unlock key to start a stopped manager)',
  })
  autolock?: boolean;

  @Field(() => String, { nullable: true, description: 'availability of the node ("active", "pause", "drain")' })
  availability?: string;

  @Field(() => String, { nullable: true, description: 'validity period for node certificates (ns|us|ms|s|m|h)' })
  certExpiry?: string;

  @Field(() => String, {
    nullable: true,
    description: 'address or interface to use for data path traffic (format: "<ip|interface>")',
  })
  dataPathAddr?: string;

  @Field(() => Int, { nullable: true, description: 'port number to use for data path traffic (1024 - 49151)' })
  dataPathPort?: number;

  @Field(() => String, { nullable: true, description: 'default address pool in cidr format' })
  defaultAddrPool?: string;

  @Field(() => Int, { nullable: true, description: 'default address pool subnet mask length' })
  defaultAddrPoolMaskLength?: number;

  @Field(() => String, { nullable: true, description: 'dispatcher heartbeat period (ns|us|ms|s|m|h)' })
  dispatcherHeartbeat?: string;

  @Field(() => String, { nullable: true, description: 'specifications of one or more certificate signing endpoints' })
  externalCa?: string;

  @Field(() => Boolean, { nullable: true, description: 'force create a new cluster from current state' })
  forceNewCluster?: boolean;

  @Field(() => String, { nullable: true, description: 'listen address (format: "<ip|interface>[:port]")' })
  listenAddr?: string;

  @Field(() => Int, { nullable: true, description: 'number of additional raft snapshots to retain' })
  maxSnapshots?: number;

  @Field(() => Int, { nullable: true, description: 'number of log entries between raft snapshots' })
  snapshotInterval?: number;

  @Field(() => Int, { nullable: true, description: 'task history retention limit' })
  taskHistoryLimit?: number;
}

@ObjectType()
export class DockerSwarmInitResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => String, { nullable: true })
  token?: string;

  @Field(() => String, { nullable: true })
  node?: string;

  @Field(() => String, { nullable: true })
  ip?: string;
}

@ArgsType()
export class DockerSwarmLeaveArgs {
  @Field(() => Boolean, { nullable: true })
  force?: boolean;
}

@ArgsType()
export class DockerSwarmUnlockArgs {
  @Field(() => String, { nullable: true, description: 'unlock key for swarm' })
  unlock?: string;
}

@ObjectType()
export class DockerSwarmUnLockResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerSwarmUnlockKeyArgs {
  @Field(() => String, { nullable: true, description: 'unlock key for swarm' })
  unlockKey?: string;
}

@ObjectType()
export class DockerSwarmUnLockKeyResult {
  @Field(() => String, { nullable: true, description: 'stdout of the unlock key result' })
  stdout?: string;

  @Field(() => String, { nullable: true, description: 'stderr of the unlock key result' })
  stderr?: string;
}

@ObjectType()
export class DockerSwarmLeaveResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerSwarmUpdateArgs {
  @Field(() => Boolean, { nullable: true, description: 'enable or disable auto-locking of swarm manager' })
  autolock?: boolean;

  @Field(() => String, {
    nullable: true,
    description: 'duration until swarm certificates expire (default "2160h0m0s")',
  })
  certExpiry?: string;

  @Field(() => String, { nullable: true, description: 'heartbeat period for dispatcher (default "5s")' })
  dispatcherHeartbeat?: string;

  @Field(() => String, { nullable: true, description: 'specifications of one or more certificate signing endpoints' })
  externalCa?: string;

  @Field(() => Int, { nullable: true, description: 'number of additional raft snapshots to retain' })
  maxSnapshots?: number;

  @Field(() => Int, { nullable: true, description: 'number of log entries between raft snapshots' })
  snapshotInterval?: number;

  @Field(() => Int, { nullable: true, description: 'task history retention limit' })
  taskHistoryLimit?: number;
}

@ObjectType()
export class DockerSwarmUpdateResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerSwarmJoinArgs {
  @Field(() => String, { nullable: true, description: 'advertised address (format: "<ip|interface>[:port]")' })
  advertiseAddr?: string;

  @Field(() => String, { nullable: true, description: 'availability of the node ("active", "pause", "drain")' })
  availability?: string;

  @Field(() => String, {
    nullable: true,
    description: 'address or interface to use for data path traffic (format: "<ip|interface>")',
  })
  dataPathAddr?: string;

  @Field(() => String, { nullable: true, description: 'listen address (format: "<ip|interface>[:port]")' })
  listenAddr?: string;

  @Field(() => String, { nullable: true, description: 'token for entry into the swarm' })
  token?: string;

  @Field(() => String, { nullable: true, description: 'ip address of the manager node' })
  ip?: string;
}

@ObjectType()
export class DockerSwarmJoinResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerSwarmJoinTokenArgs {
  @Field(() => Boolean, { nullable: true })
  quiet?: boolean;

  @Field(() => Boolean, { nullable: true })
  rotate?: boolean;
}

@ObjectType()
export class DockerSwarmJoinTokenResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => String, { nullable: true })
  token?: string;
}

@ArgsType()
export class DockerSwarmCaArgs {
  @Field(() => String, {
    nullable: true,
    description: 'path to the PEM-formatted root CA certificate to use for the new cluster',
  })
  caCert?: string;

  @Field(() => String, {
    nullable: true,
    description: 'path to the PEM-formatted root CA key to use for the new cluster',
  })
  caKey?: string;

  @Field(() => String, {
    nullable: true,
    description: 'validity period for node certificates (ns|us|ms|s|m|h) (default 2160h0m0s)',
  })
  certExpiry?: string;

  @Field(() => Boolean, {
    nullable: true,
    description: 'exit immediately instead of waiting for the root rotation to converge',
  })
  detach?: boolean;

  @Field(() => String, { nullable: true, description: 'specifications of one or more certificate signing endpoints' })
  externalCa?: string;

  @Field(() => Boolean, { nullable: true, description: 'suppress progress output' })
  quiet?: boolean;

  @Field(() => Boolean, {
    nullable: true,
    description: 'rotate the swarm CA - if no certificate or key are provided, new ones will be generated',
  })
  rotate?: boolean;
}

@ObjectType()
export class DockerSwarmCaResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}
