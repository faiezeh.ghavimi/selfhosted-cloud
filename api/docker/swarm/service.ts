/*
 *  File: /docker/swarm/service.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import kebabCase from 'lodash.kebabcase';
import {
  DockerSwarmInitArgs,
  DockerSwarmInitResult,
  DockerSwarmJoinArgs,
  DockerSwarmLeaveArgs,
  DockerSwarmUnlockArgs,
  DockerSwarmUpdateArgs,
  DockerSwarmJoinTokenArgs,
  DockerSwarmCaArgs,
  DockerSwarmCaResult,
  DockerSwarmUnlockKeyArgs,
} from './dto';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';

@Injectable()
export class DockerSwarmService {
  constructor(private readonly logger: Logger) {}

  async init(args: DockerSwarmInitArgs = {}): Promise<DockerSwarmInitResult> {
    const result = await this.swarm('init', {
      advertiseAddr: args.advertiseAddr,
      autolock: args.autolock,
      availability: args.availability,
      certExpiry: args.certExpiry,
      dataPathAddr: args.dataPathAddr,
      dataPathPort: args.dataPathPort,
      defaultAddrPool: args.defaultAddrPool,
      defaultAddrPoolMaskLength: args.defaultAddrPoolMaskLength,
      dispatcherHeartbeat: args.dispatcherHeartbeat,
      externalCa: args.externalCa,
      forceNewCluster: args.forceNewCluster,
      listenAddr: args.listenAddr,
      maxSnapshots: args.maxSnapshots,
      snapshotInterval: args.snapshotInterval,
      taskHistoryLimit: args.taskHistoryLimit,
    });
    result.node = result.stdout?.match(/Swarm initialized: current node \(([\w-]+)\) is now a manager/)?.[1];
    result.token = result.stdout?.match(/docker swarm join --token ([\w-]+) ([\w.:-]+)/)?.[1];
    const ip = result.stdout?.match(/(\d+\.\d+\.\d+\.\d+):(\d+)/);
    result.ip = ip;
    return result;
  }

  async join(args: DockerSwarmJoinArgs = {}) {
    const result = await this.swarm(
      'join',
      {
        advertiseAddr: args.advertiseAddr,
        availability: args.availability,
        dataPathAddr: args.dataPathAddr,
        listenAddr: args.listenAddr,
        token: args.token,
      },
      args.ip ? [args.ip] : [],
    );
    result.node = result.stdout?.match(/Swarm initialized: current node \(([\w-]+)\) is now a manager/)?.[1];
    result.token = result.stdout?.match(/docker swarm join --token ([\w-]+) ([\w.:-]+)/)?.[1];
    return result;
  }

  async leave(args: DockerSwarmLeaveArgs = {}) {
    return this.swarm('leave', {
      force: args.force,
    });
  }

  async unlock(args: DockerSwarmUnlockArgs) {
    return this.swarm('unlock', {
      unlock: args.unlock,
    });
  }

  async unlockKey(args: DockerSwarmUnlockKeyArgs = {}) {
    return this.swarm('unlock-key', {
      unlockKey: args.unlockKey,
    });
  }

  async update(args: DockerSwarmUpdateArgs = {}) {
    return this.swarm('update', {
      autolock: args.autolock,
      certExpiry: args.certExpiry,
      dispatcherHeartbeat: args.dispatcherHeartbeat,
      externalCa: args.externalCa,
      taskHistoryLimit: args.taskHistoryLimit,
    });
  }

  async joinToken(args: DockerSwarmJoinTokenArgs = {}) {
    return this.swarm('join-token', {
      quiet: args.quiet,
      rotate: args.rotate,
    });
  }

  async Ca(args: DockerSwarmCaArgs = {}): Promise<DockerSwarmCaResult> {
    return this.swarm('ca', {
      quiet: args.quiet,
      rotate: args.rotate,
      detach: args.detach,
      certExpiry: args.certExpiry,
      externalCa: args.externalCa,
      caCert: args.caCert,
      caKey: args.caKey,
    });
  }

  private async swarm(command: string, options: Record<string, Flag> = {}, args?: string[]): Promise<SwarmResult> {
    try {
      const p = execa('docker', [
        'swarm',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...(args || []),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface SwarmResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
