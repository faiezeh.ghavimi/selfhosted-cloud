/*
 *  File: /docker/events.ts
 *  Project: api
 *  File Created: 19-01-2024 11:25:48
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Logger } from '@multiplatform.one/typegraphql';
import { ObjectType, Field } from 'type-graphql';
import { camelCaseKeys } from './utils';
import { execa } from 'execa';
import { pubSub } from '../pubSub';

export const DOCKER_EVENTS = 'DOCKER_EVENTS';

export function listenDockerEvents(logger: Logger, onEvent?: (event: any) => void) {
  const p = execa('docker', ['events', '--format', '{{json .}}']);
  p.stdout?.on('data', (data) => {
    try {
      const event = camelCaseKeys(JSON.parse(data.toString()));
      if (event) {
        pubSub.publish(DOCKER_EVENTS, event);
        if (onEvent) onEvent(event);
      }
    } catch (err) {
      logger.trace(err);
    }
  });
}

export interface IDockerEvent {
  Action: string;
  Type: string;
  from?: string;
  id?: string;
  scope?: string;
  status?: string;
  time?: number;
  timeNano?: number;
  Actor: {
    ID: string;
    Attributes: {
      'com.docker.compose.config-hash'?: string;
      'com.docker.compose.container-number'?: string;
      'com.docker.compose.depends_on'?: string;
      'com.docker.compose.image'?: string;
      'com.docker.compose.oneoff'?: string;
      'com.docker.compose.project'?: string;
      'com.docker.compose.project.config_files'?: string;
      'com.docker.compose.project.working_dir'?: string;
      'com.docker.compose.service'?: string;
      'com.docker.compose.version'?: string;
      container?: string;
      execDuration?: string;
      exitCode?: string;
      image?: string;
      name?: string;
      signal?: string;
      type?: string;
      [key: string]: string | undefined;
    };
  };
  [key: string]: any;
}

@ObjectType()
export class DockerEventActorAttributes {
  @Field(() => String, { nullable: true })
  comDockerComposeConfigHash?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeContainerNumber?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeDependsOn?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeImage?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeOneoff?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeProject?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeProjectConfigFiles?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeProjectWorkingDir?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeService?: string;

  @Field(() => String, { nullable: true })
  comDockerComposeVersion?: string;

  @Field(() => String, { nullable: true })
  container?: string;

  @Field(() => String, { nullable: true })
  execDuration?: string;

  @Field(() => String, { nullable: true })
  exitCode?: string;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  signal?: string;

  @Field(() => String, { nullable: true })
  type?: string;
}

@ObjectType()
export class DockerEventActor {
  @Field(() => String)
  id: string;

  @Field(() => DockerEventActorAttributes)
  attributes: DockerEventActorAttributes;
}

@ObjectType()
export class DockerEvent {
  @Field(() => String)
  action: string;

  @Field(() => String)
  type: string;

  @Field(() => String, { nullable: true })
  from?: string;

  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  scope?: string;

  @Field(() => String, { nullable: true })
  status?: string;

  @Field(() => Number, { nullable: true })
  time?: number;

  @Field(() => Number, { nullable: true })
  timeNano?: number;

  @Field(() => DockerEventActor)
  actor: DockerEventActor;
}
