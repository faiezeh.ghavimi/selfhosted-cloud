/*
 *  File: /docker/image/dto.ts
 *  Project: api
 *  File Created: 18-01-2024 10:37:21
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Field, ArgsType, Int, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerImageLsArgs {
  @Field(() => Boolean, { nullable: true, description: 'Show all images (default hides intermediate images)' })
  all?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Show digests' })
  digests?: boolean;

  @Field(() => String, { nullable: true, description: 'Filter output based on conditions provided' })
  filter?: string;

  @Field(() => Boolean, { nullable: true, description: "Don't truncate output" })
  noTrunc?: boolean;
}

@ObjectType()
export class DockerImageLsResult {
  @Field(() => String, { nullable: true })
  containers?: string;

  @Field(() => String, { nullable: true })
  createdAt?: string;

  @Field(() => String, { nullable: true })
  createdSince?: string;

  @Field(() => String, { nullable: true })
  digest?: string;

  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  repository?: string;

  @Field(() => String, { nullable: true })
  sharedSize?: string;

  @Field(() => String, { nullable: true })
  size?: string;

  @Field(() => String, { nullable: true })
  tag?: string;

  @Field(() => String, { nullable: true })
  uniqueSize?: string;

  @Field(() => String, { nullable: true })
  virtualSize?: string;
}

@ArgsType()
export class DockerImagePruneArgs {
  @Field(() => Boolean, { nullable: true, description: 'Do not delete untagged parents' })
  all?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Do not prompt for confirmation' })
  force?: boolean;

  @Field(() => Int, { nullable: true, description: 'Set the minimum unused period (in seconds) before a prune' })
  pruneFilter?: number;
}

@ObjectType()
export class DockerImagePruneResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}
