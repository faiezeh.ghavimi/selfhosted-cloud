/*
 *  File: /docker/image/types.ts
 *  Project: api
 *  File Created: 30-01-2024 16:34:24
 *  Author: HariKrishna
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export interface IDockerImageLs {
  Containers?: string;
  CreatedAt?: string;
  CreatedSince?: string;
  Digest?: string;
  ID?: string;
  Repository?: string;
  SharedSize?: string;
  Size?: string;
  Tag?: string;
  UniqueSize?: string;
  VirtualSize?: string;
}

export interface IDockerImageInspect {
  Id: string;
  RepoTags: string[];
  RepoDigests: string[];
  Parent: string;
  Comment: string;
  Created: string;
  Container: string;
  ContainerConfig: {
    Hostname: string;
    Domainname: string;
    User: string;
    AttachStdin: boolean;
    AttachStdout: boolean;
    AttachStderr: boolean;
    Tty: boolean;
    OpenStdin: boolean;
    StdinOnce: boolean;
    Env: string[] | null;
    Cmd: string[] | null;
    Image: string;
    Volumes: any | null;
    WorkingDir: string;
    Entrypoint: string[] | null;
    OnBuild: any | null;
    Labels: any | null;
  };
  DockerVersion: string;
  Author: string;
  Config: {
    Hostname: string;
    Domainname: string;
    User: string;
    AttachStdin: boolean;
    AttachStdout: boolean;
    AttachStderr: boolean;
    ExposedPorts: {
      [key: string]: any;
    };
    Tty: boolean;
    OpenStdin: boolean;
    StdinOnce: boolean;
    Env: string[];
    Cmd: string[];
    ArgsEscaped: boolean;
    Image: string;
    Volumes: {
      [key: string]: any;
    };
    WorkingDir: string;
    Entrypoint: string[];
    OnBuild: any | null;
    Labels: any | null;
  };
  Architecture: string;
  Os: string;
  Size: number;
  GraphDriver: {
    Data: {
      LowerDir: string;
      MergedDir: string;
      UpperDir: string;
      WorkDir: string;
    };
    Name: string;
  };
  RootFS: {
    Type: string;
    Layers: string[];
  };
  Metadata: {
    LastTagTime: string;
  };
}
