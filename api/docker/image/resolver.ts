/*
 *  File: /docker/image/resolver.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerImageService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Mutation, Resolver, Args, Query } from 'type-graphql';
import {
  DockerImagesResult,
  DockerRmResult,
  DockerHistoryResult,
  DockerRmArgs,
  DockerHistoryArgs,
  DockerPullArgs,
  DockerPullResult,
  DockerPushArgs,
  DockerPushResult,
  DockerTagArgs,
  DockerTagResult,
  DockerLoadArgs,
  DockerLoadResult,
  DockerBuildArgs,
  DockerBuildResult,
  DockerImportArgs,
  DockerImportResult,
  DockerInspectResult,
  DockerInspectArgs,
  DockerSaveArgs,
  DockerSaveResult,
} from '../dto';
import { DockerImageLsArgs, DockerImageLsResult, DockerImagePruneResult, DockerImagePruneArgs } from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerImageResolver {
  constructor(private readonly dockerImage: DockerImageService) {}

  @Query(() => [DockerImageLsResult])
  async dockerImageLs(@Args(() => DockerImageLsArgs) args: DockerImageLsArgs) {
    return this.dockerImage.ls(args);
  }

  @Mutation(() => DockerRmResult)
  async dockerImageRm(@Args(() => DockerRmArgs) args: DockerRmArgs) {
    return this.dockerImage.rm(args);
  }

  @Query(() => [DockerHistoryResult])
  async dockerImageHistory(@Args(() => DockerHistoryArgs) args: DockerHistoryArgs) {
    return this.dockerImage.history(args);
  }

  @Mutation(() => DockerPullResult)
  async dockerImagePull(@Args(() => DockerPullArgs) args: DockerPullArgs) {
    return this.dockerImage.pull(args);
  }

  @Mutation(() => DockerPushResult)
  async dockerImagePush(@Args(() => DockerPushArgs) args: DockerPushArgs) {
    return this.dockerImage.push(args);
  }

  @Mutation(() => DockerTagResult)
  async dockerImageTag(@Args(() => DockerTagArgs) args: DockerTagArgs) {
    return this.dockerImage.tag(args);
  }

  @Mutation(() => DockerLoadResult)
  async dockerImageLoad(@Args(() => DockerLoadArgs) args: DockerLoadArgs) {
    return this.dockerImage.load(args);
  }

  @Mutation(() => DockerBuildResult)
  async dockerImageBuild(@Args(() => DockerBuildArgs) args: DockerBuildArgs) {
    return this.dockerImage.build(args);
  }

  @Mutation(() => DockerImportResult)
  async dockerImageImport(@Args(() => DockerImportArgs) args: DockerImportArgs) {
    return this.dockerImage.import(args);
  }

  @Query(() => DockerInspectResult)
  async dockerImageInspect(@Args(() => DockerInspectArgs) args: DockerInspectArgs) {
    return this.dockerImage.inspect(args);
  }

  @Mutation(() => DockerSaveResult)
  async dockerImageSave(@Args(() => DockerSaveArgs) args: DockerSaveArgs) {
    return this.dockerImage.save(args);
  }

  @Mutation(() => DockerImagePruneResult)
  async dockerImagePrune(@Args(() => DockerImagePruneArgs) args: DockerImagePruneArgs) {
    return this.dockerImage.prune(args);
  }
}
