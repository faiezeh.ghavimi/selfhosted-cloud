/*
 *  File: /docker/container/dto.ts
 *  Project: api
 *  File Created: 01-02-2024 17:54:30
 *  Author: Lalit rajak
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerContainerLsArgs {
  @Field(() => Boolean, {
    nullable: true,
    description: 'Show all containers. Only running containers are shown by default',
  })
  all?: boolean;

  @Field(() => String, { nullable: true, description: 'Filter output based on conditions provided' })
  filter?: string;

  @Field(() => Number, { nullable: true, description: 'Show n last created containers (includes all states)' })
  last?: number;

  @Field(() => Boolean, { nullable: true, description: 'Show the latest created container (includes all states)' })
  latest?: boolean;

  @Field(() => Boolean, { nullable: true, description: "Don't truncate output" })
  noTrunc?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Display total file sizes' })
  size?: boolean;
}

@ObjectType()
export class DockerContainerLsResult {
  @Field(() => String, { nullable: true, description: 'Unique identifier for the container' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'Image used to create the container' })
  image?: string;

  @Field(() => String, { nullable: true, description: 'Command that the container is running' })
  command?: string;

  @Field(() => String, { nullable: true, description: 'Timestamp when the container was created' })
  created?: string;

  @Field(() => String, { nullable: true, description: 'Current status of the container' })
  status?: string;

  @Field(() => String, { nullable: true, description: 'Ports that are exposed by the container' })
  ports?: string;

  @Field(() => String, { nullable: true, description: 'Names assigned to the container' })
  names?: string;
}

@ArgsType()
export class DockerContainerPauseArgs {
  @Field(() => [String], { nullable: true, description: 'one or more containers to pause' })
  containers: string[];
}

@ArgsType()
export class DockerContainerCreateArgs {
  @Field(() => String, { description: 'The image for the container' })
  image: string;

  @Field(() => DockerContainerCreateOptions, { nullable: true, description: 'Options for creating the container' })
  options?: DockerContainerCreateOptions;

  @Field(() => [String], { nullable: true, description: 'Commands to run in the container' })
  commands?: string[];

  @Field(() => [String], { nullable: true, description: 'Arguments for the commands' })
  args?: string[];
}

@InputType()
export class DockerContainerCreateOptions {
  @Field(() => [String], { nullable: true, description: 'Add a custom host-to-IP mapping (host:ip)' })
  addHost?: string[];

  @Field(() => [String], { nullable: true, description: 'Attach to STDIN, STDOUT or STDERR' })
  attach?: string[];

  @Field(() => Number, {
    nullable: true,
    description: 'Block IO (relative weight), between 10 and 1000, or 0 to disable (default 0)',
  })
  blkioWeight?: number;

  @Field(() => [String], { nullable: true, description: 'Block IO weight (relative device weight) (default [])' })
  blkioWeightDevice?: string[];

  @Field(() => [String], { nullable: true, description: 'Add Linux capabilities' })
  capAdd?: string[];

  @Field(() => [String], { nullable: true, description: 'Drop Linux capabilities' })
  capDrop?: string[];

  @Field(() => String, { nullable: true, description: 'Name of the container' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'Optional parent cgroup for the container' })
  cgroupParent?: string;

  @Field(() => String, { nullable: true, description: 'Cgroup namespace to use (host|private)' })
  cgroupns?: string;

  @Field(() => String, { nullable: true, description: 'Write the container ID to the file' })
  cidfile?: string;

  @Field(() => Number, { nullable: true, description: 'Limit CPU CFS (Completely Fair Scheduler) period' })
  cpuPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'Limit CPU CFS (Completely Fair Scheduler) quota' })
  cpuQuota?: number;

  @Field(() => Number, { nullable: true, description: 'Limit CPU real-time period in microseconds' })
  cpuRtPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'Limit CPU real-time runtime in microseconds' })
  cpuRtRuntime?: number;

  @Field(() => Number, { nullable: true, description: 'CPU shares (relative weight)' })
  cpuShares?: number;

  @Field(() => Number, { nullable: true, description: 'Number of CPUs' })
  cpus?: number;

  @Field(() => String, { nullable: true, description: 'CPUs in which to allow execution (0-3, 0,1)' })
  cpusetCpus?: string;

  @Field(() => String, { nullable: true, description: 'MEMs in which to allow execution (0-3, 0,1)' })
  cpusetMems?: string;

  @Field(() => [String], { nullable: true, description: 'Add a host device to the container' })
  device?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Detach the container' })
  detach?: boolean;

  @Field(() => [String], { nullable: true, description: 'Add a rule to the cgroup allowed devices list' })
  deviceCgroupRule?: string[];

  @Field(() => [String], {
    nullable: true,
    description: 'Limit read rate (bytes per second) from a device (default [])',
  })
  deviceReadBps?: string[];

  @Field(() => [String], { nullable: true, description: 'Limit read rate (IO per second) from a device (default [])' })
  deviceReadIops?: string[];

  @Field(() => [String], {
    nullable: true,
    description: 'Limit write rate (bytes per second) to a device (default [])',
  })
  deviceWriteBps?: string[];

  @Field(() => [String], { nullable: true, description: 'Limit write rate (IO per second) to a device (default [])' })
  deviceWriteIops?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Skip image verification (default true)' })
  disableContentTrust?: boolean;

  @Field(() => [String], { nullable: true, description: 'Set custom DNS servers' })
  dns?: string[];

  @Field(() => [String], { nullable: true, description: 'Set DNS options' })
  dnsOption?: string[];

  @Field(() => [String], { nullable: true, description: 'Set custom DNS search domains' })
  dnsSearch?: string[];

  @Field(() => String, { nullable: true, description: 'Container NIS domain name' })
  domainname?: string;

  @Field(() => String, { nullable: true, description: 'Overwrite the default ENTRYPOINT of the image' })
  entrypoint?: string;

  @Field(() => String, { nullable: true, description: 'Set environment variables' })
  env?: string;

  @Field(() => [String], { nullable: true, description: 'Read in a file of environment variables' })
  envFile?: string[];

  @Field(() => [String], { nullable: true, description: 'Expose a port or a range of ports' })
  expose?: string[];

  @Field(() => String, { nullable: true, description: "GPU devices to add to the container ('all' to pass all GPUs)" })
  gpus?: string;

  @Field(() => [String], { nullable: true, description: 'Add additional groups to join' })
  groupAdd?: string[];

  @Field(() => String, { nullable: true, description: 'Command to run to check health' })
  healthCmd?: string;

  @Field(() => String, { nullable: true, description: 'Time between running the check (ms|s|m|h) (default 0s)' })
  healthInterval?: string;

  @Field(() => Number, { nullable: true, description: 'Consecutive failures needed to report unhealthy' })
  healthRetries?: number;

  @Field(() => String, {
    nullable: true,
    description:
      'Start period for the container to initialize before starting health-retries countdown (ms|s|m|h) (default 0s)',
  })
  healthStartPeriod?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Maximum time to allow one check to run (ms|s|m|h) (default 0s)',
  })
  healthTimeout?: string;

  @Field(() => Boolean, { nullable: true, description: 'Print usage' })
  help?: boolean;

  @Field(() => String, { nullable: true, description: 'Container host name' })
  hostname?: string;

  @Field(() => Boolean, {
    nullable: true,
    description: 'Run an init inside the container that forwards signals and reaps processes',
  })
  init?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Keep STDIN open even if not attached' })
  interactive?: boolean;

  @Field(() => String, { nullable: true, description: 'IPv4 address (e.g., 172.30.100.104)' })
  ip?: string;

  @Field(() => String, { nullable: true, description: 'IPv6 address (e.g., 2001:db8::33)' })
  ip6?: string;

  @Field(() => String, { nullable: true, description: 'IPC mode to use' })
  ipc?: string;

  @Field(() => String, { nullable: true, description: 'Container isolation technology' })
  isolation?: string;

  @Field(() => String, { nullable: true, description: 'Kernel memory limit' })
  kernelMemory?: string;

  @Field(() => [String], { nullable: true, description: 'Set meta data on a container' })
  label?: string[];

  @Field(() => [String], { nullable: true, description: 'Read in a line delimited file of labels' })
  labelFile?: string[];

  @Field(() => [String], { nullable: true, description: 'Add link to another container' })
  link?: string[];

  @Field(() => [String], { nullable: true, description: 'Container IPv4/IPv6 link-local addresses' })
  linkLocalIp?: string[];

  @Field(() => String, { nullable: true, description: 'Logging driver for the container' })
  logDriver?: string;

  @Field(() => [String], { nullable: true, description: 'Log driver options' })
  logOpt?: string[];

  @Field(() => String, { nullable: true, description: 'Container MAC address (e.g., 92:d0:c6:0a:29:33)' })
  macAddress?: string;

  @Field(() => String, { nullable: true, description: "Publish a container's port(s) to the host" })
  publish?: string;

  @Field(() => Boolean, { nullable: true, description: 'Publish all exposed ports to random ports' })
  publishAll?: boolean;

  @Field(() => String, {
    nullable: true,
    description: 'Pull image before creating ("always"|"missing"|"never") (default "missing")',
  })
  pull?: string;

  @Field(() => Boolean, { nullable: true, description: "Mount the container's root fileSystem as read only" })
  readOnly?: boolean;

  @Field(() => String, { nullable: true, description: 'Restart policy to apply when a container exits (default "no")' })
  restart?: string;

  @Field(() => Boolean, { nullable: true, description: 'Automatically remove the container when it exits' })
  rm?: boolean;

  @Field(() => String, { nullable: true, description: 'Runtime to use for this container' })
  runtime?: string;

  @Field(() => [String], { nullable: true, description: 'Security Options' })
  securityOpt?: string[];

  @Field(() => String, { nullable: true, description: 'Size of /dev/shm' })
  shmSize?: string;

  @Field(() => String, { nullable: true, description: 'Signal to stop a container (default "SIGTERM")' })
  stopSignal?: string;

  @Field(() => Number, { nullable: true, description: 'Timeout (in seconds) to stop a container' })
  stopTimeout?: number;

  @Field(() => [String], { nullable: true, description: 'Storage driver options for the container' })
  storageOpt?: string[];

  @Field(() => [String], { nullable: true, description: 'Sysctl options (default map[])' })
  sysctl?: string[];

  @Field(() => [String], { nullable: true, description: 'Mount a tmpfs directory' })
  tmpfs?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Allocate a pseudo-TTY' })
  tty?: boolean;

  @Field(() => [String], { nullable: true, description: 'Ulimit options (default [])' })
  ulimit?: string[];

  @Field(() => String, { nullable: true, description: 'Username or UID (format: <name|uid>[:<group|gid>])' })
  user?: string;

  @Field(() => String, { nullable: true, description: 'User namespace to use' })
  userns?: string;

  @Field(() => String, { nullable: true, description: 'UTS namespace to use' })
  uts?: string;

  @Field(() => [String], { nullable: true, description: 'Bind mount a volume' })
  volume?: string[];

  @Field(() => String, { nullable: true, description: 'Optional volume driver for the container' })
  volumeDriver?: string;

  @Field(() => [String], { nullable: true, description: 'Mount volumes from the specified container(s)' })
  volumesFrom?: string[];

  @Field(() => String, { nullable: true, description: 'Working directory inside the container' })
  workdir?: string;
}

@ObjectType()
export class DockerContainerCreateResult {
  @Field(() => String, { nullable: true, description: 'The ID of the created container' })
  id?: string;
}

@ArgsType()
export class DockerContainerRenameArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  nameOrId: string;

  @Field(() => String, { description: 'new name' })
  newName: string;
}

@ObjectType()
export class DockerContainerRenameResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerContainerAttachArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  container: string;

  @Field(() => DockerContainerAttachOptions, { nullable: true, description: 'Options for attaching the container' })
  options?: DockerContainerAttachOptions;
}

@InputType()
export class DockerContainerAttachOptions {
  @Field(() => String, { description: 'Override the key sequence for detaching a container' })
  detachKeys?: string;

  @Field(() => Boolean, { description: 'Do not attach STDIN' })
  noStdin?: boolean;

  @Field(() => Boolean, { description: 'Proxy all received signals to the process (default true)' })
  sigProxy?: boolean;
}

@ArgsType()
export class DockerContainerCommitArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  container: string;

  @Field(() => DockerContainerCommitOptions, { nullable: true, description: 'Options for committing the container' })
  options?: DockerContainerCommitOptions;

  @Field(() => String, { description: 'The name of the repository to commit to' })
  repository: string;
}

@InputType()
export class DockerContainerCommitOptions {
  @Field(() => String, { description: 'Author (e.g., "John Smith <john@a-team.com>")' })
  author?: string;

  @Field(() => [String], { description: 'Apply Dockerfile instruction to the created image' })
  change?: string[];

  @Field(() => String, { description: 'Commit message' })
  message?: string;

  @Field(() => Boolean, { description: 'Pause container during commit (default true)' })
  pause?: boolean;
}

@ArgsType()
export class DockerContainerCpArgs {
  @Field(() => String, { description: 'source path' })
  source: string;

  @Field(() => String, { description: 'destination path' })
  destination: string;

  @Field(() => DockerContainerCpOptions, { nullable: true, description: 'Options for copying files' })
  options?: DockerContainerCpOptions;
}

@InputType()
export class DockerContainerCpOptions {
  @Field(() => String, { description: 'Archive all in the source' })
  archive?: string;

  @Field(() => Boolean, { description: 'Always follow symbol link in SRC_PATH' })
  followLink?: boolean;
}

@ObjectType()
export class DockerContainerDiffResult {
  @Field(() => String, {
    description: 'Type of change that occurred (e.g., "A" for added, "D" for deleted, "C" for changed)',
  })
  changeType: string;

  @Field(() => String, { description: 'The file path that has been added, deleted, or changed' })
  filePath: string;
}

@ArgsType()
export class DockerContainerExecArgs {
  @Field(() => String, { description: 'ID or name of the container' })
  container: string;

  @Field(() => DockerContainerExecOptions, { description: 'Execution options for the container' })
  options: DockerContainerExecOptions;

  @Field(() => [String], { description: 'List of commands to execute in the container' })
  commands: string[];
}

@InputType()
export class DockerContainerExecOptions {
  @Field(() => Boolean, { nullable: true, description: 'Detached mode: run command in the background' })
  detach?: boolean;

  @Field(() => String, { nullable: true, description: 'Override the key sequence for detaching a container' })
  detachKeys?: string;

  @Field(() => [String], { nullable: true, description: 'Set environment variables' })
  env?: string[];

  @Field(() => [String], { nullable: true, description: 'Read in a file of environment variables' })
  envFile?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Keep STDIN open even if not attached' })
  interactive?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Give extended privileges to the command' })
  privileged?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Allocate a pseudo-TTY' })
  tty?: boolean;

  @Field(() => String, { nullable: true, description: 'Username or UID (format: <name|uid>[:<group|gid>])' })
  user?: string;

  @Field(() => String, { nullable: true, description: 'Working directory inside the container' })
  workdir?: string;
}

@ArgsType()
export class DockerContainerExportArgs {
  @Field(() => String, { description: 'ID or name of the container to export' })
  container: string;

  @Field(() => String, { nullable: true, description: 'Write to a file, instead of STDOUT' })
  output?: string;
}

@ArgsType()
export class DockerContainerInspectArgs {
  @Field(() => [String], { description: 'ID or name of the containers to inspect' })
  containers: string[];

  @Field(() => DockerContainerInspectOptions, { nullable: true, description: 'Options for inspecting the container' })
  options?: DockerContainerInspectOptions;
}

@InputType()
export class DockerContainerInspectOptions {
  @Field(() => String, { nullable: true, description: 'Format the output using the given Go template' })
  format?: string;

  @Field(() => Boolean, { nullable: true, description: 'Display total file sizes' })
  size?: boolean;
}

@ArgsType()
export class DockerContainerKillArgs {
  @Field(() => [String], { description: 'ID or name of the containers' })
  containers: string[];

  @Field(() => DockerContainerKillOptions, { nullable: true, description: 'Options for killing the container' })
  options?: DockerContainerKillOptions;
}

@InputType()
export class DockerContainerKillOptions {
  @Field(() => String, { nullable: true, description: 'Signal to send to the container (default "SIGKILL")' })
  signal?: string;
}

@ObjectType()
export class DockerContainerKillResult {
  @Field(() => [String], { nullable: true, description: 'The ID"s of the containers that were killed' })
  containers: string[];
}

@ArgsType()
export class DockerContainerLogsArgs {
  @Field(() => String, { description: 'ID or name of the container' })
  container: string;

  @Field(() => DockerContainerLogsOptions, { nullable: true, description: 'Options for fetching the logs' })
  options?: DockerContainerLogsOptions;
}

@InputType()
export class DockerContainerLogsOptions {
  @Field(() => Boolean, { nullable: true, description: 'Show extra details provided to logs' })
  details?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Follow log output' })
  follow?: boolean;

  @Field(() => String, {
    nullable: true,
    description: 'Show logs since timestamp (e.g. 2013-01-02T13:23:37Z) or relative (e.g. 42m for 42 minutes)',
  })
  since?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Number of lines to show from the end of the logs (default "all")',
  })
  tail?: string;

  @Field(() => Boolean, { nullable: true, description: 'Show timestamps' })
  timestamps?: boolean;

  @Field(() => String, {
    nullable: true,
    description: 'Show logs before a timestamp (e.g. 2013-01-02T13:23:37Z) or relative (e.g. 42m for 42 minutes)',
  })
  until?: string;
}

@ArgsType()
export class DockerContainerPortArgs {
  @Field(() => String, { description: 'ID or name of the container' })
  container: string;

  @Field(() => String, { nullable: true, description: 'Private port' })
  privatePort?: string;
}

@ObjectType()
export class DockerContainerPortResult {
  @Field(() => [String], { nullable: true, description: 'The public port that maps to the private port' })
  publicPorts?: string[];
}

@ArgsType()
export class DockerContainerPruneArgs {
  @Field(() => String, { nullable: true, description: 'Provide filter values (e.g. "until=<timestamp>")' })
  filter?: string;

  @Field(() => Boolean, { nullable: true, description: 'Do not prompt for confirmation' })
  force?: boolean;
}

@ArgsType()
export class DockerContainerRestartArgs {
  @Field(() => [String], { description: 'ID or name of the containers' })
  containers: string[];

  @Field(() => DockerContainerRestartOptions, { nullable: true, description: 'Options for restarting the container' })
  options?: DockerContainerRestartOptions;
}

@InputType()
export class DockerContainerRestartOptions {
  @Field(() => Number, { nullable: true, description: 'Seconds to wait for stop before killing the container' })
  time?: number;
}

@ArgsType()
export class DockerContainerRmArgs {
  @Field(() => [String], { description: 'ID or name of the containers' })
  containers: string[];

  @Field(() => DockerContainerRmOptions, { nullable: true, description: 'Options for removing the container' })
  options?: DockerContainerRmOptions;
}

@InputType()
export class DockerContainerRmOptions {
  @Field(() => Boolean, { nullable: true, description: 'Force the removal of a running container (uses SIGKILL)' })
  force?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Remove the specified link' })
  link?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Remove anonymous volumes associated with the container' })
  volumes?: boolean;
}

@ArgsType()
export class DockerContainerStartArgs {
  @Field(() => [String], { description: 'ID or name of the containers to start' })
  containers: string[];

  @Field(() => DockerContainerStartOptions, { nullable: true, description: 'options for the start container' })
  options?: DockerContainerStartOptions;
}

@InputType()
export class DockerContainerStartOptions {
  @Field(() => Boolean, { nullable: true, description: 'Attach STDOUT/STDERR and forward signals' })
  attach?: boolean;

  @Field(() => String, { nullable: true, description: 'Override the key sequence for detaching a container' })
  detachKeys?: string;

  @Field(() => Boolean, { nullable: true, description: "Attach container's STDIN" })
  interactive?: boolean;
}

@ObjectType()
export class DockerContainerStartResult {
  @Field(() => [String], { nullable: true, description: 'IDs or names of the started containers' })
  containers: string[];
}

@ArgsType()
export class DockerContainerUpdateArgs {
  @Field(() => [String], { description: 'ID or name of the container to update' })
  containers: string[];

  @Field(() => DockerContainerUpdateOptions, { description: 'Options for updating the container' })
  options: DockerContainerUpdateOptions;
}

@InputType()
export class DockerContainerUpdateOptions {
  @Field(() => Number, {
    nullable: true,
    description: 'Block IO (relative weight), between 10 and 1000, or 0 to disable (default 0)',
  })
  blkioWeight?: number;

  @Field(() => Number, { nullable: true, description: 'Limit CPU CFS (Completely Fair Scheduler) period' })
  cpuPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'Limit CPU CFS (Completely Fair Scheduler) quota' })
  cpuQuota?: number;

  @Field(() => Number, { nullable: true, description: 'Limit the CPU real-time period in microseconds' })
  cpuRtPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'Limit the CPU real-time runtime in microseconds' })
  cpuRtRuntime?: number;

  @Field(() => Number, { nullable: true, description: 'CPU shares (relative weight)' })
  cpuShares?: number;

  @Field(() => Number, { nullable: true, description: 'Number of CPUs' })
  cpus?: number;

  @Field(() => String, { nullable: true, description: 'CPUs in which to allow execution (0-3, 0,1)' })
  cpusetCpus?: string;

  @Field(() => String, { nullable: true, description: 'MEMs in which to allow execution (0-3, 0,1)' })
  cpusetMems?: string;

  @Field(() => Boolean, { nullable: true, description: 'Detach the container' })
  detach?: boolean;

  @Field(() => String, { nullable: true, description: 'Kernel memory limit' })
  kernelMemory?: string;

  @Field(() => String, { nullable: true, description: 'Memory limit' })
  memory?: string;

  @Field(() => String, { nullable: true, description: 'Memory soft limit' })
  memoryReservation?: string;

  @Field(() => String, {
    nullable: true,
    description: "Swap limit equal to memory plus swap: '-1' to enable unlimited swap",
  })
  memorySwap?: string;

  @Field(() => Number, { nullable: true, description: 'Tune container pids limit (set -1 for unlimited)' })
  pidsLimit?: number;

  @Field(() => String, { nullable: true, description: 'Restart policy to apply when a container exits' })
  restart?: string;
}

@ArgsType()
export class DockerContainerTopArgs {
  @Field(() => String, { description: 'ID or name of the container to display the running processes' })
  container: string;
}

@ObjectType()
export class DockerContainerTopResult {
  @Field(() => String, { description: 'User identifier' })
  uId: string;

  @Field(() => String, { description: 'Process identifier' })
  pId: string;

  @Field(() => String, { description: 'Parent process identifier' })
  ppId: string;

  @Field(() => String, { description: 'CPU usage' })
  c: string;

  @Field(() => String, { description: 'Start time of the process' })
  sTime: string;

  @Field(() => String, { description: 'Controlling terminal' })
  tty: string;

  @Field(() => String, { description: 'Cumulative CPU time' })
  time: string;

  @Field(() => String, { description: 'Command name/line' })
  cmd: string;
}

@ArgsType()
export class DockerContainerWaitArgs {
  @Field(() => [String], { description: 'ID or name of the container to wait' })
  containers: string[];
}

@ArgsType()
export class DockerContainerStatsArgs {
  @Field(() => [String], { nullable: true, description: 'ID or name of the container to display the stats' })
  containers?: string[];

  @Field(() => DockerContainerStatsOptions, { nullable: true, description: 'Options for fetching the stats' })
  options?: DockerContainerStatsOptions;
}

@InputType()
export class DockerContainerStatsOptions {
  @Field(() => Boolean, { nullable: true, description: 'Show all containers (default shows just running)' })
  all?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Disable streaming stats and only pull the first result' })
  noStream?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Do not truncate output' })
  noTrunc?: boolean;
}

@ObjectType()
export class DockerContainerStatsResult {
  @Field(() => String, { nullable: true, description: 'container id' })
  containerId?: string;

  @Field(() => String, { nullable: true, description: 'container name' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'cpu percentage' })
  cpuPercentage?: string;

  @Field(() => String, { nullable: true, description: 'memory usage' })
  memoryUsage?: string;

  @Field(() => String, { nullable: true, description: 'memory limit' })
  memoryLimit?: string;

  @Field(() => String, { nullable: true, description: 'memory percentage' })
  memoryPercentage?: string;

  @Field(() => String, { nullable: true, description: 'network input/output' })
  netIO?: string;

  @Field(() => String, { nullable: true, description: 'block input/output' })
  blockIO?: string;

  @Field(() => String, { nullable: true, description: 'process id' })
  pids?: string;
}

@ArgsType()
export class DockerContainerStopArgs {
  @Field(() => [String], { description: 'ID or name of the containers to stop' })
  containers: string[];

  @Field(() => DockerContainerStopOptions, { nullable: true, description: 'Options for stopping the container' })
  options?: DockerContainerStopOptions;
}

@InputType()
export class DockerContainerStopOptions {
  @Field(() => Number, { nullable: true, description: 'Seconds to wait for stop before killing the container' })
  time?: number;
}
