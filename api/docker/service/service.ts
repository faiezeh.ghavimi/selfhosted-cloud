/*
 *  File: /docker/service/service.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import axios, { AxiosError } from 'axios';
import kebabCase from 'lodash.kebabcase';
import { DockerNodeService } from '../node';
import { DockerService } from '../dockerService';
import { DockerStackService } from '../stack';
import { DockerSwarmService } from '../swarm';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { GraphQLError } from 'graphql';
import { Logger, Injectable, Inject } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from '../utils';
import {
  DockerServiceCreateArgs,
  DockerServiceCreateResult,
  DockerServiceExecArgs,
  DockerServiceExecResult,
  DockerServiceInspectArgs,
  DockerServiceInspectResult,
  DockerServiceLogsArgs,
  DockerServiceLogsResult,
  DockerServiceLsArgs,
  DockerServiceLsResult,
  DockerServicePsArgs,
  DockerServicePsResult,
  DockerServiceRmArgs,
  DockerServiceRmResult,
  DockerServiceRollbackArgs,
  DockerServiceRollbackResult,
  DockerServiceScaleArgs,
  DockerServiceScaleResult,
  DockerServiceUpdateArgs,
  DockerServiceUpdateResult,
} from './dto';
import { KeycloakRequest, Token } from '@multiplatform.one/keycloak-typegraphql';

@Injectable()
export class DockerServiceService {
  constructor(
    private readonly dockerNodeService: DockerNodeService,
    private readonly dockerService: DockerService,
    private readonly dockerStackService: DockerStackService,
    private readonly dockerSwarmService: DockerSwarmService,
    private readonly logger: Logger,
    @Inject('REQ') private readonly req: KeycloakRequest,
  ) {}

  async inspect(args: DockerServiceInspectArgs): Promise<DockerServiceInspectResult> {
    const { stderr, stdout } = await this.service(
      'inspect',
      {
        format: 'json',
      },
      args.name,
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout))?.[0];
  }

  async create(args: DockerServiceCreateArgs): Promise<DockerServiceCreateResult> {
    const { stderr, stdout } = await this.service(
      'create',
      {
        name: args.name,
        detach: true,
        publish: args.publish,
        env: args.env,
        label: args.label,
        constraint: args.constraint,
        mode: args.mode,
        network: args.network,
        endpointMode: args.endpointMode,
        restartCondition: args.restartCondition,
        restartDelay: args.restartDelay,
        restartMaxAttempts: args.restartMaxAttempts,
        restartWindow: args.restartWindow,
        rollbackDelay: args.rollbackDelay,
        rollbackFailureAction: args.rollbackFailureAction,
        rollbackMaxFailureRatio: args.rollbackMaxFailureRatio,
        rollbackMonitor: args.rollbackMonitor,
        rollbackOrder: args.rollbackOrder,
        rollbackParallelism: args.rollbackParallelism,
        updateDelay: args.updateDelay,
        updateFailureAction: args.updateFailureAction,
        updateMaxFailureRatio: args.updateMaxFailureRatio,
        updateMonitor: args.updateMonitor,
        updateOrder: args.updateOrder,
        updateParallelism: args.updateParallelism,
      },
      args.image,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr,
    };
  }

  async update(args: DockerServiceUpdateArgs): Promise<DockerServiceUpdateResult> {
    const { stderr, stdout } = await this.service(
      'update',
      {
        name: args.name,
        image: args.image,
        args: args.args,
        env: args.envRm,
        label: args.labelRm,
        constraint: args.constraintRm,
        network: args.networkRm,
        endpointMode: args.endpointMode,
        restartCondition: args.restartCondition,
        restartDelay: args.restartDelay,
        restartMaxAttempts: args.restartMaxAttempts,
        restartWindow: args.restartWindow,
        rollbackDelay: args.rollbackDelay,
        rollbackFailureAction: args.rollbackFailureAction,
        rollbackMaxFailureRatio: args.rollbackMaxFailureRatio,
        rollbackMonitor: args.rollbackMonitor,
        rollbackOrder: args.rollbackOrder,
        rollbackParallelism: args.rollbackParallelism,
        updateDelay: args.updateDelay,
        updateFailureAction: args.updateFailureAction,
        updateMaxFailureRatio: args.updateMaxFailureRatio,
        updateMonitor: args.updateMonitor,
        updateOrder: args.updateOrder,
        updateParallelism: args.updateParallelism,
      },
      args.name,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr,
    };
  }

  async rollback(args: DockerServiceRollbackArgs): Promise<DockerServiceRollbackResult> {
    const { stderr, stdout } = await this.service(
      'rollback',
      {
        format: 'json',
      },
      args.name,
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout))?.[0];
  }

  async scale(args: DockerServiceScaleArgs): Promise<DockerServiceScaleResult> {
    const { stderr, stdout } = await this.service(
      'scale',
      {
        format: 'json',
      },
      args.name,
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout))?.[0];
  }

  async logs(args: DockerServiceLogsArgs): Promise<DockerServiceLogsResult> {
    const { stderr, stdout } = await this.service(
      'logs',
      {
        details: args.details,
        follow: args.follow,
        since: args.since,
        tail: args.tail,
        timestamps: args.timestamps,
      },
      args.name,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      logs: stdout,
    };
  }

  async ps(args: DockerServicePsArgs): Promise<DockerServicePsResult[]> {
    const { stderr, stdout } = await this.service(
      'ps',
      {
        filter: args.filter,
        format: '{{json .}}',
        noResolve: args.noResolve,
        noTrunc: args.noTrunc,
      },
      args.name,
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async ls(args: DockerServiceLsArgs): Promise<DockerServiceLsResult[]> {
    const { stderr, stdout } = await this.service('ls', {
      format: '{{json .}}',
      filter: args.filter,
    });
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async rm(args: DockerServiceRmArgs): Promise<DockerServiceRmResult> {
    const { stderr, stdout } = await this.service('rm', {}, args.name);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr,
    };
  }

  async exec(args: DockerServiceExecArgs): Promise<DockerServiceExecResult[]> {
    const tasks = (
      await this.ps({
        name: args.name,
        filter: ['desired-state=running'],
      })
    ).filter((_, i) => args.all || i === 0);
    const self = await this.dockerNodeService.inspect({ hostname: 'self' });
    const accessToken = (this.req?.kauth?.grant?.access_token as Token)?.token;
    return (
      await Promise.all(
        tasks.map(async (task) => {
          if (!task.node || !task.id) return null;
          const node = await this.dockerNodeService.inspect({ hostname: task.node });
          if (node.description?.hostname === self.description?.hostname) {
            const containerId = (await this.dockerService.inspect({ nameOrId: task.id })).status?.containerStatus
              ?.containerId;
            if (!containerId) return null;
            const { stdout, stderr } = await this.dockerService.exec({
              args: args.args,
              command: args.command,
              detach: args.detach,
              env: args.env,
              interactive: args.interactive,
              nameOrId: containerId,
              privileged: args.privileged,
              tty: args.tty,
              user: args.user,
              workdir: args.workdir,
            });
            return { stdout, stderr };
          }
          if (!accessToken || !node.status?.addr) return null;
          const port = Number(process.env.EXPOSED_API_PORT || process.env.API_PORT || 5001);
          try {
            const inspectResponse = await axios.post<{
              data: {
                dockerInspect: {
                  status: {
                    containerStatus: {
                      containerId: string;
                    };
                  };
                };
              };
              errors?: GraphQLError[];
            }>(
              `http://${node.status.addr}:${port}/graphql`,
              {
                query: `
                  query DockerInspect {
                    dockerInspect(nameOrId: "${task.id}") {
                      status {
                        containerStatus {
                          containerId
                        }
                      }
                    }
                  }
            `,
              },
              {
                headers: {
                  authorization: `Bearer ${accessToken}`,
                },
              },
            );
            if (inspectResponse.data.errors) {
              throw new GraphQLError('failed to inspect service through graphql', {
                extensions: {
                  externalErrors: inspectResponse.data.errors,
                },
              });
            }
            if (!inspectResponse?.data?.data?.dockerInspect?.status?.containerStatus?.containerId) {
              throw new GraphQLError('failed to inspect service through graphql');
            }
            const containerId = inspectResponse.data.data.dockerInspect.status.containerStatus.containerId;
            const execResponse = await axios.post<{
              data: {
                dockerExec: {
                  stdout: string;
                  stderr: string;
                };
              };
              errors?: GraphQLError[];
            }>(
              `http://${node.status.addr}:${port}/graphql`,
              {
                query: `
                  mutation DockerExec {
                    dockerExec(${Object.entries({
                      nameOrId: containerId,
                      command: args.command,
                      ...(args.args && { args: args.args }),
                      ...(args.detach && { detach: args.detach }),
                      ...(args.env && { env: args.env }),
                      ...(args.interactive && { interactive: args.interactive }),
                      ...(args.privileged && { privileged: args.privileged }),
                      ...(args.tty && { tty: args.tty }),
                      ...(args.user && { user: args.user }),
                      ...(args.workdir && { workdir: args.workdir }),
                    })
                      .map(([key, value]) => `${key}: ${JSON.stringify(value)}`)
                      .join(', ')}) {
                      stdout
                      stderr
                    }
                  }
            `,
              },
              {
                headers: {
                  authorization: `Bearer ${accessToken}`,
                },
              },
            );
            if (execResponse.data.errors) {
              throw new GraphQLError('failed to exec command through graphql', {
                extensions: {
                  externalErrors: execResponse.data.errors,
                },
              });
            }
            if (!execResponse?.data?.data?.dockerExec || typeof execResponse.data.data.dockerExec.stdout !== 'string') {
              throw new GraphQLError('failed to exec command through graphql');
            }
            return {
              stdout: execResponse.data.data.dockerExec.stdout,
              stderr: execResponse.data.data.dockerExec.stderr,
            };
          } catch (err) {
            const error = err as AxiosError;
            if (!error.response) throw err;
          }
        }),
      )
    ).filter(Boolean) as DockerServiceExecResult[];
  }

  private async service(
    command: string,
    options: Record<string, Flag> = {},
    ...args: (string | undefined)[]
  ): Promise<ServiceResult> {
    try {
      const p = execa('docker', [
        'service',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface ServiceResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
