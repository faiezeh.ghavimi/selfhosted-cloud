/*
 *  File: /docker/service/resolver.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerServiceService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Resolver, Args, Query, Mutation } from 'type-graphql';
import {
  DockerServiceInspectArgs,
  DockerServiceInspectResult,
  DockerServicePsArgs,
  DockerServicePsResult,
  DockerServiceLsArgs,
  DockerServiceLsResult,
  DockerServiceRollbackResult,
  DockerServiceScaleArgs,
  DockerServiceScaleResult,
  DockerServiceRollbackArgs,
  DockerServiceRmResult,
  DockerServiceRmArgs,
  DockerServiceLogsArgs,
  DockerServiceLogsResult,
  DockerServiceCreateArgs,
  DockerServiceCreateResult,
  DockerServiceUpdateArgs,
  DockerServiceUpdateResult,
  DockerServiceExecResult,
  DockerServiceExecArgs,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerServiceResolver {
  constructor(private readonly dockerService: DockerServiceService) {}

  @Query(() => DockerServiceInspectResult)
  async dockerServiceInspect(@Args() args: DockerServiceInspectArgs) {
    return this.dockerService.inspect(args);
  }

  @Query(() => [DockerServicePsResult])
  async dockerServicePs(@Args() args: DockerServicePsArgs) {
    return this.dockerService.ps(args);
  }

  @Mutation(() => DockerServiceCreateResult)
  async dockerServiceCreate(@Args() args: DockerServiceCreateArgs) {
    return this.dockerService.create(args);
  }

  @Mutation(() => DockerServiceUpdateResult)
  async dockerServiceUpdate(@Args() args: DockerServiceUpdateArgs) {
    return this.dockerService.update(args);
  }

  @Query(() => [DockerServiceLsResult])
  async dockerServiceLs(@Args() args: DockerServiceLsArgs) {
    return this.dockerService.ls(args);
  }

  @Mutation(() => DockerServiceScaleResult)
  async dockerServiceScale(@Args() args: DockerServiceScaleArgs) {
    return this.dockerService.scale(args);
  }

  @Mutation(() => DockerServiceRollbackResult)
  async dockerServiceRollback(@Args() args: DockerServiceRollbackArgs) {
    return this.dockerService.rollback(args);
  }

  @Mutation(() => [DockerServiceRmResult])
  async dockerServiceRm(@Args() args: DockerServiceRmArgs) {
    return this.dockerService.rm(args);
  }

  @Query(() => DockerServiceLogsResult)
  async dockerServiceLogs(@Args() args: DockerServiceLogsArgs) {
    return this.dockerService.logs(args);
  }

  @Mutation(() => [DockerServiceExecResult])
  async dockerServiceExec(@Args(() => DockerServiceExecArgs) args: DockerServiceExecArgs) {
    return this.dockerService.exec(args);
  }
}
