/*
 *  File: /docker/service/dto.ts
 *  File: /docker/service/dto.ts
 *  File Created: 31-01-2024 15:15:05
 *  Author: K S R PHANI BHUSHAN
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/* eslint-disable max-lines */
/*
 *  File: /docker/service/dto.ts
 *  Project: api
 *  File Created: 19-01-2024 09:43:03
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ArgsType, Field, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerServicePsArgs {
  @Field(() => String, { description: 'name of the service' })
  name: string;

  @Field(() => [String], { nullable: true, description: 'filter output based on conditions provided' })
  filter?: string[];

  @Field(() => Boolean, { nullable: true, description: 'do not map ids to names' })
  noResolve?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'do not truncate output' })
  noTrunc?: boolean;
}

@ObjectType()
export class DockerServicePsResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  currentState?: string;

  @Field(() => String, { nullable: true })
  desiredState?: string;

  @Field(() => String, { nullable: true })
  error?: string;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  node?: string;

  @Field(() => String, { nullable: true })
  ports?: string;
}

@ArgsType()
export class DockerServiceCreateArgs {
  @Field(() => [String], { description: 'Add Linux capabilities' })
  capAdd?: string[];

  @Field(() => [String], { description: 'Drop Linux capabilities' })
  capDrop?: string[];

  @Field(() => [String], { description: 'Specify configurations to expose to the service' })
  config?: string[];

  @Field(() => [String], { description: 'Placement constraints' })
  constraint?: string[];

  @Field(() => [String], { description: 'Container labels' })
  containerLabel?: string[];

  @Field(() => String, { description: 'Credential spec for managed service account (Windows only)' })
  credentialSpec?: string;

  @Field(() => Boolean, { description: 'Exit immediately instead of waiting for the service to converge' })
  detach?: boolean;

  @Field(() => [String], { description: 'Set custom DNS servers' })
  dns?: string[];

  @Field(() => [String], { description: 'Set DNS options' })
  dnsOption?: string[];

  @Field(() => [String], { description: 'Set custom DNS search domains' })
  dnsSearch?: string[];

  @Field(() => String, { description: 'Endpoint mode (vip or dnsrr) (default "vip")' })
  endpointMode?: string;

  @Field(() => String, { description: 'Overwrite the default ENTRYPOINT of the image' })
  entrypoint?: string;

  @Field(() => [String], { description: 'Set environment variables' })
  env?: string[];

  @Field(() => [String], { description: 'Read in a file of environment variables' })
  envFile?: string[];

  @Field(() => [String], { description: 'User defined resources' })
  genericResource?: string[];

  @Field(() => [String], { description: 'Set one or more supplementary user groups for the container' })
  group?: string[];

  @Field(() => String, { description: 'Command to run to check health' })
  healthCmd?: string;

  @Field(() => String, { description: 'Time between running the check (ms|s|m|h)' })
  healthInterval?: string;

  @Field(() => Number, { description: 'Consecutive failures needed to report unhealthy' })
  healthRetries?: number;

  @Field(() => String, { description: 'Time between running the check during the start period (ms|s|m|h)' })
  healthStartInterval?: string;

  @Field(() => String, {
    description: 'Start period for the container to initialize before counting retries towards unstable (ms|s|m|h)',
  })
  healthStartPeriod?: string;

  @Field(() => String, { description: 'Maximum time to allow one check to run (ms|s|m|h)' })
  healthTimeout?: string;

  @Field(() => [String], { description: 'Set one or more custom host-to-IP mappings (host:ip)' })
  host?: string[];

  @Field(() => String, { description: 'Container hostname' })
  hostname?: string;

  @Field(() => Boolean, {
    description: 'Use an init inside each service container to forward signals and reap processes',
  })
  init?: boolean;

  @Field(() => [String], { description: 'Service labels' })
  label?: string[];

  @Field(() => Number, { description: 'Limit CPUs' })
  limitCpu?: number;

  @Field(() => Number, { description: 'Limit Memory' })
  limitMemory?: number;

  @Field(() => Number, { description: 'Limit maximum number of processes (default 0 = unlimited)' })
  limitPids?: number;

  @Field(() => String, { description: 'Logging driver for service' })
  logDriver?: string;

  @Field(() => [String], { description: 'Logging driver options' })
  logOpt?: string[];

  @Field(() => Number, { description: 'Number of job tasks to run concurrently (default equal to --replicas)' })
  maxConcurrent?: number;

  @Field(() => String, {
    description: 'Service mode ("replicated", "global", "replicated-job", "global-job") (default "replicated")',
  })
  mode?: string;

  @Field(() => [String], { description: 'Attach a filesystem mount to the service' })
  mount?: string[];

  @Field(() => String, { description: 'Service name' })
  name?: string;

  @Field(() => [String], { description: 'Network attachments' })
  network?: string[];

  @Field(() => Boolean, { description: 'Disable any container-specified HEALTHCHECK' })
  noHealthcheck?: boolean;

  @Field(() => Boolean, { description: 'Do not query the registry to resolve image digest and supported platforms' })
  noResolveImage?: boolean;

  @Field(() => [String], { description: 'Add a placement preference' })
  placementPref?: string[];

  @Field(() => [String], { description: 'Publish a port as a node port' })
  publish?: string[];

  @Field(() => Boolean, { description: 'Suppress progress output' })
  quiet?: boolean;

  @Field(() => Number, { description: 'Replicas' })
  replicas?: number;

  @Field(() => Number, { description: 'Maximum number of tasks per node (default 0 = unlimited)' })
  replicasMaxPerNode?: number;

  @Field(() => Number, { description: 'Reserve CPUs' })
  reserveCpu?: number;

  @Field(() => Number, { description: 'Reserve Memory' })
  reserveMemory?: number;

  @Field(() => String, { description: 'Restart when condition is met ("none", "on-failure", "any") (default "any")' })
  restartCondition?: string;

  @Field(() => String, { description: 'Delay between restart attempts (ns|us|ms|s|m|h) (default 5s)' })
  restartDelay?: string;

  @Field(() => Number, { description: 'Maximum number of restarts before giving up' })
  restartMaxAttempts?: number;

  @Field(() => String, { description: 'Window used to evaluate the restart policy (ns|us|ms|s|m|h)' })
  restartWindow?: string;

  @Field(() => String, { description: 'Delay between task rollbacks (ns|us|ms|s|m|h) (default 0s)' })
  rollbackDelay?: string;

  @Field(() => String, { description: 'Action on rollback failure ("pause", "continue") (default "pause")' })
  rollbackFailureAction?: string;

  @Field(() => Number, { description: 'Failure rate to tolerate during a rollback (default 0)' })
  rollbackMaxFailureRatio?: number;

  @Field(() => String, {
    description: 'Duration after each task rollback to monitor for failure (ns|us|ms|s|m|h) (default 5s)',
  })
  rollbackMonitor?: string;

  @Field(() => String, { description: 'Rollback order ("start-first", "stop-first") (default "stop-first")' })
  rollbackOrder?: string;

  @Field(() => Number, {
    description: 'Maximum number of tasks rolled back simultaneously (0 to roll back all at once) (default 1)',
  })
  rollbackParallelism?: number;

  @Field(() => [String], { description: 'Specify secrets to expose to the service' })
  secret?: string[];

  @Field(() => String, { description: 'Time to wait before force killing a container (ns|us|ms|s|m|h) (default 10s)' })
  stopGracePeriod?: string;

  @Field(() => String, { description: 'Signal to stop the container' })
  stopSignal?: string;

  @Field(() => [String], { description: 'Sysctl options' })
  sysctl?: string[];

  @Field(() => Boolean, { description: 'Allocate a pseudo-TTY' })
  tty?: boolean;

  @Field(() => [String], { description: 'Ulimit options (default [])' })
  ulimit?: string[];

  @Field(() => String, { description: 'Delay between updates (ns|us|ms|s|m|h) (default 0s)' })
  updateDelay?: string;

  @Field(() => String, { description: 'Action on update failure ("pause", "continue", "rollback") (default "pause")' })
  updateFailureAction?: string;

  @Field(() => Number, { description: 'Failure rate to tolerate during an update (default 0)' })
  updateMaxFailureRatio?: number;

  @Field(() => String, {
    description: 'Duration after each task update to monitor for failure (ns|us|ms|s|m|h) (default 5s)',
  })
  updateMonitor?: string;

  @Field(() => String, { description: 'Update order ("start-first", "stop-first") (default "stop-first")' })
  updateOrder?: string;

  @Field(() => Number, {
    description: 'Maximum number of tasks updated simultaneously (0 to update all at once) (default 1)',
  })
  updateParallelism?: number;

  @Field(() => String, { description: 'Username or UID (format: <name|uid>[:<group|gid>])' })
  user?: string;

  @Field(() => Boolean, { description: 'Send registry authentication details to swarm agents' })
  withRegistryAuth?: boolean;

  @Field(() => String, { description: 'Working directory inside the container' })
  workdir?: string;

  @Field(() => String, { description: 'Image name' })
  image: string;
}

@ObjectType()
export class DockerServiceCreateResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  mode?: string;

  @Field(() => String, { nullable: true })
  ports?: string;

  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerServiceUpdateArgs {
  @Field(() => [String], { description: 'Service command args' })
  args?: string[];

  @Field(() => [String], { description: 'Add Linux capabilities' })
  capAdd?: string[];

  @Field(() => [String], { description: 'Drop Linux capabilities' })
  capDrop?: string[];

  @Field(() => [String], { description: 'Add or update a config file on a service' })
  configAdd?: string[];

  @Field(() => [String], { description: 'Remove a configuration file' })
  configRm?: string[];

  @Field(() => [String], { description: 'Add or update a placement constraint' })
  constraintAdd?: string[];

  @Field(() => [String], { description: 'Remove a constraint' })
  constraintRm?: string[];

  @Field(() => [String], { description: 'Add or update a container label' })
  containerLabelAdd?: string[];

  @Field(() => [String], { description: 'Remove a container label by its key' })
  containerLabelRm?: string[];

  @Field(() => String, { description: 'Credential spec for managed service account (Windows only)' })
  credentialSpec?: string;

  @Field(() => Boolean, { description: 'Exit immediately instead of waiting for the service to converge' })
  detach?: boolean;

  @Field(() => [String], { description: 'Add or update a custom DNS server' })
  dnsAdd?: string[];

  @Field(() => [String], { description: 'Add or update a DNS option' })
  dnsOptionAdd?: string[];

  @Field(() => [String], { description: 'Remove a DNS option' })
  dnsOptionRm?: string[];

  @Field(() => [String], { description: 'Remove a custom DNS server' })
  dnsRm?: string[];

  @Field(() => [String], { description: 'Add or update a custom DNS search domain' })
  dnsSearchAdd?: string[];

  @Field(() => [String], { description: 'Remove a DNS search domain' })
  dnsSearchRm?: string[];

  @Field(() => String, { description: 'Endpoint mode (vip or dnsrr)' })
  endpointMode?: string;

  @Field(() => String, { description: 'Overwrite the default ENTRYPOINT of the image })' })
  entrypoint?: string;

  @Field(() => [String], { description: 'Add or update an environment variable' })
  envAdd?: string[];

  @Field(() => [String], { description: 'Remove an environment variable' })
  envRm?: string[];

  @Field(() => Boolean, { description: 'Force update even if no changes require it' })
  force?: boolean;

  @Field(() => [String], { description: 'Add a Generic resource' })
  genericResourceAdd?: string[];

  @Field(() => [String], { description: 'Remove a Generic resource' })
  genericResourceRm?: string[];

  @Field(() => [String], { description: 'Add an additional supplementary user group to the container' })
  groupAdd?: string[];

  @Field(() => [String], { description: 'Remove a previously added supplementary user group from the container' })
  groupRm?: string[];

  @Field(() => String, { description: 'Command to run to check health' })
  healthCmd?: string;

  @Field(() => String, { description: 'Time between running the check (ms|s|m|h)' })
  healthInterval?: string;

  @Field(() => Number, { description: 'Consecutive failures needed to report unhealthy' })
  healthRetries?: number;

  @Field(() => String, { description: 'Time between running the check during the start period (ms|s|m|h)' })
  healthStartInterval?: string;

  @Field(() => String, {
    description: 'Start period for the container to initialize before counting retries towards unstable (ms|s|m|h)',
  })
  healthStartPeriod?: string;

  @Field(() => String, { description: 'Maximum time to allow one check to run (ms|s|m|h)' })
  healthTimeout?: string;

  @Field(() => [String], { description: 'Add a custom host-to-IP mapping ("host:ip")' })
  hostAdd?: string[];

  @Field(() => [String], { description: 'Remove a custom host-to-IP mapping ("host:ip")' })
  hostRm?: string[];

  @Field(() => String, { description: 'Container hostname' })
  hostname?: string;

  @Field(() => String, { description: 'Service image tag' })
  image?: string;

  @Field(() => Boolean, {
    description: 'Use an init inside each service container to forward signals and reap processes',
  })
  init?: boolean;

  @Field(() => String, { description: 'Service container isolation mode' })
  isolation?: string;

  @Field(() => [String], { description: 'Add or update a service label' })
  labelAdd?: string[];

  @Field(() => [String], { description: 'Remove a label by its key' })
  labelRm?: string[];

  @Field(() => Number, { description: 'Limit CPUs' })
  limitCpu?: number;

  @Field(() => Number, { description: 'Limit Memory' })
  limitMemory?: number;

  @Field(() => Number, { description: 'Limit maximum number of processes (default 0 = unlimited)' })
  limitPids?: number;

  @Field(() => String, { description: 'Logging driver for service' })
  logDriver?: string;

  @Field(() => [String], { description: 'Logging driver options' })
  logOpt?: string[];

  @Field(() => Number, { description: 'Number of job tasks to run concurrently (default equal to --replicas)' })
  maxConcurrent?: number;

  @Field(() => [String], { description: 'Add or update a mount on a service' })
  mountAdd?: string[];

  @Field(() => [String], { description: 'Remove a mount by its target path' })
  mountRm?: string[];

  @Field(() => [String], { description: 'Add a network' })
  networkAdd?: string[];

  @Field(() => [String], { description: 'Remove a network' })
  networkRm?: string[];

  @Field(() => Boolean, { description: 'Disable any container-specified HEALTHCHECK' })
  noHealthcheck?: boolean;

  @Field(() => Boolean, { description: 'Do not query the registry to resolve image digest and supported platforms' })
  noResolveImage?: boolean;

  @Field(() => [String], { description: 'Add a placement preference' })
  placementPrefAdd?: string[];

  @Field(() => [String], { description: 'Remove a placement preference' })
  placementPrefRm?: string[];

  @Field(() => [String], { description: 'Add or update a published port' })
  publishAdd?: string[];

  @Field(() => [String], { description: 'Remove a published port by its target port' })
  publishRm?: string[];

  @Field(() => Boolean, { description: 'Suppress progress output' })
  quiet?: boolean;

  @Field(() => Boolean, { description: "Mount the container's root filesystem as read only" })
  readOnly?: boolean;

  @Field(() => Number, { description: 'Number of tasks' })
  replicas?: number;

  @Field(() => Number, { description: 'Maximum number of tasks per node (default 0 = unlimited)' })
  replicasMaxPerNode?: number;

  @Field(() => Number, { description: 'Reserve CPUs' })
  reserveCpu?: number;

  @Field(() => Number, { description: 'Reserve Memory' })
  reserveMemory?: number;

  @Field(() => String, { description: 'Restart when condition is met ("none", "on-failure", "any")' })
  restartCondition?: string;

  @Field(() => String, { description: 'Delay between restart attempts (ns|us|ms|s|m|h)' })
  restartDelay?: string;

  @Field(() => Number, { description: 'Maximum number of restarts before giving up' })
  restartMaxAttempts?: number;

  @Field(() => String, { description: 'Window used to evaluate the restart policy (ns|us|ms|s|m|h)' })
  restartWindow?: string;

  @Field(() => Boolean, { description: 'Rollback to previous specification' })
  rollback?: boolean;

  @Field(() => String, { description: 'Delay between task rollbacks (ns|us|ms|s|m|h)' })
  rollbackDelay?: string;

  @Field(() => String, { description: 'Action on rollback failure ("pause", "continue")' })
  rollbackFailureAction?: string;

  @Field(() => Number, { description: 'Failure rate to tolerate during a rollback' })
  rollbackMaxFailureRatio?: number;

  @Field(() => String, { description: 'Duration after each task rollback to monitor for failure (ns|us|ms|s|m|h)' })
  rollbackMonitor?: string;

  @Field(() => String, { description: 'Rollback order ("start-first", "stop-first")' })
  rollbackOrder?: string;

  @Field(() => Number, {
    description: 'Maximum number of tasks rolled back simultaneously (0 to roll back all at once)',
  })
  rollbackParallelism?: number;

  @Field(() => [String], { description: 'Add or update a secret on a service' })
  secretAdd?: string[];

  @Field(() => [String], { description: 'Remove a secret' })
  secretRm?: string[];

  @Field(() => String, { description: 'Time to wait before force killing a container (ns|us|ms|s|m|h)' })
  stopGracePeriod?: string;

  @Field(() => String, { description: 'Signal to stop the container' })
  stopSignal?: string;

  @Field(() => [String], { description: 'Add or update a Sysctl option' })
  sysctlAdd?: string[];

  @Field(() => [String], { description: 'Remove a Sysctl option' })
  sysctlRm?: string[];

  @Field(() => Boolean, { description: 'Allocate a pseudo-TTY' })
  tty?: boolean;

  @Field(() => [String], { description: 'Add or update a ulimit option (default [])' })
  ulimitAdd?: string[];

  @Field(() => [String], { description: 'Remove a ulimit option' })
  ulimitRm?: string[];

  @Field(() => String, { description: 'Delay between updates (ns|us|ms|s|m|h)' })
  updateDelay?: string;

  @Field(() => String, { description: 'Action on update failure ("pause", "continue", "rollback")' })
  updateFailureAction?: string;

  @Field(() => Number, { description: 'Failure rate to tolerate during an update' })
  updateMaxFailureRatio?: number;

  @Field(() => String, { description: 'Duration after each task update to monitor for failure (ns|us|ms|s|m|h)' })
  updateMonitor?: string;

  @Field(() => String, { description: 'Update order ("start-first", "stop-first")' })
  updateOrder?: string;

  @Field(() => Number, { description: 'Maximum number of tasks updated simultaneously (0 to update all at once)' })
  updateParallelism?: number;

  @Field(() => String, { description: 'Username or UID (format: <name|uid>[:<group|gid>])' })
  user?: string;

  @Field(() => Boolean, { description: 'Send registry authentication details to swarm agents' })
  withRegistryAuth?: boolean;

  @Field(() => String, { description: 'Working directory inside the container' })
  workdir?: string;

  @Field(() => String, { description: 'Service name' })
  name: string;
}

@ObjectType()
export class DockerServiceUpdateResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  mode?: string;

  @Field(() => String, { nullable: true })
  ports?: string;

  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerServiceLsArgs {
  @Field(() => String, { nullable: true, description: 'filter output based on conditions provided' })
  filter?: string[];
}

@ObjectType()
export class DockerServiceLsResult {
  @Field(() => String, { nullable: true, description: 'ID of the service' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'name of the service' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'mode of the service' })
  mode?: string;

  @Field(() => String, { nullable: true, description: 'number of replicas' })
  replicas?: string;

  @Field(() => String, { nullable: true, description: 'image of the service' })
  image?: string;

  @Field(() => String, { nullable: true, description: 'ports of the service' })
  ports?: string;
}

@ArgsType()
export class DockerServiceRmArgs {
  @Field(() => String, { description: 'name of the stack' })
  name?: string;

  @Field(() => [String], { nullable: true, description: 'name of the stacks' })
  names?: string[];
}

@ObjectType()
export class DockerServiceRmResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerServiceScaleArgs {
  @Field(() => String, { description: 'name of the service' })
  name: string;

  @Field(() => Number, { description: 'number of replicas' })
  replicas: number;

  @Field(() => Boolean, {
    nullable: true,
    description: 'exit immediately instead of waiting for the service to converge',
  })
  detach?: boolean;
}

@ObjectType()
export class DockerServiceScaleResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  mode?: string;

  @Field(() => String, { nullable: true })
  replicas?: string;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  ports?: string;
}

@ArgsType()
export class DockerServiceLogsArgs {
  @Field(() => String, { description: 'name of the service' })
  name: string;

  @Field(() => Boolean, { nullable: true, description: 'show extra details provided to logs' })
  details?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'follow log output' })
  follow?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'do not map IDs to Names in output' })
  noResolve?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'do not include task IDs in output' })
  noTaskIds?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'do not truncate output' })
  noTrunc?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'do not neatly format logs' })
  raw?: boolean;

  @Field(() => String, {
    nullable: true,
    description: 'show logs since timestamp (e.g. "2013-01-02T13:23:37Z") or relative (e.g. "42m" for 42 minutes)',
  })
  since?: string;

  @Field(() => String, { nullable: true, description: 'number of lines to show from the end of the logs' })
  tail?: string;

  @Field(() => Boolean, { nullable: true, description: 'show timestamps' })
  timestamps?: boolean;
}

@ObjectType()
export class DockerServiceLogsResult {
  @Field(() => String, { nullable: true })
  logs?: string;
}

@ArgsType()
export class DockerServiceRollbackArgs {
  @Field(() => String, { description: 'name of the service' })
  name: string;

  @Field(() => Boolean, {
    nullable: true,
    description: 'exit immediately instead of waiting for the service to converge',
  })
  detach?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'suppress progress output' })
  quiet?: boolean;
}

@ObjectType()
export class DockerServiceRollbackResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerServiceInspectArgs {
  @Field(() => String, { description: 'name of the service' })
  name: string;
}

@ObjectType()
export class DockerServiceInspectResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => DockerServiceInspectVersion, { nullable: true })
  version?: DockerServiceInspectVersion;

  @Field(() => String, { nullable: true })
  createdAt?: string;

  @Field(() => String, { nullable: true })
  updatedAt?: string;

  @Field(() => DockerServiceInspectSpec, { nullable: true })
  spec?: DockerServiceInspectSpec;

  @Field(() => DockerServiceInspectEndpoint, { nullable: true })
  endpoint?: DockerServiceInspectEndpoint;
}

@ObjectType()
export class DockerServiceInspectVersion {
  @Field(() => Number, { nullable: true })
  index?: number;
}

@ObjectType()
export class DockerServiceInspectSpec {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => DockerServiceInspectLabels, { nullable: true })
  labels?: DockerServiceInspectLabels;

  @Field(() => DockerServiceInspectTaskTemplate, { nullable: true })
  taskTemplate?: DockerServiceInspectTaskTemplate;

  @Field(() => DockerServiceInspectMode, { nullable: true })
  mode?: DockerServiceInspectMode;

  @Field(() => DockerServiceInspectUpdateConfig, { nullable: true })
  updateConfig?: DockerServiceInspectUpdateConfig;

  @Field(() => DockerServiceInspectRollbackConfig, { nullable: true })
  rollbackConfig?: DockerServiceInspectRollbackConfig;

  @Field(() => DockerServiceInspectEndpointSpec, { nullable: true })
  endpointSpec?: DockerServiceInspectEndpointSpec;
}

@ObjectType()
export class DockerServiceInspectLabels {
  @Field(() => String, { nullable: true })
  key?: string;
}

@ObjectType()
export class DockerServiceInspectTaskTemplate {
  @Field(() => DockerServiceInspectContainerSpec, { nullable: true })
  containerSpec?: DockerServiceInspectContainerSpec;

  // @Field(() => DockerServiceInspectResources, { nullable: true })
  // resources?: DockerServiceInspectResources;

  @Field(() => DockerServiceInspectRestartPolicy, { nullable: true })
  restartPolicy?: DockerServiceInspectRestartPolicy;

  @Field(() => DockerServiceInspectPlacement, { nullable: true })
  placement?: DockerServiceInspectPlacement;

  @Field(() => DockerServiceInspectNetworks, { nullable: true })
  networks?: DockerServiceInspectNetworks;

  @Field(() => Number, { nullable: true })
  forceUpdate?: number;

  @Field(() => String, { nullable: true })
  runtime?: string;
}

@ObjectType()
export class DockerServiceInspectContainerSpec {
  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => DockerServiceInspectLabels, { nullable: true })
  labels?: DockerServiceInspectLabels;

  @Field(() => [String], { nullable: true })
  env?: string[];

  // @Field(() => DockerServiceInspectPrivileges, { nullable: true })
  // privileges?: DockerServiceInspectPrivileges;

  @Field(() => Number, { nullable: true })
  stopGracePeriod?: number;

  // @Field(() => DockerServiceInspectDNSConfig, { nullable: true })
  // dnsConfig?: DockerServiceInspectDNSConfig;

  @Field(() => String, { nullable: true })
  isolation?: string;
}

@ObjectType()
// export class DockerServiceInspectPrivileges {
//   @Field(() => DockerServiceInspectCredentialSpec, { nullable: true })
//   credentialSpec?: DockerServiceInspectCredentialSpec;

//   @Field(() => DockerServiceInspectSELinuxContext, { nullable: true })
//   sELinuxContext?: DockerServiceInspectSELinuxContext;
// }

// @ObjectType()
// export class DockerServiceInspectCredentialSpec {}

// @ObjectType()
// export class DockerServiceInspectSELinuxContext {}

// @ObjectType()
// export class DockerServiceInspectDNSConfig {}

// @ObjectType()
// export class DockerServiceInspectResources {}

@ObjectType()
export class DockerServiceInspectRestartPolicy {
  @Field(() => String, { nullable: true })
  condition?: string;

  @Field(() => Number, { nullable: true })
  delay?: number;

  @Field(() => Number, { nullable: true })
  maxAttempts?: number;
}

@ObjectType()
export class DockerServiceInspectPlacement {
  @Field(() => [DockerServiceInspectPlatform], { nullable: true })
  platforms?: DockerServiceInspectPlatform[];
}

@ObjectType()
export class DockerServiceInspectPlatform {
  @Field(() => String, { nullable: true })
  architecture?: string;

  @Field(() => String, { nullable: true })
  os?: string;
}

@ObjectType()
export class DockerServiceInspectNetworks {
  @Field(() => [DockerServiceInspectNetwork], { nullable: true })
  networks?: DockerServiceInspectNetwork[];
}

@ObjectType()
export class DockerServiceInspectNetwork {
  @Field(() => String, { nullable: true })
  target?: string;

  @Field(() => [String], { nullable: true })
  aliases?: string[];
}

@ObjectType()
export class DockerServiceInspectEndpoint {
  @Field(() => DockerServiceInspectSpec, { nullable: true })
  spec?: DockerServiceInspectSpec;

  @Field(() => [DockerServiceInspectPort], { nullable: true })
  ports?: DockerServiceInspectPort[];

  @Field(() => [DockerServiceInspectVirtualIP], { nullable: true })
  virtualIPs?: DockerServiceInspectVirtualIP[];
}

@ObjectType()
export class DockerServiceInspectPort {
  @Field(() => String, { nullable: true })
  protocol?: string;

  @Field(() => Number, { nullable: true })
  targetPort?: number;

  @Field(() => Number, { nullable: true })
  publishedPort?: number;

  @Field(() => String, { nullable: true })
  publishMode?: string;
}

@ObjectType()
export class DockerServiceInspectVirtualIP {
  @Field(() => String, { nullable: true })
  networkID?: string;

  @Field(() => String, { nullable: true })
  addr?: string;
}

@ObjectType()
export class DockerServiceInspectMode {
  @Field(() => DockerServiceInspectReplicated, { nullable: true })
  replicated?: DockerServiceInspectReplicated;
}

@ObjectType()
export class DockerServiceInspectReplicated {
  @Field(() => Number, { nullable: true })
  replicas?: number;
}

@ObjectType()
export class DockerServiceInspectUpdateConfig {
  @Field(() => Number, { nullable: true })
  parallelism?: number;

  @Field(() => String, { nullable: true })
  failureAction?: string;

  @Field(() => Number, { nullable: true })
  monitor?: number;

  @Field(() => Number, { nullable: true })
  maxFailureRatio?: number;

  @Field(() => String, { nullable: true })
  order?: string;
}

@ObjectType()
export class DockerServiceInspectRollbackConfig {
  @Field(() => Number, { nullable: true })
  parallelism?: number;

  @Field(() => String, { nullable: true })
  failureAction?: string;

  @Field(() => Number, { nullable: true })
  monitor?: number;

  @Field(() => Number, { nullable: true })
  maxFailureRatio?: number;

  @Field(() => String, { nullable: true })
  order?: string;
}

@ObjectType()
export class DockerServiceInspectEndpointSpec {
  @Field(() => String, { nullable: true })
  mode?: string;

  @Field(() => [DockerServiceInspectPort], { nullable: true })
  ports?: DockerServiceInspectPort[];
}

@ArgsType()
export class DockerServiceExecArgs {
  @Field(() => String, { description: 'name of the service' })
  name: string;

  @Field(() => String, { description: 'command to be executed' })
  command: string;

  @Field(() => [String], { nullable: true, description: 'arguments for the command' })
  args?: string[];

  @Field(() => Boolean, { nullable: true, description: 'detach mode' })
  detach?: boolean;

  @Field(() => [String], { nullable: true, description: 'environment variables' })
  env?: string[];

  @Field(() => Boolean, { nullable: true, description: 'keep stdin open even if not attached' })
  interactive?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'give extended privileges to the command' })
  privileged?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'allocate a pseudo-tty' })
  tty?: boolean;

  @Field(() => String, { nullable: true, description: 'username or uid' })
  user?: string;

  @Field(() => String, { nullable: true, description: 'working directory inside the container' })
  workdir?: string;

  @Field(() => Boolean, { nullable: true, description: 'run command on all containers' })
  all?: boolean;
}

@ObjectType()
export class DockerServiceExecResult {
  @Field(() => String)
  stdout: string;

  @Field(() => String)
  stderr: string;

  @Field(() => Number)
  exitCode: number;
}
