/*
 *  File: /docker/utils.ts
 *  Project: api
 *  File Created: 20-01-2024 11:30:16
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import camelCase from 'lodash.camelcase';

export function camelCaseKeys(obj: any) {
  if (Array.isArray(obj)) {
    return obj.map((val) => camelCaseKeys(val));
  } else if (obj !== null && obj.constructor === Object) {
    return Object.entries(obj).reduce((result, [key, value]) => {
      result[camelCase(key)] = camelCaseKeys(value);
      return result;
    }, {});
  } else {
    return obj;
  }
}
