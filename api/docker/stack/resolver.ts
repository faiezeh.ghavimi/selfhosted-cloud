/*
 *  File: /docker/stack/resolver.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerStackService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Query, Mutation, Resolver, Args } from 'type-graphql';
import {
  DockerStackDeployArgs,
  DockerStackDeployResult,
  DockerStackLsResult,
  DockerStackPsArgs,
  DockerStackPsResult,
  DockerStackRmArgs,
  DockerStackRmResult,
  DockerStackServicesArgs,
  DockerStackServicesResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerStackResolver {
  constructor(private readonly dockerStack: DockerStackService) {}

  @Mutation(() => DockerStackDeployResult)
  async dockerStackDeploy(@Args() args: DockerStackDeployArgs) {
    return this.dockerStack.deploy(args);
  }

  @Query(() => [DockerStackLsResult])
  async dockerStackLs() {
    return this.dockerStack.ls();
  }

  @Query(() => [DockerStackPsResult])
  async dockerStackPs(@Args() args: DockerStackPsArgs) {
    return this.dockerStack.ps(args);
  }

  @Mutation(() => DockerStackRmResult)
  async dockerStackRm(@Args() args: DockerStackRmArgs) {
    return this.dockerStack.rm(args);
  }

  @Query(() => [DockerStackServicesResult])
  async dockerStackServices(@Args() args: DockerStackServicesArgs) {
    return this.dockerStack.services(args);
  }
}
