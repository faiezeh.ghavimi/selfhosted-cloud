/*
 *  File: /docker/stack/dto.ts
 *  Project: api
 *  File Created: 19-01-2024 09:43:03
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ArgsType, Field, ObjectType, registerEnumType } from 'type-graphql';

@ArgsType()
export class DockerStackDeployArgs {
  @Field(() => String, { description: 'name of the stack' })
  name: string;

  @Field(() => String, { nullable: true, description: 'path to a compose file' })
  composeFile?: string;

  @Field(() => Boolean, { nullable: true, description: 'prune services that are no longer referenced' })
  prune?: boolean;

  @Field(() => ResolveImageEnum, {
    nullable: true,
    description: 'query the registry to resolve image digest and supported platforms',
  })
  resolveImage?: ResolveImageEnum;

  @Field(() => Boolean, { nullable: true, description: 'send registry authentication details to swarm agents' })
  withRegistryAuth?: boolean;
}

@ObjectType()
export class DockerStackDeployResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ObjectType()
export class DockerStackLsResult {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  services?: string;
}

@ArgsType()
export class DockerStackPsArgs {
  @Field(() => String, { description: 'name of the stack' })
  name?: string;

  @Field(() => [String], { nullable: true, description: 'filter output based on conditions provided' })
  filter?: string[];

  @Field(() => String, { nullable: true, description: 'Format output using a custom template' })
  format?: string;

  @Field(() => Boolean, { nullable: true, description: 'do not map ids to names' })
  noResolve?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'do not truncate output' })
  noTrunc?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'only display task IDs' })
  quiet?: boolean;
}

@ObjectType()
export class DockerStackPsResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  node?: string;

  @Field(() => String, { nullable: true })
  desiredState?: string;

  @Field(() => String, { nullable: true })
  currentState?: string;

  @Field(() => String, { nullable: true })
  error?: string;

  @Field(() => String, { nullable: true })
  ports?: string;
}

@ArgsType()
export class DockerStackRmArgs {
  @Field(() => String, { description: 'name of the stack' })
  name?: string;

  @Field(() => [String], { nullable: true, description: 'name of the stacks' })
  names?: string[];
}

@ObjectType()
export class DockerStackRmResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerStackServicesArgs {
  @Field(() => String, { description: 'name of the stack' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'filter output based on conditions provided' })
  filter?: string;
}

@ObjectType()
export class DockerStackServicesResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  mode?: string;

  @Field(() => String, { nullable: true })
  replicas?: string;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  ports?: string;
}

export enum ResolveImageEnum {
  ALWAYS = 'always',
  CHANGED = 'changed',
  NEVER = 'never',
}
registerEnumType(ResolveImageEnum, {
  name: 'ResolveImageEnum',
});
