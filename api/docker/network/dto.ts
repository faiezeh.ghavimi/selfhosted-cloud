/*
 *  File: /docker/network/dto.ts
 *  Project: api
 *  File Created: 31-01-2024 14:43:26
 *  Author: HariKrishna
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Field, ArgsType, Int, ObjectType } from 'type-graphql';
import { Flag } from '../types';

@ArgsType()
export class DockerNetworkLsArgs {
  @Field(() => String, { nullable: true, description: 'provide filter values (e.g. "driver=bridge")' })
  filter?: string;

  @Field(() => Boolean, { nullable: true, description: 'do not truncate the output' })
  noTrunc?: boolean;
}

@ObjectType()
export class DockerNetworkLsResult {
  @Field(() => String, { nullable: true, description: 'time when the network was created' })
  createdAt?: string;

  @Field(() => String, { nullable: true, description: 'IPv6 of the network' })
  ipv6?: string;

  @Field(() => String, { nullable: true, description: 'internal of the network' })
  internal?: string;

  @Field(() => String, { nullable: true, description: 'labels of the network' })
  labels?: string;

  @Field(() => String, { nullable: true, description: ' id of the network' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'driver of the network' })
  driver?: string;

  @Field(() => String, { nullable: true, description: 'name of the network' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'scope of the network' })
  scope?: string;
}

@ArgsType()
export class DockerNetworkRmArgs {
  @Field(() => [String], { nullable: true, description: 'Names of the networks' })
  networks: string[];

  @Field(() => Boolean, { nullable: true, description: 'Force the removal of the network' })
  force?: boolean;
}

@ObjectType()
export class DockerNetworkRmResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerNetworkCreateArgs {
  @Field(() => String, { nullable: true, description: 'name of the network' })
  name: string;

  @Field(() => String, { nullable: true, description: 'driver to manage the Network' })
  driver?: string;

  @Field(() => String, { nullable: true, description: "control the network's scope" })
  scope?: string;

  @Field(() => Boolean, { nullable: true, description: 'enable manual container attachment' })
  attachable?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'create swarm routing-mesh network' })
  ingress?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'restrict external access to the network' })
  internal?: boolean;

  @Field(() => [String], { nullable: true, description: 'allocate container ip from a sub-range' })
  ipRange?: string[];

  @Field(() => String, { nullable: true, description: 'ip address Management Driver' })
  ipamDriver?: string;

  @Field(() => [String], { nullable: true, description: 'set IPAM driver specific options' })
  ipamOpt?: string[];

  @Field(() => Boolean, { nullable: true, description: 'enable IPv6 networking' })
  ipv6?: boolean;

  @Field(() => [String], { nullable: true, description: 'set metadata on a network' })
  labels?: string[];

  @Field(() => [String], { nullable: true, description: 'subnet in CIDR format that represents a network segment' })
  subnet?: string[];

  @Field(() => [String], { nullable: true, description: 'auxiliary IPv4 or IPv6 addresses used by Network driver' })
  auxAddress?: string[];

  @Field(() => String, { nullable: true, description: 'the network from which to copy the configuration' })
  configFrom?: string;

  @Field(() => Boolean, { nullable: true, description: 'create a configuration only network' })
  configOnly?: boolean;

  @Field(() => [String], { nullable: true, description: 'IPv4 or IPv6 Gateway for the master subnet' })
  gateway?: string[];
}

@ObjectType()
export class DockerNetworkCreateResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerNetworkInspectArgs {
  @Field(() => [String], { nullable: true, description: 'Names of the networks' })
  networks: string[];

  @Field(() => Boolean, { nullable: true, description: 'Show verbose information about the networks' })
  verbose?: boolean;
}

@ObjectType()
export class DockerNetworkInspectResult {
  @Field(() => String, { nullable: true, description: 'name of the network' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'id of the network' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'time when the network was created' })
  created?: string;

  @Field(() => String, { nullable: true, description: 'scope of the network' })
  scope?: string;

  @Field(() => String, { nullable: true, description: 'driver of the network' })
  driver?: string;

  @Field(() => Boolean, { nullable: true, description: 'enable IPv6 on the network' })
  enableIPv6?: boolean;

  @Field(() => String, { nullable: true, description: 'IPAM of the network' })
  ipam?: string;

  @Field(() => Boolean, { nullable: true, description: 'internal of the network' })
  internal?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'attachable of the network' })
  attachable?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'ingress of the network' })
  ingress?: boolean;

  @Field(() => String, { nullable: true, description: 'config from of the network' })
  configFrom?: string;

  @Field(() => Boolean, { nullable: true, description: 'config only of the network' })
  configOnly?: boolean;

  @Field(() => String, { nullable: true, description: 'containers of the network' })
  containers?: string;

  @Field(() => String, { nullable: true, description: 'options of the network' })
  options?: string;

  @Field(() => String, { nullable: true, description: 'labels of the network' })
  labels?: string;
}

@ArgsType()
export class DockerNetworkPruneArgs {
  @Field(() => Boolean, { nullable: true, description: 'Remove all unused networks' })
  force?: boolean;

  @Field(() => String, { nullable: true, description: 'Provide filter values (e.g. "driver=bridge")' })
  filter?: string;
}

@ObjectType()
export class DockerNetworkPruneResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerNetworkConnectArgs {
  @Field(() => String, { nullable: true, description: 'name of the network' })
  network: string;

  @Field(() => String, { nullable: true, description: 'container name or id' })
  container: string;

  @Field(() => [String], { nullable: true, description: 'Add network-scoped alias for the container' })
  alias?: string[];

  @Field(() => [String], { nullable: true, description: 'driver options for the network' })
  driverOpt?: string[];

  @Field(() => String, { nullable: true, description: 'IPv4 address' })
  ipv4?: string;

  @Field(() => String, { nullable: true, description: 'IPv6 address' })
  ipv6?: string;

  @Field(() => [String], { nullable: true, description: 'Add link to another container' })
  link?: string[];

  @Field(() => [String], { nullable: true, description: 'Add a link-local address for the container' })
  linkLocalIp?: string[];
}

@ObjectType()
export class DockerNetworkConnectResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerNetworkDisconnectArgs {
  @Field(() => String, { nullable: true, description: 'name of the network' })
  network: string;

  @Field(() => String, { nullable: true, description: 'container name or id' })
  container: string;

  @Field(() => Boolean, { nullable: true, description: 'Force the container to disconnect from a network' })
  force?: boolean;
}

@ObjectType()
export class DockerNetworkDisconnectResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}
