/*
 *  File: /docker/network/types.ts
 *  Project: api
 *  File Created: 31-01-2024 15:13:57
 *  Author: HariKrishna
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export interface IDockerNetworkLs {
  CreatedAt?: string;
  Driver?: string;
  ID?: string;
  IPv6?: string;
  Internal?: string;
  Labels?: string;
  Name?: string;
  Scope?: string;
}

export interface IDockerNetworkInspect {
  Name?: string;
  Id?: string;
  Created?: string;
  Scope?: string;
  Driver?: string;
  EnableIPv6?: boolean;
  IPAM?: {
    Driver?: string;
    Options?: any;
    Config?: {
      Subnet?: string;
      Gateway?: string;
    }[];
  };
  Internal?: boolean;
  Attachable?: boolean;
  Ingress?: boolean;
  ConfigFrom?: {
    Network?: string;
  };
  ConfigOnly?: boolean;
  Containers?: any;
  Options?: any;
  Labels?: {
    [key: string]: string;
  };
}
