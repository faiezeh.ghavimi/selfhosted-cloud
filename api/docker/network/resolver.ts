/*
 *  File: /docker/network/resolver.ts
 *  Project: api
 *  File Created: 31-01-2024 14:44:23
 *  Author: HariKrishna
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerNetworkService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Mutation, Resolver, Args, Query } from 'type-graphql';
import {
  DockerNetworkConnectArgs,
  DockerNetworkConnectResult,
  DockerNetworkCreateArgs,
  DockerNetworkCreateResult,
  DockerNetworkDisconnectArgs,
  DockerNetworkDisconnectResult,
  DockerNetworkInspectArgs,
  DockerNetworkInspectResult,
  DockerNetworkLsArgs,
  DockerNetworkLsResult,
  DockerNetworkPruneArgs,
  DockerNetworkPruneResult,
  DockerNetworkRmArgs,
  DockerNetworkRmResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerNetworkResolver {
  constructor(private readonly dockerNetwork: DockerNetworkService) {}

  @Query(() => [DockerNetworkLsResult])
  async dockerNetworkLs(@Args(() => DockerNetworkLsArgs) args: DockerNetworkLsArgs) {
    return this.dockerNetwork.ls(args);
  }

  @Mutation(() => DockerNetworkRmResult)
  async dockerNetworkRm(@Args(() => DockerNetworkRmArgs) args: DockerNetworkRmArgs) {
    return this.dockerNetwork.rm(args);
  }

  @Mutation(() => DockerNetworkCreateResult)
  async dockerNetworkCreate(@Args(() => DockerNetworkCreateArgs) args: DockerNetworkCreateArgs) {
    return this.dockerNetwork.create(args);
  }

  @Query(() => [DockerNetworkInspectResult])
  async dockerNetworkInspect(@Args(() => DockerNetworkInspectArgs) args: DockerNetworkInspectArgs) {
    return this.dockerNetwork.inspect(args);
  }

  @Mutation(() => DockerNetworkPruneResult)
  async dockerNetworkPrune(@Args(() => DockerNetworkPruneArgs) args: DockerNetworkPruneArgs) {
    return this.dockerNetwork.prune(args);
  }

  @Mutation(() => DockerNetworkConnectResult)
  async dockerNetworkConnect(@Args(() => DockerNetworkConnectArgs) args: DockerNetworkConnectArgs) {
    return this.dockerNetwork.connect(args);
  }

  @Mutation(() => DockerNetworkDisconnectResult)
  async dockerNetworkDisconnect(@Args(() => DockerNetworkDisconnectArgs) args: DockerNetworkDisconnectArgs) {
    return this.dockerNetwork.disconnect(args);
  }
}
