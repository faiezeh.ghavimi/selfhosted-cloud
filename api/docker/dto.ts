/*
 *  File: /docker/dto.ts
 *  File: /docker/dto.ts
 *  File Created: 29-01-2024 15:52:41
 *  Author: HariKrishna
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/* eslint-disable max-lines */
/*
 *  File: /docker/dto.ts
 *  Project: api
 *  File Created: 29-01-2024 15:52:41
 *  Author: HariKrishna
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { GraphQLJSONObject } from 'graphql-scalars';
import { ArgsType, Field, ObjectType } from 'type-graphql';

@ArgsType()
export class DockerPsArgs {
  @Field(() => [String], { nullable: true, description: 'filter output based on conditions provided' })
  filter?: string[];

  @Field(() => Boolean, { nullable: true, description: 'do not map ids to names' })
  noResolve?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'do not truncate output' })
  noTrunc?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'show all containers (default shows just running)' })
  all?: boolean;

  @Field(() => Number, { nullable: true, description: 'show n last created containers (includes all states)' })
  last?: number;

  @Field(() => Boolean, { nullable: true, description: 'show the latest created container (includes all states)' })
  latest?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'display total file sizes' })
  size?: boolean;
}

@ObjectType()
export class DockerPsResult {
  @Field(() => String, { nullable: true })
  command?: string;

  @Field(() => String, { nullable: true })
  createdAt?: string;

  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  labels?: string;

  @Field(() => String, { nullable: true })
  localVolumes?: string;

  @Field(() => String, { nullable: true })
  mounts?: string;

  @Field(() => String, { nullable: true })
  names?: string;

  @Field(() => String, { nullable: true })
  networks?: string;

  @Field(() => String, { nullable: true })
  ports?: string;

  @Field(() => String, { nullable: true })
  runningFor?: string;

  @Field(() => String, { nullable: true })
  size?: string;

  @Field(() => String, { nullable: true })
  state?: string;

  @Field(() => String, { nullable: true })
  status?: string;
}

@ArgsType()
export class DockerUpdateArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => Number, {
    nullable: true,
    description: 'Block IO (relative weight), between 10 and 1000, or 0 to disable (default 0)',
  })
  blkioWeight?: number;

  @Field(() => Number, { nullable: true, description: 'Limit CPU CFS (Completely Fair Scheduler) period' })
  cpuPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'Limit CPU CFS (Completely Fair Scheduler) quota' })
  cpuQuota?: number;

  @Field(() => Number, { nullable: true, description: 'Limit the CPU real-time period in microseconds' })
  cpuRtPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'Limit the CPU real-time runtime in microseconds' })
  cpuRtRuntime?: number;

  @Field(() => Number, { nullable: true, description: 'CPU shares (relative weight)' })
  cpuShares?: number;

  @Field(() => Number, { nullable: true, description: 'Number of CPUs' })
  cpus?: number;

  @Field(() => String, { nullable: true, description: 'CPUs in which to allow execution (0-3, 0,1)' })
  cpusetCpus?: string;

  @Field(() => String, { nullable: true, description: 'MEMs in which to allow execution (0-3, 0,1)' })
  cpusetMems?: string;

  @Field(() => String, { nullable: true, description: 'Memory limit' })
  memory?: string;

  @Field(() => String, { nullable: true, description: 'Memory soft limit' })
  memoryReservation?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Swap limit equal to memory plus swap: -1 to enable unlimited swap',
  })
  memorySwap?: string;

  @Field(() => Number, { nullable: true, description: 'Tune container pids limit (set -1 for unlimited)' })
  pidsLimit?: number;

  @Field(() => String, { nullable: true, description: 'Restart policy to apply when a container exits' })
  restart?: string;

  @Field(() => String, { nullable: true, description: 'Runtime to use for this container' })
  runtime?: string;
}

@ObjectType()
export class DockerUpdateResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerImportArgs {
  @Field(() => String, {
    description: 'File, URL, or - to import contents from a tarball to create a filesystem image',
  })
  source: string;

  @Field(() => String, { description: 'repository name and tag' })
  repository: string;

  @Field(() => [String], { nullable: true, description: 'Apply Dockerfile instruction to the created image' })
  change?: string[];

  @Field(() => String, { nullable: true, description: 'Set commit message for imported image' })
  message?: string;

  @Field(() => String, { nullable: true, description: 'Set platform if server is multi-platform capable' })
  platform?: string;
}

@ObjectType()
export class DockerImportResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerTopArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => [String], { nullable: true, description: 'filter output based on conditions provided' })
  filter?: string[];
}

@ObjectType()
export class DockerTopResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerBuildArgs {
  @Field(() => String, { description: 'path to the dockerfile' })
  path: string;

  @Field(() => String, { nullable: true, description: 'name of the dockerfile' })
  file?: string;

  @Field(() => [String], { nullable: true, description: 'build arguments' })
  buildArg?: string[];

  @Field(() => [String], { nullable: true, description: 'set metadata for an image' })
  label?: string[];

  @Field(() => String, { nullable: true, description: 'write the image id to the file' })
  iidfile?: string;

  @Field(() => String, {
    nullable: true,
    description: 'set the networking mode for the "RUN" instructions during build',
  })
  network?: string;

  @Field(() => Boolean, { nullable: true, description: 'do not use cache when building the image' })
  noCache?: boolean;

  @Field(() => [String], { nullable: true, description: 'do not cache specified stages' })
  noCacheFilter?: string[];

  @Field(() => String, { nullable: true, description: 'set target platform for build' })
  platform?: string;

  @Field(() => String, { nullable: true, description: 'set type of progress output' })
  progress?: string;

  @Field(() => Boolean, { nullable: true, description: 'always attempt to pull all referenced images' })
  pull?: boolean;

  @Field(() => String, { nullable: true, description: 'size of "/dev/shm"' })
  shmSize?: string;

  @Field(() => [String], { nullable: true, description: 'ssh agent socket or keys to expose to the build' })
  ssh?: string[];

  @Field(() => String, { nullable: true, description: 'set the target build stage to build' })
  target?: string;

  @Field(() => String, { nullable: true, description: 'ulimit options' })
  ulimit?: string[];

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  addHost?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  annotation?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  attest?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  buildContext?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  builder?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  cacheFrom?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  cacheTo?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  cgroupParent?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  load?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  metadataFile?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  output?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  provenance?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  sbom?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  push?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  secret?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'print usage' })
  tag?: boolean;
}

@ObjectType()
export class DockerBuildResult {
  @Field(() => String, { nullable: true, description: 'id of the built image' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'warning messages during build' })
  warning?: string;

  @Field(() => String, { nullable: true, description: 'error messages during build' })
  stdout?: string;

  @Field(() => String, { nullable: true, description: 'error messages during build' })
  stderr?: string;
}

@ArgsType()
export class DockerStatsArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => Boolean, { nullable: true, description: 'Show all containers (default shows just running)' })
  all?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Disable streaming stats and only pull the first result' })
  noStream?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Do not truncate output' })
  noTrunc?: boolean;
}

@ObjectType()
export class DockerStatsResult {
  @Field(() => String, { nullable: true, description: 'id of the container' })
  containerId?: string;

  @Field(() => String, { nullable: true, description: 'Name of the container' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'CPU usage in percentage' })
  cpuPerc?: string;

  @Field(() => String, { nullable: true, description: 'Memory usage' })
  memUsage?: string;

  @Field(() => String, { nullable: true, description: 'Memory usage in percentage' })
  memPerc?: string;

  @Field(() => String, { nullable: true, description: 'Network I/O' })
  netIO?: string;

  @Field(() => String, { nullable: true, description: 'Block I/O' })
  blockIO?: string;

  @Field(() => String, { nullable: true, description: 'number of pids' })
  pids?: string;

  @Field(() => String, { nullable: true, description: 'CPU usage' })
  cpuUsage?: string;

  @Field(() => String, { nullable: true, description: 'Memory limit' })
  memLimit?: string;

  @Field(() => String, { nullable: true, description: 'Memory usage limit in percentage' })
  memPercLimit?: string;

  @Field(() => String, { nullable: true, description: 'Network input' })
  netInput?: string;

  @Field(() => String, { nullable: true, description: 'Network output' })
  netOutput?: string;

  @Field(() => String, { nullable: true, description: 'Block input' })
  blockInput?: string;

  @Field(() => String, { nullable: true, description: 'Block output' })
  blockOutput?: string;

  @Field(() => String, { nullable: true, description: 'current number of pids' })
  pidsCurrent?: string;

  @Field(() => String, { nullable: true, description: 'limit of pids' })
  pidsLimit?: string;

  @Field(() => String, { nullable: true, description: 'percentage of pids' })
  pidsPercent?: string;

  @Field(() => String, { nullable: true, description: 'limit of pids in percentage' })
  pidsPercentLimit?: string;
}

@ArgsType()
export class DockerLoadArgs {
  @Field(() => String, { description: 'Read from tar archive file, instead of STDIN' })
  input: string;
}

@ObjectType()
export class DockerLoadResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerTagArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  source: string;

  @Field(() => String, { description: 'name or id of the docker object' })
  target: string;
}

@ObjectType()
export class DockerTagResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerSaveArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => String, { description: 'Write to a file, instead of STDOUT' })
  output: string;
}

@ObjectType()
export class DockerSaveResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => String, { nullable: true })
  output?: string;
}

@ArgsType()
export class DockerStartArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  nameOrId: string;

  @Field(() => Boolean, { nullable: true, description: 'attach STDOUT/STDERR and forward signals' })
  attach?: boolean;

  @Field(() => String, { nullable: true, description: 'override the key sequence for detaching a container' })
  detachKeys?: string;

  @Field(() => Boolean, { nullable: true, description: 'attach container STDIN' })
  interactive?: boolean;
}

@ObjectType()
export class DockerStartResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerStopArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  nameOrId: string;

  @Field(() => Number, { nullable: true, description: 'seconds to wait for stop before killing it' })
  time?: number;

  @Field(() => String, { nullable: true, description: 'Signal to send to the container' })
  signal?: string;
}

@ObjectType()
export class DockerStopResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerAttachArgs {
  @Field(() => Boolean, { nullable: true, description: 'do not attach STDIN' })
  noStdin?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'proxy all received signals to the process' })
  sigProxy?: boolean;

  @Field(() => String, { nullable: true, description: 'override the key sequence for detaching a container' })
  detachKeys?: string;

  @Field(() => String, { nullable: true, description: 'name or id of the docker object' })
  nameOrId: string;
}

@ObjectType()
export class DockerAttachResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerKillArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => String, { nullable: true, description: 'signal to send to the container' })
  signal?: string;
}

@ObjectType()
export class DockerKillResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerCreateArgs {
  @Field(() => String, { description: 'image to create the container from' })
  image: string;

  @Field(() => String, { nullable: true, description: 'command to run in the container' })
  command?: string;

  @Field(() => [String], { nullable: true, description: 'arguments for the command' })
  args?: string[];

  @Field(() => [String], { nullable: true, description: 'add a custom host-to-IP mapping (host:ip)' })
  addHost?: string[];

  @Field(() => [String], {
    nullable: true,
    description: 'add an annotation to the container (passed through to the OCI runtime)',
  })
  annotation?: string[];

  @Field(() => [String], { nullable: true, description: 'attach to STDIN, STDOUT or STDERR' })
  attach?: string[];

  @Field(() => Number, {
    nullable: true,
    description: 'block IO (relative weight), between 10 and 1000, or 0 to disable',
  })
  blkioWeight?: number;

  @Field(() => [String], { nullable: true, description: 'block IO weight (relative device weight)' })
  blkioWeightDevice?: string[];

  @Field(() => [String], { nullable: true, description: 'add Linux capabilities' })
  capAdd?: string[];

  @Field(() => [String], { nullable: true, description: 'drop Linux capabilities' })
  capDrop?: string[];

  @Field(() => String, { nullable: true, description: 'optional parent cgroup for the container' })
  cgroupParent?: string;

  @Field(() => String, { nullable: true, description: 'cgroup namespace to use (host|private)' })
  cgroupns?: string;

  @Field(() => String, { nullable: true, description: 'write the container id to the file' })
  cidfile?: string;

  @Field(() => Number, { nullable: true, description: 'limit cpu cfs (completely fair scheduler) period' })
  cpuPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'limit CPU CFS (Completely Fair Scheduler) quota' })
  cpuQuota?: number;

  @Field(() => Number, { nullable: true, description: 'limit CPU real-time period in microseconds' })
  cpuRtPeriod?: number;

  @Field(() => Number, { nullable: true, description: 'limit CPU real-time runtime in microseconds' })
  cpuRtRuntime?: number;

  @Field(() => Number, { nullable: true, description: 'CPU shares (relative weight)' })
  cpuShares?: number;

  @Field(() => Number, { nullable: true, description: 'number of CPUs' })
  cpus?: number;

  @Field(() => String, { nullable: true, description: 'CPUs in which to allow execution (0-3, 0,1)' })
  cpusetCpus?: string;

  @Field(() => String, { nullable: true, description: 'MEMs in which to allow execution (0-3, 0,1)' })
  cpusetMems?: string;

  @Field(() => [String], { nullable: true, description: 'add a host device to the container' })
  device?: string[];

  @Field(() => [String], { nullable: true, description: 'add a rule to the cgroup allowed devices list' })
  deviceCgroupRule?: string[];

  @Field(() => [String], { nullable: true, description: 'limit read rate (bytes per second) from a device' })
  deviceReadBps?: string[];

  @Field(() => [String], { nullable: true, description: 'limit read rate (IO per second) from a device' })
  deviceReadIops?: string[];

  @Field(() => [String], { nullable: true, description: 'limit write rate (bytes per second) to a device' })
  deviceWriteBps?: string[];

  @Field(() => [String], { nullable: true, description: 'limit write rate (IO per second) to a device' })
  deviceWriteIops?: string[];

  @Field(() => Boolean, { nullable: true, description: 'skip image verification' })
  disableContentTrust?: boolean;

  @Field(() => [String], { nullable: true, description: 'set custom DNS servers' })
  dns?: string[];

  @Field(() => [String], { nullable: true, description: 'set DNS options' })
  dnsOption?: string[];

  @Field(() => [String], { nullable: true, description: 'set custom DNS search domains' })
  dnsSearch?: string[];

  @Field(() => String, { nullable: true, description: 'container NIS domain name' })
  domainname?: string;

  @Field(() => String, { nullable: true, description: 'Override the default ENTRYPOINT of the image' })
  entrypoint?: string;

  @Field(() => [String], { nullable: true, description: 'Set environment variables' })
  env?: string[];

  @Field(() => [String], { nullable: true, description: 'Read in a file of environment variables' })
  envFile?: string[];

  @Field(() => [String], { nullable: true, description: 'Expose a port or a range of ports' })
  expose?: string[];

  @Field(() => String, { nullable: true, description: 'GPU devices to add to the container ("all" to pass all GPUs)' })
  gpus?: string;

  @Field(() => [String], { nullable: true, description: 'Add additional groups to join' })
  groupAdd?: string[];

  @Field(() => String, { nullable: true, description: 'Command to run to check health' })
  healthCmd?: string;

  @Field(() => String, { nullable: true, description: 'Time between running the check (ms|s|m|h)' })
  healthInterval?: string;

  @Field(() => Number, { nullable: true, description: 'Consecutive failures needed to report unhealthy' })
  healthRetries?: number;

  @Field(() => String, {
    nullable: true,
    description: 'Time between running the check during the start period (ms|s|m|h)',
  })
  healthStartInterval?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Start period for the container to initialize before starting health-retries countdown (ms|s|m|h)',
  })
  healthStartPeriod?: string;

  @Field(() => String, { nullable: true, description: 'Maximum time to allow one check to run (ms|s|m|h)' })
  healthTimeout?: string;

  @Field(() => Boolean, { nullable: true, description: 'Print usage' })
  help?: boolean;

  @Field(() => String, { nullable: true, description: 'Container host name' })
  hostname?: string;

  @Field(() => Boolean, {
    nullable: true,
    description: 'Run an init inside the container that forwards signals and reaps processes',
  })
  init?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Keep STDIN open even if not attached' })
  interactive?: boolean;

  @Field(() => String, { nullable: true, description: 'IPv4 address ' })
  ip?: string;

  @Field(() => String, { nullable: true, description: 'IPv6 address' })
  ip6?: string;

  @Field(() => String, { nullable: true, description: 'IPC mode to use' })
  ipc?: string;

  @Field(() => String, { nullable: true, description: 'Container isolation technology' })
  isolation?: string;

  @Field(() => String, { nullable: true, description: 'Kernel memory limit' })
  kernelMemory?: string;

  @Field(() => [String], { nullable: true, description: 'Set meta data on a container' })
  label?: string[];

  @Field(() => [String], { nullable: true, description: 'Read in a line delimited file of labels' })
  labelFile?: string[];

  @Field(() => [String], { nullable: true, description: 'Add link to another container' })
  link?: string[];

  @Field(() => [String], { nullable: true, description: 'Container IPv4/IPv6 link-local addresses' })
  linkLocalIp?: string[];

  @Field(() => String, { nullable: true, description: 'Logging driver for the container' })
  logDriver?: string;

  @Field(() => [String], { nullable: true, description: 'Log driver options' })
  logOpt?: string[];

  @Field(() => String, { nullable: true, description: 'Container MAC address (e.g., 92:d0:c6:0a:29:33)' })
  macAddress?: string;

  @Field(() => String, { nullable: true, description: 'Memory limit' })
  memory?: string;

  @Field(() => String, { nullable: true, description: 'Memory soft limit' })
  memoryReservation?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Swap limit equal to memory plus swap: "-1" to enable unlimited swap',
  })
  memorySwap?: string;

  @Field(() => Number, { nullable: true, description: 'Tune container memory swappiness (0 to 100)' })
  memorySwappiness?: number;

  @Field(() => [String], { nullable: true, description: 'Attach a filesystem mount to the container' })
  mount?: string[];

  @Field(() => String, { nullable: true, description: 'Assign a name to the container' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'Connect a container to a network' })
  network?: string;

  @Field(() => [String], { nullable: true, description: 'Add network-scoped alias for the container' })
  networkAlias?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Disable any container-specified HEALTHCHECK' })
  noHealthcheck?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Disable OOM Killer' })
  oomKillDisable?: boolean;

  @Field(() => Number, { nullable: true, description: "Tune host's OOM preferences (-1000 to 1000)" })
  oomScoreAdj?: number;

  @Field(() => String, { nullable: true, description: 'pid namespace to use' })
  pid?: string;

  @Field(() => Number, { nullable: true, description: 'Tune container pids limit (set -1 for unlimited)' })
  pidsLimit?: number;

  @Field(() => String, { nullable: true, description: 'Set platform if server is multi-platform capable' })
  platform?: string;

  @Field(() => Boolean, { nullable: true, description: 'Give extended privileges to this container' })
  privileged?: boolean;

  @Field(() => [String], { nullable: true, description: "Publish a container's port(s) to the host" })
  publish?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Publish all exposed ports to random ports' })
  publishAll?: boolean;

  @Field(() => String, { nullable: true, description: 'Pull image before creating ("always", "|missing", "never")' })
  pull?: string;

  @Field(() => Boolean, { nullable: true, description: "Mount the container's root filesystem as read only" })
  readOnly?: boolean;

  @Field(() => String, { nullable: true, description: 'Restart policy to apply when a container exits' })
  restart?: string;

  @Field(() => Boolean, { nullable: true, description: 'Automatically remove the container when it exits' })
  rm?: boolean;

  @Field(() => String, { nullable: true, description: 'Runtime to use for this container' })
  runtime?: string;

  @Field(() => [String], { nullable: true, description: 'Security Options' })
  securityOpt?: string[];

  @Field(() => String, { nullable: true, description: 'Size of /dev/shm' })
  shmSize?: string;

  @Field(() => String, { nullable: true, description: 'Signal to stop the container' })
  stopSignal?: string;

  @Field(() => Number, { nullable: true, description: 'Timeout (in seconds) to stop a container' })
  stopTimeout?: number;

  @Field(() => [String], { nullable: true, description: 'Storage driver options for the container' })
  storageOpt?: string[];

  @Field(() => [String], { nullable: true, description: 'Sysctl options' })
  sysctl?: string[];

  @Field(() => [String], { nullable: true, description: 'Mount a tmpfs directory' })
  tmpfs?: string[];

  @Field(() => Boolean, { nullable: true, description: 'Allocate a pseudo-TTY' })
  tty?: boolean;

  @Field(() => [String], { nullable: true, description: 'Ulimit options' })
  ulimit?: string[];

  @Field(() => String, { nullable: true, description: 'username or uid (format: <name|uid>[:<group|gid>])' })
  user?: string;

  @Field(() => String, { nullable: true, description: 'User namespace to use' })
  userns?: string;

  @Field(() => String, { nullable: true, description: 'UTS namespace to use' })
  uts?: string;

  @Field(() => [String], { nullable: true, description: 'Bind mount a volume' })
  volume?: string[];

  @Field(() => String, { nullable: true, description: 'Optional volume driver for the container' })
  volumeDriver?: string;

  @Field(() => [String], { nullable: true, description: 'Mount volumes from the specified container(s)' })
  volumesFrom?: string[];

  @Field(() => String, { nullable: true, description: 'Working directory inside the container' })
  workdir?: string;
}

@ObjectType()
export class DockerCreateResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerInfoArgs {
  @Field(() => String, { nullable: true, description: 'Format output using a custom template' })
  format?: string;
}

@ObjectType()
export class DockerInfoResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerExecArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => String, { description: 'command to be executed' })
  command: string;

  @Field(() => [String], { nullable: true, description: 'arguments for the command' })
  args?: string[];

  @Field(() => Boolean, { nullable: true, description: 'detach mode' })
  detach?: boolean;

  @Field(() => [String], { nullable: true, description: 'environment variables' })
  env?: string[];

  @Field(() => Boolean, { nullable: true, description: 'keep stdin open even if not attached' })
  interactive?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'give extended privileges to the command' })
  privileged?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'allocate a pseudo-tty' })
  tty?: boolean;

  @Field(() => String, { nullable: true, description: 'username or uid' })
  user?: string;

  @Field(() => String, { nullable: true, description: 'working directory inside the container' })
  workdir?: string;
}

@ObjectType()
export class DockerExecResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerCommitArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => String, { description: 'repository name' })
  repository: string;

  @Field(() => String, { description: 'tag name' })
  tag: string;

  @Field(() => String, { nullable: true, description: 'commit message' })
  message?: string;

  @Field(() => String, { nullable: true, description: 'author' })
  author?: string;

  @Field(() => Boolean, { nullable: true, description: 'pause container during commit' })
  pause?: boolean;

  @Field(() => [String], { nullable: true, description: 'apply Dockerfile instruction to the created image' })
  change?: string[];
}

@ObjectType()
export class DockerCommitResult {
  @Field(() => String, { nullable: true, description: 'id of the committed image' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'warning messages during commit' })
  warning?: string;
}

@ArgsType()
export class DockerCpArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => String, { description: 'path to file or directory to copy' })
  source: string;

  @Field(() => String, { description: 'path to directory' })
  destination: string;

  @Field(() => Boolean, { nullable: true, description: 'archive mode (copy all uid/gid information)' })
  archive?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'always follow symbol link in SRC_PATH' })
  followLink?: boolean;
}

@ObjectType()
export class DockerCpResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerEventsArgs {
  @Field(() => [String], { nullable: true, description: 'Filter values' })
  filter?: string[];

  @Field(() => String, { nullable: true, description: 'Format the output using the given go template' })
  format?: string;

  @Field(() => String, { nullable: true, description: 'Events created since this timestamp' })
  since?: string;

  @Field(() => String, { nullable: true, description: 'Events created until this timestamp' })
  until?: string;
}

@ObjectType()
export class DockerEventsResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerSearchArgs {
  @Field(() => String, { description: 'term to search' })
  term: string;

  @Field(() => String, { nullable: true, description: 'Filter output based on conditions provided' })
  filter?: string;

  @Field(() => String, { nullable: true, description: 'Pretty-print search using a Go template' })
  format?: string;

  @Field(() => Number, { nullable: true, description: 'Max number of search results' })
  limit?: number;

  @Field(() => Boolean, { nullable: true, description: "Don't truncate output" })
  noTrunc?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'only show image ids' })
  orderBy?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Show sizes' })
  stars?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Show sizes' })
  isOfficial?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Show sizes' })
  isAutomated?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Show sizes' })
  starCount?: boolean;
}

@ObjectType()
export class DockerSearchResult {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  description?: string;

  @Field(() => String, { nullable: true })
  starCount?: string;

  @Field(() => Boolean, { nullable: true })
  isOfficial?: boolean;

  @Field(() => Number, { nullable: true })
  isAutomated?: number;
}

@ArgsType()
export class DockerRmArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => Boolean, { nullable: true, description: 'remove the volumes associated with the container' })
  volumes?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'remove the specified link associated with the container' })
  link?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'remove the specified link associated with the container' })
  force?: boolean;
}

@ObjectType()
export class DockerRmResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerRmiArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => Boolean, { nullable: true, description: 'force removal of the image' })
  force?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'do not delete untagged parents' })
  noPrune?: boolean;
}

@ObjectType()
export class DockerRmiResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerImagesArgs {
  @Field(() => Boolean, { nullable: true, description: 'Show all images (default hides intermediate images)' })
  all?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Show digests' })
  digests?: boolean;

  @Field(() => String, { nullable: true, description: 'Filter output based on conditions provided' })
  filter?: string;

  @Field(() => String, {
    nullable: true,
    description:
      "Format output using a custom template: 'table': Print output in table format with column headers (default) 'table TEMPLATE': Print output in table format using the given Go template 'json': Print in JSON format 'TEMPLATE': Print output using the given Go template. Refer to https://docs.docker.com/go/formatting/ for more information about formatting output with templates",
  })
  format?: string;

  @Field(() => Boolean, { nullable: true, description: "Don't truncate output" })
  noTrunc?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Show sizes' })
  size?: boolean;
}

@ArgsType()
export class DockerVersionArgs {
  @Field(() => Boolean, { nullable: true, description: 'format the output' })
  format?: boolean;
}

@ObjectType()
export class DockerVersionResult {
  @Field(() => String, { nullable: true })
  version?: string;

  @Field(() => String, { nullable: true })
  apiVersion?: string;

  @Field(() => String, { nullable: true })
  minAPIVersion?: string;

  @Field(() => String, { nullable: true })
  gitCommit?: string;

  @Field(() => String, { nullable: true })
  goVersion?: string;

  @Field(() => String, { nullable: true })
  os?: string;

  @Field(() => String, { nullable: true })
  arch?: string;

  @Field(() => String, { nullable: true })
  kernelVersion?: string;

  @Field(() => String, { nullable: true })
  buildTime?: string;

  @Field(() => String, { nullable: true })
  experimental?: string;

  @Field(() => String, { nullable: true })
  buildTag?: string;

  @Field(() => String, { nullable: true })
  components?: string[];

  @Field(() => String, { nullable: true })
  commit?: string;

  @Field(() => String, { nullable: true })
  versionString?: string;
}

@ObjectType()
export class DockerImagesResult {
  @Field(() => String, { nullable: true })
  repository?: string;

  @Field(() => String, { nullable: true })
  tag?: string;

  @Field(() => String, { nullable: true })
  imageId?: string;

  @Field(() => String, { nullable: true })
  created?: string;

  @Field(() => String, { nullable: true })
  size?: string;
}

@ArgsType()
export class DockerWaitArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => String, { nullable: true, description: 'block until container stops, then print exit code' })
  format?: string;
}

@ObjectType()
export class DockerWaitResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => String, { nullable: true })
  statusCode?: string;
}

@ArgsType()
export class DockerPortArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => String, { description: 'port number' })
  port: string;
}

@ObjectType()
export class DockerPortResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerDiffArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;
}

@ObjectType()
export class DockerDiffResult {
  @Field(() => String, { nullable: true })
  type?: string;

  @Field(() => String, { nullable: true })
  path?: string;
}

@ArgsType()
export class DockerExportArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => String, { description: 'write to a file, instead of STDOUT' })
  output: string;
}

@ObjectType()
export class DockerExportResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerPullArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  name: string;

  @Field(() => Boolean, { nullable: true, description: 'download all tagged images in the repository' })
  allTags?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'skip image verification (default true)' })
  disableContentTrust?: boolean;

  @Field(() => String, { nullable: true, description: 'set platform if server is multi-platform capable' })
  platform?: string;
}

@ObjectType()
export class DockerPullResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerPushArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  name: string;

  @Field(() => Boolean, { nullable: true, description: 'push all tagged images in the repository' })
  allTags?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'skip image signing (default true)' })
  disableContentTrust?: boolean;
}

@ObjectType()
export class DockerPushResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerRenameArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  nameOrId: string;

  @Field(() => String, { description: 'new name' })
  newName: string;
}

@ObjectType()
export class DockerRenameResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerRestartArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  nameOrId: string;

  @Field(() => Number, { nullable: true, description: 'seconds to wait for stop before killing it' })
  time?: number;

  @Field(() => String, { nullable: true, description: 'Signal to send to the container' })
  signal?: string;
}

@ObjectType()
export class DockerRestartResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerLoginArgs {
  @Field(() => String, { description: 'username' })
  username: string;

  @Field(() => String, { description: 'password' })
  password: string;

  @Field(() => String, { nullable: true, description: 'server address' })
  serverAddress?: string;
}

@ObjectType()
export class DockerLoginResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => String, { nullable: true })
  status?: string;
}

@ArgsType()
export class DockerLogoutArgs {
  @Field(() => String, { nullable: true, description: 'server address' })
  serverAddress?: string;
}

@ObjectType()
export class DockerLogoutResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => String, { nullable: true })
  status?: string;
}

@ArgsType()
export class DockerPauseArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  nameOrId: string;

  @Field(() => [String], { nullable: true, description: 'one or more containers to pause' })
  containers?: string[];
}

@ObjectType()
export class DockerPauseResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => String, { nullable: true })
  status?: string;
}

@ArgsType()
export class DockerUnpauseArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  nameOrId: string;

  @Field(() => [String], { nullable: true, description: 'one or more containers to unpause' })
  containers?: string[];
}

@ObjectType()
export class DockerUnpauseResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;

  @Field(() => String, { nullable: true })
  status?: string;
}

@ArgsType()
export class DockerLogsArgs {
  @Field(() => String, { description: 'name or id of the docker container' })
  nameOrId: string;

  @Field(() => Boolean, { nullable: true, description: 'Show extra details provided to logs' })
  details?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Follow log output' })
  follow?: boolean;

  @Field(() => String, {
    nullable: true,
    description: 'Show logs since timestamp (e.g. "2013-01-02T13:23:37Z") or relative (e.g. "42m" for 42 minutes)',
  })
  since?: string;

  @Field(() => String, {
    nullable: true,
    description: 'Number of lines to show from the end of the logs (default "all")',
  })
  tail?: string;

  @Field(() => Boolean, { nullable: true, description: 'Show timestamps' })
  timestamps?: boolean;

  @Field(() => String, {
    nullable: true,
    description: 'Show logs before a timestamp (e.g. "2013-01-02T13:23:37Z") or relative (e.g. "42m" for 42 minutes)',
  })
  until?: string;
}

@ObjectType()
export class DockerLogsResult {
  @Field(() => String, { nullable: true })
  stdout?: string;

  @Field(() => String, { nullable: true })
  stderr?: string;
}

@ArgsType()
export class DockerHistoryArgs {
  @Field(() => String, { description: 'name or id of the docker image' })
  nameOrId: string;

  @Field(() => String, { nullable: true, description: 'Format output using a custom template' })
  format?: string;

  @Field(() => Boolean, { nullable: true, description: 'Print sizes and dates in human readable format' })
  human?: boolean;

  @Field(() => Boolean, { nullable: true, description: "Don't truncate output" })
  noTrunc?: boolean;
}

@ObjectType()
export class DockerHistoryResult {
  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  created?: string;

  @Field(() => String, { nullable: true })
  createdBy?: string;

  @Field(() => String, { nullable: true })
  size?: string;

  @Field(() => String, { nullable: true })
  comment?: string;
}

@ArgsType()
export class DockerInspectArgs {
  @Field(() => String, { description: 'name or id of the docker object' })
  nameOrId: string;

  @Field(() => Boolean, { nullable: true, description: 'display total file sizes if the type is container' })
  size?: boolean;
}

@ObjectType()
export class DockerInspectResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => String, { nullable: true })
  created?: string;

  @Field(() => String, { nullable: true })
  path?: string;

  @Field(() => [String], { nullable: true })
  args?: string[];

  @Field(() => DockerInspectResultState, { nullable: true })
  state?: DockerInspectResultState;

  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => String, { nullable: true })
  resolvConfPath?: string;

  @Field(() => String, { nullable: true })
  hostnamePath?: string;

  @Field(() => String, { nullable: true })
  hostsPath?: string;

  @Field(() => String, { nullable: true })
  logPath?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => Number, { nullable: true })
  restartCount?: number;

  @Field(() => String, { nullable: true })
  driver?: string;

  @Field(() => String, { nullable: true })
  platform?: string;

  @Field(() => String, { nullable: true })
  mountLabel?: string;

  @Field(() => String, { nullable: true })
  processLabel?: string;

  @Field(() => String, { nullable: true })
  appArmorProfile?: string;

  @Field(() => [String], { nullable: true })
  execIds?: string[];

  @Field(() => DockerInspectResultHostConfig, { nullable: true })
  hostConfig?: DockerInspectResultHostConfig;

  @Field(() => DockerInspectResultGraphDriver, { nullable: true })
  graphDriver?: DockerInspectResultGraphDriver;

  @Field(() => [DockerInspectResultMount], { nullable: true })
  mounts?: DockerInspectResultMount[];

  @Field(() => DockerInspectResultConfig, { nullable: true })
  config?: DockerInspectResultConfig;

  @Field(() => DockerInspectResultNetworkSettings, { nullable: true })
  networkSettings?: DockerInspectResultNetworkSettings;

  @Field(() => DockerInspectResultStatus, { nullable: true })
  status?: DockerInspectResultStatus;

  @Field(() => [DockerInspectResultNetworkAttachment], { nullable: true })
  networkAttachments?: DockerInspectResultNetworkAttachment[];

  @Field(() => DockerInspectResultSpec, { nullable: true })
  spec?: DockerInspectResultSpec;
}

@ObjectType()
export class DockerInspectResultSpec {
  @Field(() => DockerInspectResultContainerSpec, { nullable: true })
  containerSpec?: DockerInspectResultContainerSpec;

  @Field(() => GraphQLJSONObject, { nullable: true })
  resources?: Record<string, any>;

  @Field(() => DockerInspectResultRestartPolicy, { nullable: true })
  restartPolicy?: DockerInspectResultRestartPolicy;

  @Field(() => DockerInspectResultPlacement, { nullable: true })
  placement?: DockerInspectResultPlacement;

  @Field(() => [DockerInspectResultNetwork], { nullable: true })
  networks?: DockerInspectResultNetwork[];

  @Field(() => Number, { nullable: true })
  forceUpdate?: number;
}

@ObjectType()
export class DockerInspectResultContainerSpec {
  @Field(() => String, { nullable: true })
  image?: string;

  @Field(() => GraphQLJSONObject, { nullable: true })
  labels?: Record<string, string>;

  @Field(() => [String], { nullable: true })
  env?: string[];

  @Field(() => DockerInspectResultPrivileges, { nullable: true })
  privileges?: DockerInspectResultPrivileges;

  @Field(() => String, { nullable: true })
  isolation?: string;
}

@ObjectType()
export class DockerInspectResultPrivileges {
  @Field(() => String, { nullable: true })
  credentialSpec?: string;

  @Field(() => String, { nullable: true })
  seLinuxContext?: string;
}

@ObjectType()
export class DockerInspectResultRestartPolicy {
  @Field(() => String, { nullable: true })
  condition?: string;

  @Field(() => Number, { nullable: true })
  maxAttempts?: number;
}

@ObjectType()
export class DockerInspectResultPlacement {
  @Field(() => [DockerInspectResultPlatform], { nullable: true })
  platforms?: DockerInspectResultPlatform[];
}

@ObjectType()
export class DockerInspectResultPlatform {
  @Field(() => String, { nullable: true })
  architecture?: string;

  @Field(() => String, { nullable: true })
  os?: string;
}

@ObjectType()
export class DockerInspectResultNetwork {
  @Field(() => String, { nullable: true })
  target?: string;

  @Field(() => [String], { nullable: true })
  aliases?: string[];
}

@ObjectType()
export class DockerInspectResultStatus {
  @Field(() => String, { nullable: true })
  timestamp?: string;

  @Field(() => String, { nullable: true })
  state?: string;

  @Field(() => String, { nullable: true })
  message?: string;

  @Field(() => DockerInspectResultContainerStatus, { nullable: true })
  containerStatus?: DockerInspectResultContainerStatus;

  @Field(() => GraphQLJSONObject, { nullable: true })
  portStatus?: Record<string, { HostIp: string; HostPort: string }[]>;
}

@ObjectType()
export class DockerInspectResultContainerStatus {
  @Field(() => String, { nullable: true })
  containerId?: string;

  @Field(() => Number, { nullable: true })
  pid?: number;

  @Field(() => Number, { nullable: true })
  exitCode?: number;
}

@ObjectType()
export class DockerInspectResultNetworkAttachment {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => DockerInspectResultNetworkAttachmentSpec, { nullable: true })
  spec?: DockerInspectResultNetworkAttachmentSpec;

  @Field(() => [String], { nullable: true })
  addresses?: string[];
}

@ObjectType()
export class DockerInspectResultNetworkAttachmentSpec {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  driver?: string;

  @Field(() => [DockerInspectResultNetworkAttachmentConfig], { nullable: true })
  configs?: DockerInspectResultNetworkAttachmentConfig[];
}

@ObjectType()
export class DockerInspectResultNetworkAttachmentConfig {
  @Field(() => String, { nullable: true })
  subnet?: string;

  @Field(() => String, { nullable: true })
  gateway?: string;
}
@ObjectType()
export class DockerInspectResultState {
  @Field(() => String, { nullable: true })
  status?: string;

  @Field(() => Boolean, { nullable: true })
  running?: boolean;

  @Field(() => Boolean, { nullable: true })
  paused?: boolean;

  @Field(() => Boolean, { nullable: true })
  restarting?: boolean;

  @Field(() => Boolean, { nullable: true })
  oomKilled?: boolean;

  @Field(() => Boolean, { nullable: true })
  dead?: boolean;

  @Field(() => Number, { nullable: true })
  pid?: number;

  @Field(() => Number, { nullable: true })
  exitCode?: number;

  @Field(() => String, { nullable: true })
  error?: string;

  @Field(() => String, { nullable: true })
  startedAt?: string;

  @Field(() => String, { nullable: true })
  finishedAt?: string;
}

@ObjectType()
export class DockerInspectResultHostConfig {
  @Field(() => [String], { nullable: true })
  binds?: string[];

  @Field(() => String, { nullable: true })
  containerIdFile?: string;

  @Field(() => DockerInspectResultHostConfigLogConfig, { nullable: true })
  logConfig?: DockerInspectResultHostConfigLogConfig;

  @Field(() => String, { nullable: true })
  networkMode?: string;

  // @Field(() => Record)
  // portBindings: Record<string, { hostIp: string; hostPort: string }[]>;

  @Field(() => DockerInspectResultHostConfigRestartPolicy, { nullable: true })
  restartPolicy?: DockerInspectResultHostConfigRestartPolicy;

  @Field(() => Boolean, { nullable: true })
  autoRemove?: boolean;

  @Field(() => String, { nullable: true })
  volumeDriver?: string;

  @Field(() => [String], { nullable: true })
  volumesFrom?: string[];

  @Field(() => [Number], { nullable: true })
  consoleSize?: number[];

  @Field(() => [String], { nullable: true })
  capAdd?: string[];

  @Field(() => [String], { nullable: true })
  capDrop?: string[];

  @Field(() => String, { nullable: true })
  cgroupnsMode?: string;

  @Field(() => [String], { nullable: true })
  dns?: string[];

  @Field(() => [String], { nullable: true })
  dnsOptions?: string[];

  @Field(() => [String], { nullable: true })
  dnsSearch?: string[];

  @Field(() => [String], { nullable: true })
  extraHosts?: string[];

  @Field(() => [String], { nullable: true })
  groupAdd?: string[];

  // eslint-disable-next-line max-lines
  @Field(() => String, { nullable: true })
  ipcMode?: string;

  @Field(() => String, { nullable: true })
  cgroup?: string;

  @Field(() => [String], { nullable: true })
  links?: string[];

  @Field(() => Number, { nullable: true })
  oomScoreAdj?: number;

  @Field(() => String, { nullable: true })
  pidMode?: string;

  @Field(() => Boolean, { nullable: true })
  privileged?: boolean;

  @Field(() => Boolean, { nullable: true })
  publishAllPorts?: boolean;

  @Field(() => Boolean, { nullable: true })
  readonlyRootfs?: boolean;

  @Field(() => [String], { nullable: true })
  securityOpt?: string[];

  @Field(() => String, { nullable: true })
  utsMode?: string;

  @Field(() => String, { nullable: true })
  usernsMode?: string;

  @Field(() => Number, { nullable: true })
  shmSize?: number;

  @Field(() => String, { nullable: true })
  runtime?: string;

  @Field(() => String, { nullable: true })
  isolation?: string;

  @Field(() => Number, { nullable: true })
  cpuShares?: number;

  @Field(() => Number, { nullable: true })
  memory?: number;

  @Field(() => Number, { nullable: true })
  nanoCpus?: number;

  @Field(() => String, { nullable: true })
  cgroupParent?: string;

  @Field(() => Number, { nullable: true })
  blkioWeight?: number;

  @Field(() => [String], { nullable: true })
  blkioWeightDevice?: string[];

  @Field(() => [String], { nullable: true })
  blkioDeviceReadBps?: string[];

  @Field(() => [String], { nullable: true })
  blkioDeviceWriteBps?: string[];

  @Field(() => [String], { nullable: true })
  blkioDeviceReadIOps?: string[];

  @Field(() => [String], { nullable: true })
  blkioDeviceWriteIOps?: string[];

  @Field(() => Number, { nullable: true })
  cpuPeriod?: number;

  @Field(() => Number, { nullable: true })
  cpuQuota?: number;

  @Field(() => Number, { nullable: true })
  cpuRealtimePeriod?: number;

  @Field(() => Number, { nullable: true })
  cpuRealtimeRuntime?: number;

  @Field(() => String, { nullable: true })
  cpusetCpus?: string;

  @Field(() => String, { nullable: true })
  cpusetMems?: string;

  @Field(() => [String], { nullable: true })
  devices?: string[];

  @Field(() => [String], { nullable: true })
  deviceCgroupRules?: string[];

  @Field(() => [String], { nullable: true })
  deviceRequests?: string[];

  @Field(() => Number, { nullable: true })
  memoryReservation?: number;

  @Field(() => Number, { nullable: true })
  memorySwap?: number;

  @Field(() => Number, { nullable: true })
  memorySwappiness?: number;

  @Field(() => Boolean, { nullable: true })
  oomKillDisable?: boolean;

  @Field(() => Number, { nullable: true })
  pidsLimit?: number;

  @Field(() => [String], { nullable: true })
  ulimits?: string[];

  @Field(() => Number, { nullable: true })
  cpuCount?: number;

  @Field(() => Number, { nullable: true })
  cpuPercent?: number;

  @Field(() => Number, { nullable: true })
  iOMaximumIOps?: number;

  @Field(() => Number, { nullable: true })
  iOMaximumBandwidth?: number;

  @Field(() => [String], { nullable: true })
  maskedPaths?: string[];

  @Field(() => [String], { nullable: true })
  readonlyPaths?: string[];
}

@ObjectType()
export class DockerInspectResultHostConfigLogConfig {
  @Field(() => String, { nullable: true })
  type?: string;

  // @Field(() => Record)
  // config: Record<string, unknown>;
}

@ObjectType()
export class DockerInspectResultHostConfigRestartPolicy {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => Number, { nullable: true })
  maximumRetryCount?: number;
}

@ObjectType()
export class DockerInspectResultGraphDriver {
  @Field(() => DockerInspectResultGraphDriverData, { nullable: true })
  data?: DockerInspectResultGraphDriverData;

  @Field(() => String, { nullable: true })
  name?: string;
}

@ObjectType()
export class DockerInspectResultGraphDriverData {
  @Field(() => String, { nullable: true })
  lowerDir?: string;

  @Field(() => String, { nullable: true })
  mergedDir?: string;

  @Field(() => String, { nullable: true })
  upperDir?: string;

  @Field(() => String, { nullable: true })
  workDir?: string;
}

@ObjectType()
export class DockerInspectResultMount {
  @Field(() => String, { nullable: true })
  type?: string;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  source?: string;

  @Field(() => String, { nullable: true })
  destination?: string;

  @Field(() => String, { nullable: true })
  driver?: string;

  @Field(() => String, { nullable: true })
  mode?: string;

  @Field(() => Boolean, { nullable: true })
  rw?: boolean;

  @Field(() => String, { nullable: true })
  propagation?: string;
}

@ObjectType()
export class DockerInspectResultConfig {
  @Field(() => String, { nullable: true })
  hostname?: string;

  @Field(() => String, { nullable: true })
  domainname?: string;

  @Field(() => String, { nullable: true })
  user?: string;

  @Field(() => Boolean, { nullable: true })
  attachStdin?: boolean;

  @Field(() => Boolean, { nullable: true })
  attachStdout?: boolean;

  @Field(() => Boolean, { nullable: true })
  attachStderr?: boolean;

  // @Field(() => Record)
  // exposedPorts: Record<string, unknown>;

  @Field(() => Boolean, { nullable: true })
  tty?: boolean;

  @Field(() => Boolean, { nullable: true })
  openStdin?: boolean;

  @Field(() => Boolean, { nullable: true })
  stdinOnce?: boolean;

  @Field(() => [String], { nullable: true })
  env?: string[];

  @Field(() => [String], { nullable: true })
  cmd?: string[];

  @Field(() => String, { nullable: true })
  image?: string;

  // @Field(() => Record)
  // volumes: Record<string, unknown>;

  @Field(() => String, { nullable: true })
  workingDir?: string;

  @Field(() => [String], { nullable: true })
  entrypoint?: string[];

  @Field(() => [String], { nullable: true })
  onBuild?: string[];

  // @Field(() => Record)
  // labels: Record<string, string>;
}

@ObjectType()
export class DockerInspectResultNetworkSettings {
  @Field(() => String, { nullable: true })
  bridge?: string;

  @Field(() => String, { nullable: true })
  sandboxId?: string;

  @Field(() => Boolean, { nullable: true })
  hairpinMode?: boolean;

  @Field(() => String, { nullable: true })
  linkLocalIPv6Address?: string;

  @Field(() => Number, { nullable: true })
  linkLocalIPv6PrefixLen?: number;

  // @Field(() => Record)
  // ports: Record<string, { hostIp: string; hostPort: string }[]>;

  @Field(() => String, { nullable: true })
  sandboxKey?: string;

  @Field(() => [String], { nullable: true })
  secondaryIPAddresses?: string[];

  @Field(() => [String], { nullable: true })
  secondaryIPv6Addresses?: string[];

  @Field(() => String, { nullable: true })
  endpointId?: string;

  @Field(() => String, { nullable: true })
  gateway?: string;

  @Field(() => String, { nullable: true })
  globalIPv6Address?: string;

  @Field(() => Number, { nullable: true })
  globalIPv6PrefixLen?: number;

  @Field(() => String, { nullable: true })
  ipAddress?: string;

  @Field(() => Number, { nullable: true })
  ipPrefixLen?: number;

  @Field(() => String, { nullable: true })
  IPv6Gateway?: string;

  @Field(() => String, { nullable: true })
  macAddress?: string;

  // @Field(() => Record)
  // networks: Record<string, DockerInspectResultNetworkSettingsNetwork>;
}

@ObjectType()
export class DockerInspectResultNetworkSettingsNetwork {
  @Field(() => [String], { nullable: true })
  ipamConfig?: string[];

  @Field(() => [String], { nullable: true })
  links?: string[];

  @Field(() => [String], { nullable: true })
  aliases?: string[];

  @Field(() => String, { nullable: true })
  networkId?: string;

  @Field(() => String, { nullable: true })
  endpointId?: string;

  @Field(() => String, { nullable: true })
  gateway?: string;

  @Field(() => String, { nullable: true })
  ipAddress?: string;

  @Field(() => Number, { nullable: true })
  ipPrefixLen?: number;

  @Field(() => String, { nullable: true })
  IPv6Gateway?: string;

  @Field(() => String, { nullable: true })
  globalIPv6Address?: string;

  @Field(() => Number, { nullable: true })
  globalIPv6PrefixLen?: number;

  @Field(() => String, { nullable: true })
  macAddress?: string;

  @Field(() => [String], { nullable: true })
  driverOpts?: string[];
}
