/*
 *  File: /docker/types.ts
 *  Project: api
 *  File Created: 20-01-2024 09:46:36
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export type Flag = string | number | boolean | string[] | number[] | undefined;

export interface IDockerInspectResult {
  Id?: string;
  Created?: string;
  Path?: string;
  Args?: string[];
  State?: {
    Status?: string;
    Running?: boolean;
    Paused?: boolean;
    Restarting?: boolean;
    OOMKilled?: boolean;
    Dead?: boolean;
    Pid?: number;
    ExitCode?: number;
    Error?: string;
    StartedAt?: string;
    FinishedAt?: string;
  };
  Image?: string;
  ResolvConfPath?: string;
  HostnamePath?: string;
  HostsPath?: string;
  LogPath?: string;
  Name?: string;
  RestartCount?: number;
  Driver?: string;
  Platform?: string;
  MountLabel?: string;
  ProcessLabel?: string;
  AppArmorProfile?: string;
  ExecIDs?: string[];
  Spec?: {
    ContainerSpec?: {
      Image?: string;
      Labels?: Record<string, string>;
      Env?: string[];
      Privileges?: {
        CredentialSpec?: null | string;
        SELinuxContext?: null | string;
      };
      Isolation?: string;
    };
    Resources?: {};
    RestartPolicy?: {
      Condition?: string;
      MaxAttempts?: number;
    };
    Placement?: {
      Platforms?: {
        Architecture?: string;
        OS?: string;
      }[];
    };
    Networks?: {
      Target?: string;
      Aliases?: string[];
    }[];
    ForceUpdate?: number;
  };
  HostConfig?: {
    Binds?: string[];
    ContainerIDFile?: string;
    LogConfig?: {
      Type?: string;
      Config?: Record<string, unknown>;
    };
    NetworkMode?: string;
    PortBindings?: Record<string, { HostIp: string; HostPort: string }[]>;
    RestartPolicy?: {
      Name?: string;
      MaximumRetryCount?: number;
    };
    AutoRemove?: boolean;
    VolumeDriver?: string;
    VolumesFrom?: string[];
    ConsoleSize?: number[];
    CapAdd?: string[];
    CapDrop?: string[];
    CgroupnsMode?: string;
    Dns?: string[];
    DnsOptions?: string[];
    DnsSearch?: string[];
    ExtraHosts?: string[];
    GroupAdd?: string[];
    IpcMode?: string;
    Cgroup?: string;
    Links?: string[];
    OomScoreAdj?: number;
    PidMode?: string;
    Privileged?: boolean;
    PublishAllPorts?: boolean;
    ReadonlyRootfs?: boolean;
    SecurityOpt?: string[];
    UTSMode?: string;
    UsernsMode?: string;
    ShmSize?: number;
    Runtime?: string;
    Isolation?: string;
    CpuShares?: number;
    Memory?: number;
    NanoCpus?: number;
    CgroupParent?: string;
    BlkioWeight?: number;
    BlkioWeightDevice?: string[];
    BlkioDeviceReadBps?: string[];
    BlkioDeviceWriteBps?: string[];
    BlkioDeviceReadIOps?: string[];
    BlkioDeviceWriteIOps?: string[];
    CpuPeriod?: number;
    CpuQuota?: number;
    CpuRealtimePeriod?: number;
    CpuRealtimeRuntime?: number;
    CpusetCpus?: string;
    CpusetMems?: string;
    Devices?: string[];
    DeviceCgroupRules?: string[];
    DeviceRequests?: string[];
    MemoryReservation?: number;
    MemorySwap?: number;
    MemorySwappiness?: number;
    OomKillDisable?: boolean;
    PidsLimit?: number;
    Ulimits?: string[];
    CpuCount?: number;
    CpuPercent?: number;
    IOMaximumIOps?: number;
    IOMaximumBandwidth?: number;
    MaskedPaths?: string[];
    ReadonlyPaths?: string[];
  };
  GraphDriver?: {
    Data?: {
      LowerDir?: string;
      MergedDir?: string;
      UpperDir?: string;
      WorkDir?: string;
    };
    Name?: string;
  };
  Mounts?: {
    Type?: string;
    Name?: string;
    Source?: string;
    Destination?: string;
    Driver?: string;
    Mode?: string;
    RW?: boolean;
    Propagation?: string;
  }[];
  Config?: {
    Hostname?: string;
    Domainname?: string;
    User?: string;
    AttachStdin?: boolean;
    AttachStdout?: boolean;
    AttachStderr?: boolean;
    ExposedPorts?: Record<string, unknown>;
    Tty?: boolean;
    OpenStdin?: boolean;
    StdinOnce?: boolean;
    Env?: string[];
    Cmd?: string[];
    Image?: string;
    Volumes?: Record<string, unknown>;
    WorkingDir?: string;
    Entrypoint?: string[];
    OnBuild?: string[];
    Labels?: Record<string, string>;
  };
  NetworkSettings?: {
    Bridge?: string;
    SandboxID?: string;
    HairpinMode?: boolean;
    LinkLocalIPv6Address?: string;
    LinkLocalIPv6PrefixLen?: number;
    Ports?: Record<string, { HostIp: string; HostPort: string }[]>;
    SandboxKey?: string;
    SecondaryIPAddresses?: string[];
    SecondaryIPv6Addresses?: string[];
    EndpointID?: string;
    Gateway?: string;
    GlobalIPv6Address?: string;
    GlobalIPv6PrefixLen?: number;
    IPAddress?: string;
    IPPrefixLen?: number;
    IPv6Gateway?: string;
    MacAddress?: string;
    Networks?: Record<
      string,
      {
        IPAMConfig?: string[];
        Links?: string[];
        Aliases?: string[];
        NetworkID?: string;
        EndpointID?: string;
        Gateway?: string;
        IPAddress?: string;
        IPPrefixLen?: number;
        IPv6Gateway?: string;
        GlobalIPv6Address?: string;
        GlobalIPv6PrefixLen?: number;
        MacAddress?: string;
        DriverOpts?: string[];
      }
    >;
  };
  Status?: {
    Timestamp?: string;
    State?: string;
    Message?: string;
    ContainerStatus?: {
      ContainerID?: string;
      PID?: number;
      ExitCode?: number;
    };
    PortStatus?: {};
  };
  NetworksAttachments?: [
    {
      Network?: {
        ID?: string;
        Version?: {
          Index?: number;
        };
        CreatedAt?: string;
        UpdatedAt?: string;
        Spec?: {
          Name?: string;
          Labels?: Record<string, string>;
          DriverConfiguration?: {};
          Ingress?: boolean;
          IPAMOptions?: {
            Driver?: {};
          };
          Scope?: string;
        };
        DriverState?: {
          Name?: string;
          Options?: Record<string, string>;
        };
        IPAMOptions?: {
          Driver?: {
            Name?: string;
          };
          Configs?: [
            {
              Subnet?: string;
              Gateway?: string;
            },
          ];
        };
      };
      Addresses?: string[];
    },
  ];
  Volumes?: null | string[];
}

export interface IDockerPsResult {
  Command?: string;
  CreatedAt?: string;
  ID?: string;
  Image?: string;
  Labels?: string;
  LocalVolumes?: string;
  Mounts?: string;
  Names?: string;
  Networks?: string;
  Ports?: string;
  RunningFor?: string;
  Size?: string;
  State?: string;
  Status?: string;
}
