/*
 *  File: /docker/volume/resolver.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerVolumeService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Mutation, Resolver, Args, Query } from 'type-graphql';
import {
  DockerVolumeCreateArgs,
  DockerVolumeCreateResult,
  DockerVolumeInspectArgs,
  DockerVolumeInspectResult,
  DockerVolumeLsArgs,
  DockerVolumeLsResult,
  DockerVolumePruneArgs,
  DockerVolumePruneResult,
  DockerVolumeRmArgs,
  DockerVolumeRmResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerVolumeResolver {
  constructor(private readonly dockerVolume: DockerVolumeService) {}

  @Query(() => [DockerVolumeLsResult])
  async dockerVolumeLs(@Args(() => DockerVolumeLsArgs) args: DockerVolumeLsArgs) {
    return this.dockerVolume.ls(args);
  }

  @Query(() => [DockerVolumeInspectResult])
  async dockerVolumeInspect(@Args(() => DockerVolumeInspectArgs) args: DockerVolumeInspectArgs) {
    return this.dockerVolume.inspect(args);
  }

  @Mutation(() => DockerVolumeCreateResult)
  async dockerVolumeCreate(@Args(() => DockerVolumeCreateArgs) args: DockerVolumeCreateArgs) {
    return this.dockerVolume.create(args);
  }

  @Mutation(() => DockerVolumeRmResult)
  async dockerVolumeRm(@Args(() => DockerVolumeRmArgs) name: DockerVolumeRmArgs) {
    return this.dockerVolume.rm(name);
  }

  @Mutation(() => DockerVolumePruneResult)
  async dockerVolumePrune(@Args(() => DockerVolumePruneArgs) args: DockerVolumePruneArgs) {
    return this.dockerVolume.prune(args);
  }
}
