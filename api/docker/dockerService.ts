/*
 *  File: /docker/dockerService.ts
 *  Project: api
 *  File Created: 20-01-2024 09:26:36
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import kebabCase from 'lodash.kebabcase';
import {
  DockerExecArgs,
  DockerExecResult,
  DockerInspectArgs,
  DockerInspectResult,
  DockerPsArgs,
  DockerPsResult,
  DockerCommitArgs,
  DockerCommitResult,
  DockerAttachArgs,
  DockerAttachResult,
  DockerCreateArgs,
  DockerCreateResult,
  DockerCpArgs,
  DockerCpResult,
  DockerEventsArgs,
  DockerEventsResult,
  DockerRmArgs,
  DockerRmResult,
  DockerImagesArgs,
  DockerImagesResult,
  DockerRmiArgs,
  DockerRmiResult,
  DockerVersionArgs,
  DockerVersionResult,
  DockerPullArgs,
  DockerPullResult,
  DockerSearchArgs,
  DockerSearchResult,
  DockerRenameArgs,
  DockerRenameResult,
  DockerRestartArgs,
  DockerRestartResult,
  DockerLogsArgs,
  DockerLogsResult,
  DockerKillArgs,
  DockerKillResult,
  DockerHistoryArgs,
  DockerHistoryResult,
  DockerStopArgs,
  DockerStopResult,
  DockerStartResult,
  DockerTagResult,
  DockerSaveResult,
  DockerLoginArgs,
  DockerLoginResult,
  DockerLogoutArgs,
  DockerLogoutResult,
  DockerPauseArgs,
  DockerPauseResult,
  DockerTopResult,
  DockerUnpauseArgs,
  DockerUnpauseResult,
  DockerWaitArgs,
  DockerWaitResult,
  DockerExportArgs,
  DockerExportResult,
  DockerUpdateArgs,
  DockerUpdateResult,
  DockerImportArgs,
  DockerImportResult,
  DockerDiffArgs,
  DockerDiffResult,
  DockerLoadArgs,
  DockerLoadResult,
  DockerPortArgs,
  DockerPortResult,
  DockerBuildArgs,
  DockerBuildResult,
  DockerStatsArgs,
  DockerStatsResult,
} from './dto';
import { ExecaError, execa } from 'execa';
import { Flag } from './types';
import { GraphQLError } from 'graphql';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from './utils';
import { DockerContainerCreateArgs, DockerContainerCreateResult } from './container';

@Injectable()
export class DockerService {
  constructor(private readonly logger: Logger) {}

  async create(args: DockerCreateArgs): Promise<DockerCreateResult> {
    const { stderr, stdout } = await this.docker('create', {
      addHost: args.addHost,
      annotation: args.annotation,
      attach: args.attach,
      blkioWeight: args.blkioWeight,
      blkioWeightDevice: args.blkioWeightDevice,
      capAdd: args.capAdd,
      capDrop: args.capDrop,
      cgroupParent: args.cgroupParent,
      cgroupns: args.cgroupns,
      cidfile: args.cidfile,
      cpuPeriod: args.cpuPeriod,
      cpuQuota: args.cpuQuota,
      cpuRtPeriod: args.cpuRtPeriod,
      cpuRtRuntime: args.cpuRtRuntime,
      cpuShares: args.cpuShares,
      cpus: args.cpus,
      cpusetCpus: args.cpusetCpus,
      cpusetMems: args.cpusetMems,
      device: args.device,
      deviceCgroupRule: args.deviceCgroupRule,
      deviceReadBps: args.deviceReadBps,
      deviceReadIops: args.deviceReadIops,
      deviceWriteBps: args.deviceWriteBps,
      deviceWriteIops: args.deviceWriteIops,
      disableContentTrust: args.disableContentTrust,
      dns: args.dns,
      dnsOption: args.dnsOption,
      dnsSearch: args.dnsSearch,
      domainname: args.domainname,
      entrypoint: args.entrypoint,
      env: args.env,
      envFile: args.envFile,
      expose: args.expose,
      groupAdd: args.groupAdd,
      healthCmd: args.healthCmd,
      healthInterval: args.healthInterval,
      healthRetries: args.healthRetries,
      healthStartPeriod: args.healthStartPeriod,
      healthTimeout: args.healthTimeout,
      help: args.help,
      hostname: args.hostname,
      init: args.init,
      interactive: args.interactive,
      ip: args.ip,
      ipc: args.ipc,
      isolation: args.isolation,
      kernelMemory: args.kernelMemory,
      label: args.label,
      labelFile: args.labelFile,
      link: args.link,
      linkLocalIp: args.linkLocalIp,
      logDriver: args.logDriver,
      logOpt: args.logOpt,
      macAddress: args.macAddress,
      memory: args.memory,
      memoryReservation: args.memoryReservation,
      memorySwap: args.memorySwap,
      memorySwappiness: args.memorySwappiness,
      mount: args.mount,
      name: args.name,
      network: args.network,
      networkAlias: args.networkAlias,
      noHealthcheck: args.noHealthcheck,
      oomKillDisable: args.oomKillDisable,
      oomScoreAdj: args.oomScoreAdj,
      pid: args.pid,
      pidsLimit: args.pidsLimit,
      platform: args.platform,
      privileged: args.privileged,
      publish: args.publish,
      publishAll: args.publishAll,
      pull: args.pull,
      readOnly: args.readOnly,
      restart: args.restart,
      rm: args.rm,
      runtime: args.runtime,
      securityOpt: args.securityOpt,
      shmSize: args.shmSize,
      stopSignal: args.stopSignal,
      stopTimeout: args.stopTimeout,
      storageOpt: args.storageOpt,
      sysctl: args.sysctl,
      tmpfs: args.tmpfs,
      tty: args.tty,
      ulimit: args.ulimit,
      user: args.user,
      userns: args.userns,
      uts: args.uts,
      volume: args.volume,
      volumeDriver: args.volumeDriver,
      volumesFrom: args.volumesFrom,
      workdir: args.workdir,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async info(): Promise<DockerResult> {
    const { stderr, stdout } = await this.docker('info');
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async build(args: DockerBuildArgs): Promise<DockerBuildResult> {
    const { stderr, stdout } = await this.docker(
      'build',
      {
        addHost: args.addHost,
        annotation: args.annotation,
        attest: args.attest,
        buildArg: args.buildArg,
        buildContext: args.buildContext,
        builder: args.builder,
        cacheFrom: args.cacheFrom,
        cacheTo: args.cacheTo,
        cgroupParent: args.cgroupParent,
        file: args.file,
        iidfile: args.iidfile,
        label: args.label,
        load: args.load,
        metadataFile: args.metadataFile,
        network: args.network,
        noCache: args.noCache,
        noCacheFilter: args.noCacheFilter,
        output: args.output,
        platform: args.platform,
        progress: args.progress,
        provenance: args.provenance,
        pull: args.pull,
        push: args.push,
        sbom: args.sbom,
        secret: args.secret,
        shmSize: args.shmSize,
        ssh: args.ssh,
        tag: args.tag,
        target: args.target,
        ulimit: args.ulimit,
      },
      args.path,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async import(args: DockerImportArgs): Promise<DockerImportResult> {
    const { stderr, stdout } = await this.docker(
      'import',
      {
        change: args.change,
        message: args.message,
      },
      args.source,
      args.repository,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async load(args: DockerLoadArgs): Promise<DockerLoadResult> {
    const { stderr, stdout } = await this.docker(
      'load',
      {
        input: args.input,
      },
      args.input,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async update(args: DockerUpdateArgs): Promise<DockerUpdateResult> {
    const { stderr, stdout } = await this.docker('update', {
      blkioWeight: args.blkioWeight,
      cpuPeriod: args.cpuPeriod,
      cpuQuota: args.cpuQuota,
      cpuRtPeriod: args.cpuRtPeriod,
      cpuRtRuntime: args.cpuRtRuntime,
      cpuShares: args.cpuShares,
      cpus: args.cpus,
      cpusetCpus: args.cpusetCpus,
      cpusetMems: args.cpusetMems,
      memory: args.memory,
      memoryReservation: args.memoryReservation,
      memorySwap: args.memorySwap,
      pidsLimit: args.pidsLimit,
      restart: args.restart,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async save(DockerSaveArgs): Promise<DockerSaveResult> {
    const { stderr, stdout } = await this.docker('save', {
      output: DockerSaveArgs.output,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async top(DockerTopArgs): Promise<DockerTopResult> {
    const { stderr, stdout } = await this.docker('top', {
      all: DockerTopArgs.all,
      latest: DockerTopArgs.latest,
      noTrunc: DockerTopArgs.noTrunc,
      size: DockerTopArgs.size,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async tag(DockerTagArgs): Promise<DockerTagResult> {
    const { stderr, stdout } = await this.docker('tag', {
      force: DockerTagArgs.force,
      repo: DockerTagArgs.repo,
      tag: DockerTagArgs.tag,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async run(args: DockerContainerCreateArgs): Promise<DockerContainerCreateResult> {
    const { stderr, stdout } = await this.docker(
      'run',
      {
        ...args.options,
        detach: args.options?.detach ? args.options.detach : true,
      },
      args.image,
      ...(args.commands || []),
      ...(args.args || []),
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      id: stdout,
    };
  }

  async start(DockerStartArgs): Promise<DockerStartResult> {
    const { stderr, stdout } = await this.docker('start', {
      attach: DockerStartArgs.attach,
      detachKeys: DockerStartArgs.detachKeys,
      interactive: DockerStartArgs.interactive,
      noHealthcheck: DockerStartArgs.noHealthcheck,
      sigProxy: DockerStartArgs.sigProxy,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async stop(args: DockerStopArgs): Promise<DockerStopResult> {
    const { stderr, stdout } = await this.docker('stop', {
      time: args.time,
      signal: args.signal,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async kill(args: DockerKillArgs): Promise<DockerKillResult> {
    const { stderr, stdout } = await this.docker('kill', {
      signal: args.signal,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async search(args: DockerSearchArgs): Promise<DockerSearchResult[]> {
    const { stderr, stdout } = await this.docker(
      'search',
      {
        filter: args.filter,
        format: args.format,
        limit: args.limit,
        noTrunc: args.noTrunc,
        orderBy: args.orderBy,
        stars: args.stars,
      },
      args.term,
    );
    if (stderr) throw new GraphQLError(stderr);
    return stdout
      .trim()
      .split('\n')
      .slice(1)
      .map((line) => {
        const [name, description, stars, official] = line.split(/\s{2,}/);
        return {
          name,
          description,
          starCount: stars,
          isOfficial: official === '[OK]',
        } as DockerSearchResult;
      });
  }

  async attach(args: DockerAttachArgs): Promise<DockerAttachResult> {
    const { stderr, stdout } = await this.docker(
      'attach',
      {
        detachKeys: args.detachKeys,
        noStdin: args.noStdin,
        sigProxy: args.sigProxy,
      },
      args.nameOrId,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async stats(args: DockerStatsArgs): Promise<DockerStatsResult> {
    const { stderr, stdout } = await this.docker(
      'stats',
      {
        all: args.all,
        format: 'json',
        noStream: args.noStream,
        noTrunc: args.noTrunc,
      },
      args.nameOrId,
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async inspect(args: DockerInspectArgs): Promise<DockerInspectResult> {
    const { stderr, stdout } = await this.docker(
      'inspect',
      {
        format: 'json',
        size: args.size,
      },
      args.nameOrId,
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout))?.[0];
  }

  async ps(args: DockerPsArgs = {}): Promise<DockerPsResult[]> {
    const { stderr, stdout } = await this.docker('ps', {
      filter: args.filter,
      format: '{{json .}}',
      noResolve: args.noResolve,
      noTrunc: args.noTrunc,
      all: args.all,
      last: args.last,
      latest: args.latest,
      size: args.size,
    });
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(`[${stdout.split('\n').join(',')}]`));
  }

  async commit(args: DockerCommitArgs): Promise<DockerCommitResult> {
    const { stderr, stdout } = await this.docker(
      'commit',
      {
        message: args.message,
        pause: args.pause,
        change: args.change,
        author: args.author,
      },
      args.nameOrId,
      args.tag,
      args.repository,
    );

    if (stderr) throw new GraphQLError(stderr);
    return {
      id: stdout,
      warning: stderr,
    };
  }

  async exec(args: DockerExecArgs): Promise<DockerExecResult> {
    const { stderr, stdout } = await this.docker(
      'exec',
      {
        detach: args.detach,
        env: args.env,
        interactive: args.interactive,
        privileged: args.privileged,
        tty: args.tty,
        user: args.user,
        workdir: args.workdir,
      },
      args.nameOrId,
      args.command,
      ...(args.args || []),
    );
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async cp(args: DockerCpArgs): Promise<DockerCpResult> {
    const { stderr, stdout } = await this.docker(
      'cp',
      {
        archive: args.archive,
        followLink: args.followLink,
      },
      args.nameOrId,
      args.source,
      args.destination,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async events(args: DockerEventsArgs): Promise<DockerEventsResult> {
    const { stderr, stdout } = await this.docker('events', {
      since: args.since,
      until: args.until,
      filter: args.filter,
      format: args.format,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async rm(args: DockerRmArgs): Promise<DockerRmResult> {
    const { stderr, stdout } = await this.docker(
      'rm',
      {
        force: args.force,
        link: args.link,
        volumes: args.volumes,
      },
      args.nameOrId,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr,
    };
  }

  async rmi(args: DockerRmiArgs): Promise<DockerRmiResult> {
    const { stderr, stdout } = await this.docker(
      'rmi',
      {
        force: args.force,
        noPrune: args.noPrune,
      },
      args.nameOrId,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async images(args: DockerImagesArgs): Promise<DockerImagesResult[]> {
    const { stderr, stdout } = await this.docker('images', {
      all: args.all,
      digest: args.digests,
      filter: args.filter,
      format: args.format,
      noTrunc: args.noTrunc,
    });
    if (stderr) throw new GraphQLError(stderr);
    return stdout
      .trim()
      .split('\n')
      .slice(1)
      .map((line) => {
        const [repository, tag, imageId, created, size] = line.split(/\s{2,}/);
        return {
          repository,
          tag,
          imageId,
          created,
          size,
        } as DockerImagesResult;
      });
  }

  async wait(args: DockerWaitArgs): Promise<DockerWaitResult> {
    const { stderr, stdout } = await this.docker('wait', {}, args.nameOrId);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
      statusCode: stdout,
    };
  }

  async diff(args: DockerDiffArgs): Promise<DockerDiffResult[]> {
    const { stderr, stdout } = await this.docker('diff', {}, args.nameOrId);
    if (stderr) throw new GraphQLError(stderr);

    return (
      stdout
        .trim()
        .split('\n')
        .slice(1)
        .map((line) => {
          const [type, path] = line.split(/\s{2,}/);
          return {
            type,
            path,
          } as DockerDiffResult;
        }) || []
    );
  }

  async export(args: DockerExportArgs): Promise<DockerExportResult> {
    const { stderr, stdout } = await this.docker(
      'export',
      {
        output: args.output,
      },
      args.nameOrId,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async port(args: DockerPortArgs): Promise<DockerPortResult> {
    const { stderr, stdout } = await this.docker('port', {}, args.nameOrId, args.port);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async version(args: DockerVersionArgs): Promise<DockerVersionResult> {
    const { stderr, stdout } = await this.docker('version', {
      format: args.format,
    });
    if (stderr) throw new GraphQLError(stderr);
    return {
      version: stdout,
      apiVersion: stdout,
      minAPIVersion: stdout,
      gitCommit: stdout,
      goVersion: stdout,
      os: stdout,
      arch: stdout,
      kernelVersion: stdout,
      buildTime: stdout,
      experimental: stdout,
    };
  }

  async pull(args: DockerPullArgs): Promise<DockerPullResult> {
    const { stderr, stdout } = await this.docker(
      'pull',
      {
        allTags: args.allTags,
        disableContentTrust: args.disableContentTrust,
        platform: args.platform,
      },
      args.name,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async push(args: DockerPullArgs): Promise<DockerPullResult> {
    const { stderr, stdout } = await this.docker(
      'push',
      {
        allTags: args.allTags,
        disableContentTrust: args.disableContentTrust,
      },
      args.name,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async rename(args: DockerRenameArgs): Promise<DockerRenameResult> {
    const { stderr, stdout } = await this.docker('rename', {}, args.nameOrId, args.newName);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async restart(args: DockerRestartArgs): Promise<DockerRestartResult> {
    const { stderr, stdout } = await this.docker('restart', {}, args.nameOrId);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
    };
  }

  async login(args: DockerLoginArgs): Promise<DockerLoginResult> {
    const { stderr, stdout } = await this.docker(
      'login',
      {
        password: args.password,
        username: args.username,
      },
      args.serverAddress,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
      status: stdout,
    };
  }

  async logout(args: DockerLogoutArgs): Promise<DockerLogoutResult> {
    const { stderr, stdout } = await this.docker('logout', {}, args.serverAddress);
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout: stdout,
      stderr: stderr,
      status: stdout,
    };
  }

  async pause(args: DockerPauseArgs): Promise<DockerPauseResult> {
    let result;
    if (args.nameOrId) {
      result = await this.docker('pause', {}, args.nameOrId);
    } else if (args.containers) {
      result = await this.docker('pause', {}, ...args.containers);
    } else {
      throw new Error('No container identifier provided');
    }

    const { stderr, stdout } = result;
    if (stderr) throw new GraphQLError(stderr);

    return {
      stdout: stdout,
      stderr: stderr,
      status: stdout,
    };
  }

  async unpause(args: DockerUnpauseArgs): Promise<DockerUnpauseResult> {
    let result;
    if (args.nameOrId) {
      result = await this.docker('unpause', {}, args.nameOrId);
    } else if (args.containers) {
      result = await this.docker('unpause', {}, ...args.containers);
    } else {
      throw new Error('No container identifier provided');
    }

    const { stderr, stdout } = result;
    if (stderr) throw new GraphQLError(stderr);

    return {
      stdout: stdout,
      stderr: stderr,
      status: stdout,
    };
  }

  async logs(args: DockerLogsArgs): Promise<DockerLogsResult> {
    const { stderr, stdout } = await this.docker(
      'logs',
      {
        details: args.details,
        follow: args.follow,
        since: args.since,
        tail: args.tail,
        timestamps: args.timestamps,
        until: args.until,
      },
      args.nameOrId,
    );
    if (stderr) throw new GraphQLError(stderr);
    return {
      stdout,
      stderr: stderr,
    };
  }

  async history(args: DockerHistoryArgs): Promise<DockerHistoryResult[]> {
    const { stderr, stdout } = await this.docker(
      'history',
      {
        format: args.format,
        human: args.human,
        noTrunc: args.noTrunc,
      },
      args.nameOrId,
    );
    if (stderr) throw new GraphQLError(stderr);
    return stdout
      .trim()
      .split('\n')
      .slice(1)
      .map((line) => {
        const [image, created, createdBy, size, comment] = line.split(/\s{2,}/);
        return {
          image,
          created,
          createdBy,
          size,
          comment,
        } as DockerHistoryResult;
      });
  }

  private async docker(
    command: string,
    options: Record<string, Flag> = {},
    ...args: (string | undefined)[]
  ): Promise<DockerResult> {
    try {
      const p = execa('docker', [
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface DockerResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
