/*
 *  File: /docker/system/types.ts
 *  Project: api
 *  File Created: 01-02-2024 11:57:07
 *  Author: HariKrishna
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export interface DockerSystemInfoResult {
  id?: string;
  containers?: number;
  containersRunning?: number;
  containersPaused?: number;
  containersStopped?: number;
  images?: number;
  driver?: string;
  driverStatus?: string[][];
  plugins?: DockerSystemInfoPlugins;
  memoryLimit?: boolean;
  swapLimit?: boolean;
  cpuCfsPeriod?: boolean;
  cpuCfsQuota?: boolean;
  cpuShares?: boolean;
  cpuSet?: boolean;
  pidsLimit?: boolean;
  iPv4Forwarding?: boolean;
  bridgeNfIptables?: boolean;
  bridgeNfIp6Tables?: boolean;
  debug?: boolean;
  nFd?: number;
  oomKillDisable?: boolean;
  nGoroutines?: number;
  systemTime?: string;
  loggingDriver?: string;
  cgroupDriver?: string;
  cgroupVersion?: string;
  nEventsListener?: number;
  kernelVersion?: string;
  operatingSystem?: string;
  osVersion?: string;
  osType?: string;
  architecture?: string;
  indexServerAddress?: string;
  registryConfig?: DockerSystemInfoRegistryConfig;
  ncpu?: number;
  memTotal?: number;
  genericResources?: string[];
  dockerRootDir?: string;
  httpProxy?: string;
  httpsProxy?: string;
  noProxy?: string;
  name?: string;
  labels?: string[];
  experimentalBuild?: boolean;
  serverVersion?: string;
  runtimes?: DockerSystemInfoRuntimes;
  defaultRuntime?: string;
  swarm?: DockerSystemInfoSwarm;
  liveRestoreEnabled?: boolean;
  isolation?: string;
  initBinary?: string;
  containerdCommit?: DockerSystemInfoCommit;
  runcCommit?: DockerSystemInfoCommit;
  initCommit?: DockerSystemInfoCommit;
  securityOptions?: string[];
  cdiSpecDirs?: string[];
  warnings?: string[];
  clientInfo?: DockerSystemInfoClientInfo;
}

export interface DockerSystemInfoPlugins {
  volume?: string[];
  network?: string[];
  authorization?: string;
  log?: string[];
}

export interface DockerSystemInfoRegistryConfig {
  allowNondistributableArtifactsCidRs?: boolean;
  allowNondistributableArtifactsHostnames?: boolean;
  insecureRegistryCidRs?: string[];
  indexConfigs?: string[];
  mirrors?: string;
}

export interface DockerSystemInfoRuntimes {
  runc?: DockerSystemInfoRuntimeRunc;
  ioContainerdRuncV2?: DockerSystemInfoRuntimeRunc;
}

export interface DockerSystemInfoRuntimeRunc {
  path?: string;
  status?: DockerSystemInfoRuntimeStatus;
}

export interface DockerSystemInfoRuntimeStatus {
  orgOpencontainersRuntimeSpecFeatures?: string;
}

export interface DockerSystemInfoSwarm {
  nodeID?: string;
  nodeAddr?: string;
  localNodeState?: string;
  controlAvailable?: boolean;
  error?: string;
  remoteManagers?: string[];
  nodes?: number;
  managers?: number;
  cluster?: string;
}

export interface DockerSystemInfoCommit {
  id?: string;
  expected?: string;
}

export interface DockerSystemInfoClientInfo {
  debug?: boolean;
  platform?: string;
  version?: string;
  gitCommit?: string;
  goVersion?: string;
  os?: string;
  arch?: string;
  buildTime?: string;
  context?: string;
  plugins?: string[];
  warnings?: string;
}
