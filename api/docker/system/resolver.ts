/*
 *  File: /docker/system/resolver.ts
 *  Project: api
 *  File Created: 01-02-2024 11:04:14
 *  Author: HariKrishna
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DockerSystemService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Mutation, Resolver, Args, Query } from 'type-graphql';
import {
  DockerSystemDfArgs,
  DockerSystemDfResult,
  DockerSystemEventsArgs,
  DockerSystemEventsResult,
  DockerSystemInfoResult,
  DockerSystemPruneArgs,
  DockerSystemPruneResult,
} from './dto';

@Authorized()
@Injectable()
@Resolver()
export class DockerSystemResolver {
  constructor(private readonly dockerSystem: DockerSystemService) {}

  @Query(() => [DockerSystemDfResult])
  async dockerSystemDf(@Args(() => DockerSystemDfArgs) args: DockerSystemDfArgs) {
    return this.dockerSystem.df(args);
  }

  @Query(() => DockerSystemInfoResult)
  async dockerSystemInfo() {
    return this.dockerSystem.info();
  }

  @Query(() => DockerSystemEventsResult)
  async dockerSystemEvents(@Args(() => DockerSystemEventsArgs) args: DockerSystemEventsArgs) {
    return this.dockerSystem.events(args);
  }

  @Mutation(() => DockerSystemPruneResult)
  async dockerSystemPrune(@Args(() => DockerSystemPruneArgs) args: DockerSystemPruneArgs) {
    return this.dockerSystem.prune(args);
  }
}
