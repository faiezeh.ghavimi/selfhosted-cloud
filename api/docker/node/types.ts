/*
 *  File: /docker/node/types.ts
 *  Project: api
 *  File Created: 13-02-2024 05:58:49
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

export interface IDockerNodeInspect {
  ID?: string;
  Version?: {
    Index?: number;
  };
  CreatedAt?: string;
  UpdatedAt?: string;
  Spec?: {
    Labels?: Record<string, string>;
    Role?: string;
    Availability?: string;
  };
  Description?: {
    Hostname?: string;
    Platform?: {
      Architecture?: string;
      OS?: string;
    };
    Resources?: {
      NanoCPUs?: number;
      MemoryBytes?: number;
    };
    Engine?: {
      EngineVersion?: string;
      Plugins?: Array<{
        Type?: string;
        Name?: string;
      }>;
    };
    TLSInfo?: {
      TrustRoot?: string;
      CertIssuerSubject?: string;
      CertIssuerPublicKey?: string;
    };
  };
  Status?: {
    State?: string;
    Addr?: string;
  };
  ManagerStatus?: {
    Leader?: boolean;
    Reachability?: string;
    Addr?: string;
  };
}
