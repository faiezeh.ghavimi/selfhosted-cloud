/*
 *  File: /docker/node/resolver.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import {
  DockerNodeInspectArgs,
  DockerNodeInspectResult,
  DockerNodePsArgs,
  DockerNodePsResult,
  DockerNodeRmArgs,
  DockerNodeUpdateArgs,
} from './dto';
import { DockerNodeLsResult } from './dto';
import { DockerNodeService } from './service';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Query, Resolver, Args, Mutation, Arg } from 'type-graphql';

@Authorized()
@Injectable()
@Resolver()
export class DockerNodeResolver {
  constructor(private readonly dockerNodeService: DockerNodeService) {}

  @Query(() => DockerNodeInspectResult)
  async dockerNodeInspect(
    @Args(() => DockerNodeInspectArgs) args: DockerNodeInspectArgs,
  ): Promise<DockerNodeInspectResult> {
    return this.dockerNodeService.inspect(args);
  }

  @Query(() => [DockerNodeLsResult])
  async dockerNodeLs(): Promise<DockerNodeLsResult[]> {
    return this.dockerNodeService.ls();
  }

  @Query(() => [DockerNodePsResult])
  async dockerNodePs(@Args(() => DockerNodePsArgs) args: DockerNodePsArgs): Promise<DockerNodePsResult[]> {
    return this.dockerNodeService.ps(args);
  }

  @Mutation(() => String)
  async dockerNodeDemote(@Arg('nodes', () => [String]) nodes: string[]) {
    return this.dockerNodeService.demote(nodes);
  }

  @Mutation(() => String)
  async dockerNodePromote(@Arg('nodes', () => [String]) nodes: string[]) {
    return this.dockerNodeService.promote(nodes);
  }

  @Mutation(() => String)
  async dockerNodeRemove(@Args(() => DockerNodeRmArgs) nodes: DockerNodeRmArgs) {
    return this.dockerNodeService.rm(nodes);
  }

  @Mutation(() => String)
  async dockerNodeUpdate(@Args(() => DockerNodeUpdateArgs) args: DockerNodeUpdateArgs): Promise<string> {
    return this.dockerNodeService.update(args);
  }
}
