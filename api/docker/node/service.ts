/*
 *  File: /docker/node/service.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import kebabCase from 'lodash.kebabcase';
import {
  DockerNodeInspectArgs,
  DockerNodeInspectResult,
  DockerNodeLsResult,
  DockerNodePsArgs,
  DockerNodePsResult,
  DockerNodeRmArgs,
  DockerNodeUpdateArgs,
} from './dto';
import { ExecaError, execa } from 'execa';
import { Flag } from '../types';
import { GraphQLError } from 'graphql';
import { Logger, Injectable } from '@multiplatform.one/typegraphql';
import { camelCaseKeys } from '../utils';

@Injectable()
export class DockerNodeService {
  constructor(private readonly logger: Logger) {}

  async demote(nodes: string[]): Promise<string> {
    const { stderr, stdout } = await this.node('demote', {}, nodes);
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  async inspect(args: DockerNodeInspectArgs): Promise<DockerNodeInspectResult> {
    const { stdout, stderr } = await this.node(
      'inspect',
      {
        format: '{{json .}}',
      },
      [args.hostname],
    );
    if (stderr) throw new GraphQLError(stderr);
    return camelCaseKeys(JSON.parse(stdout));
  }

  async ls(): Promise<DockerNodeLsResult[]> {
    const { stderr, stdout } = await this.node('ls', {
      format: 'json',
    });
    if (stderr) {
      throw new GraphQLError(stderr);
    }
    return stdout
      .split('\n')
      .slice(1)
      .map((line) => {
        const parts = line.split(/\s{2,}/).map((part) => part.trim());
        console.log(parts);
        return {
          id: parts[0],
          hostname: parts[1],
          status: parts[2],
          availability: parts[3],
          managerStatus: parts[4],
          engineVersion: parts[5],
        } as DockerNodeLsResult;
      });
  }

  async promote(nodes: string[]): Promise<string> {
    const { stderr, stdout } = await this.node('promote', {}, nodes);
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  async ps(args: DockerNodePsArgs): Promise<DockerNodePsResult[]> {
    const { stdout, stderr } = await this.node('ps', {
      ...args.options,
      format: '{{json .}}',
    });
    if (stderr) throw new GraphQLError(stderr);
    return stdout.split('\n').map((line) => {
      const [id, name, image, node, desiredState, currentState, error, ports] = line.split(/\s{2,}/);
      return {
        id,
        name,
        image,
        node,
        desiredState,
        currentState,
        error,
        ports,
      } as DockerNodePsResult;
    });
  }

  async rm(args: DockerNodeRmArgs): Promise<string> {
    const { stderr, stdout } = await this.node('rm', { force: args.force }, args.nodes);
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  async update(args: DockerNodeUpdateArgs): Promise<string> {
    const { stderr, stdout } = await this.node(
      'update',
      {
        ...args.options,
      },
      [args.node],
    );
    if (stderr) throw new GraphQLError(stderr);
    return stdout;
  }

  private async node(command: string, options: Record<string, Flag> = {}, args: string[] = []): Promise<NodeResult> {
    try {
      const p = execa('docker', [
        'node',
        command,
        ...Object.entries(options).reduce<string[]>((acc, [key, value]: [string, Flag]) => {
          if (value === true) {
            acc.push(`--${kebabCase(key)}`);
          } else if (typeof value === 'string' || typeof value === 'number') {
            acc.push(`--${kebabCase(key)}`, value.toString());
          } else if (Array.isArray(value)) {
            value.forEach((v) => {
              acc.push(`--${kebabCase(key)}`, v.toString());
            });
          }
          return acc;
        }, []),
        ...((args || []).filter(Boolean) as string[]),
      ]);
      if (process.env.DEBUG !== '0') {
        p.stdout?.pipe(process.stdout);
        p.stderr?.pipe(process.stderr);
      }
      const proc = await p;
      return {
        stderr: proc.stderr,
        stdout: proc.stdout,
      };
    } catch (err) {
      const error = err as ExecaError;
      if (error.stderr) {
        return {
          stderr: error.stderr,
          stdout: error.stdout,
        };
      }
      throw err;
    }
  }
}

export interface NodeResult {
  stderr: string;
  stdout: string;
  [key: string]: any;
}
