/*
 *  File: /docker/node/dto.ts
 *  Project: api
 *  File Created: 19-01-2024 09:43:03
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { GraphQLJSONObject } from 'graphql-scalars';
import { Field, ArgsType, Int, ObjectType, InputType } from 'type-graphql';

@ArgsType()
export class DockerNodeInspectArgs {
  @Field(() => String, { description: 'the hostname of the node' })
  hostname: string;
}

@ObjectType()
export class DockerNodeInspectResult {
  @Field(() => String, { nullable: true })
  id?: string;

  @Field(() => Int, { nullable: true })
  versionIndex?: number;

  @Field(() => String, { nullable: true })
  createdAt?: string;

  @Field(() => String, { nullable: true })
  updatedAt?: string;

  @Field(() => DockerNodeSpec, { nullable: true })
  spec?: DockerNodeSpec;

  @Field(() => DockerNodeDescription, { nullable: true })
  description?: DockerNodeDescription;

  @Field(() => DockerNodeStatus, { nullable: true })
  status?: DockerNodeStatus;

  @Field(() => DockerNodeManagerStatus, { nullable: true })
  managerStatus?: DockerNodeManagerStatus;
}

@ObjectType()
export class DockerNodeSpec {
  @Field(() => GraphQLJSONObject, { nullable: true })
  labels?: Record<string, string>;

  @Field(() => String, { nullable: true })
  role?: string;

  @Field(() => String, { nullable: true })
  availability?: string;
}

@ObjectType()
export class DockerNodeDescription {
  @Field(() => String, { nullable: true })
  hostname?: string;

  @Field(() => DockerNodePlatform, { nullable: true })
  platform?: DockerNodePlatform;

  @Field(() => DockerNodeResources, { nullable: true })
  resources?: DockerNodeResources;

  @Field(() => DockerNodeEngine, { nullable: true })
  engine?: DockerNodeEngine;

  @Field(() => DockerNodeTLSInfo, { nullable: true })
  tlsInfo?: DockerNodeTLSInfo;
}

@ObjectType()
export class DockerNodePlatform {
  @Field(() => String, { nullable: true })
  architecture?: string;

  @Field(() => String, { nullable: true })
  os?: string;
}

@ObjectType()
export class DockerNodeResources {
  @Field(() => Number, { nullable: true })
  nanoCPUs?: number;

  @Field(() => Number, { nullable: true })
  memoryBytes?: number;
}

@ObjectType()
export class DockerNodeEngine {
  @Field(() => String, { nullable: true })
  engineVersion?: string;

  @Field(() => [DockerNodePlugin], { nullable: true })
  plugins?: DockerNodePlugin[];
}

@ObjectType()
export class DockerNodePlugin {
  @Field(() => String, { nullable: true })
  type?: string;

  @Field(() => String, { nullable: true })
  name?: string;
}

@ObjectType()
export class DockerNodeTLSInfo {
  @Field(() => String, { nullable: true })
  trustRoot?: string;

  @Field(() => String, { nullable: true })
  certIssuerSubject?: string;

  @Field(() => String, { nullable: true })
  certIssuerPublicKey?: string;
}

@ObjectType()
export class DockerNodeStatus {
  @Field(() => String, { nullable: true })
  state?: string;

  @Field(() => String, { nullable: true })
  addr?: string;
}

@ObjectType()
export class DockerNodeManagerStatus {
  @Field(() => Boolean, { nullable: true })
  leader?: boolean;

  @Field(() => String, { nullable: true })
  reachability?: string;

  @Field(() => String, { nullable: true })
  addr?: string;
}

@ObjectType()
export class DockerNodeLsResult {
  @Field({ description: 'The ID of the Docker node' })
  id: string;

  @Field({ description: 'The hostname of the Docker node' })
  hostname: string;

  @Field({ description: 'The status of the Docker node' })
  status: string;

  @Field({ description: 'The availability of the Docker node' })
  availability: string;

  @Field({ description: 'The manager status of the Docker node' })
  managerStatus: string;

  @Field({ description: 'The engine version of the Docker node' })
  engineVersion: string;
}

@ArgsType()
export class DockerNodePsArgs {
  @Field(() => [String], { nullable: true, description: 'List of node IDs to filter the tasks' })
  nodes?: string[];

  @Field(() => DockerNodePsOptions, {
    nullable: true,
    description: 'Options for filtering, formatting, and displaying tasks',
  })
  options?: DockerNodePsOptions;
}

@InputType()
export class DockerNodePsOptions {
  @Field(() => String, { nullable: true, description: 'Filter output based on conditions provided' })
  filter?: string;

  @Field(() => String, { nullable: true, description: 'Pretty-print tasks using a Go template' })
  format?: string;

  @Field(() => Boolean, { nullable: true, description: 'Do not map IDs to Names' })
  noResolve?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Do not truncate output' })
  noTrunc?: boolean;

  @Field(() => Boolean, { nullable: true, description: 'Only display task IDs' })
  quiet?: boolean;
}

@ObjectType()
export class DockerNodePsResult {
  @Field(() => String, { nullable: true, description: 'The unique identifier for the task' })
  id?: string;

  @Field(() => String, { nullable: true, description: 'The name of the task' })
  name?: string;

  @Field(() => String, { nullable: true, description: 'The image used by the task' })
  image?: string;

  @Field(() => String, { nullable: true, description: 'The node ID where the task is deployed' })
  node?: string;

  @Field(() => String, { nullable: true, description: 'The desired state of the task' })
  desiredState?: string;

  @Field(() => String, { nullable: true, description: 'The current state of the task' })
  currentState?: string;

  @Field(() => String, { nullable: true, description: 'Any error messages associated with the task' })
  error?: string;

  @Field(() => String, { nullable: true, description: 'The ports that are exposed by the task' })
  ports?: string;
}

@ArgsType()
export class DockerNodeRmArgs {
  @Field(() => [String], { description: 'List of node IDs to remove' })
  nodes: string[];

  @Field(() => Boolean, { nullable: true, description: 'Force remove the node' })
  force?: boolean;
}

@ArgsType()
export class DockerNodeUpdateArgs {
  @Field(() => String, { description: 'node' })
  node: string;

  @Field(() => DockerNodeUpdateOptions, { description: 'the update options for the node' })
  options: DockerNodeUpdateOptions;
}

@InputType()
export class DockerNodeUpdateOptions {
  @Field(() => String, { nullable: true, description: 'Availability of the node ("active"|"pause"|"drain")' })
  availability?: string;

  @Field(() => [String], { nullable: true, description: 'Add or update a node label (key=value)' })
  labelAdd?: string[];

  @Field(() => [String], { nullable: true, description: 'Remove a node label if exists' })
  labelRm?: string[];

  @Field(() => String, { nullable: true, description: 'Role of the node ("worker"|"manager")' })
  role?: string;
}
