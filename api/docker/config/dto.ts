/*
 *  File: /docker/config/dto.ts
 *  Project: api
 *  File Created: 16-02-2024 16:00:51
 *  Author: Lalit rajak
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ArgsType, Field, InputType } from 'type-graphql';

@ArgsType()
export class DockerConfigCreateArgs {
  @Field(() => String, { description: 'file|-' })
  file: string;

  @Field(() => String, { nullable: true, description: 'Config name' })
  name: string;

  @Field(() => DockerConfigCreateOptions, { nullable: true, description: 'options for creating config' })
  options: DockerConfigCreateOptions;
}

@InputType()
export class DockerConfigCreateOptions {
  @Field(() => [String], { nullable: true, description: 'Config labels' })
  label: string[];

  @Field(() => String, { nullable: true, description: 'Template driver' })
  templateDriver: string;
}
