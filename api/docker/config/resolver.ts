/*
 *  File: /docker/config/resolver.ts
 *  Project: api
 *  File Created: 06-01-2024 23:24:23
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { Injectable } from '@multiplatform.one/typegraphql';
import { Resolver, Args, Mutation } from 'type-graphql';
import { DockerConfigCreateArgs, DockerConfigService } from '../config';

@Authorized()
@Injectable()
@Resolver()
export class DockerConfigResolver {
  constructor(private readonly config: DockerConfigService) {}

  @Mutation(() => String)
  async createConfig(@Args(() => DockerConfigCreateArgs) args: DockerConfigCreateArgs) {
    return this.config.create(args);
  }
}
