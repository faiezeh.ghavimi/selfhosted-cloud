/*
 *  File: /deployment/resolver.ts
 *  Project: api
 *  File Created: 19-01-2024 12:55:00
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Args, Info, Mutation, Resolver } from 'type-graphql';
import { Authorized } from '@multiplatform.one/keycloak-typegraphql';
import { DeploymentService } from './service';
import { GraphQLResolveInfo } from 'graphql';
import { Injectable } from '@multiplatform.one/typegraphql';
import { InstallAppArgs, InstallAppResult, RemoveAppArgs } from './dto';
import { PrismaDeployment } from '../generated/type-graphql';
import { transformInfoIntoPrismaArgs } from '../generated/type-graphql/helpers';

@Authorized()
@Injectable()
@Resolver(() => PrismaDeployment)
export class DeploymentResolver {
  constructor(private readonly deploymentService: DeploymentService) {}

  @Mutation(() => InstallAppResult, { nullable: false })
  async installApp(@Info() info: GraphQLResolveInfo, @Args() args: InstallAppArgs) {
    const { count } = transformInfoIntoPrismaArgs(info);
    return this.deploymentService.installApp(args, count);
  }

  @Mutation(() => PrismaDeployment, { nullable: false })
  async removeApp(@Args() args: RemoveAppArgs) {
    return this.deploymentService.removeApp(args);
  }
}
