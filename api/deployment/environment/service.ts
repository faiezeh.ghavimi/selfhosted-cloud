/*
 *  File: /deployment/environment/service.ts
 *  Project: api
 *  File Created: 19-01-2024 13:15:56
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import fs from 'fs/promises';
import jsYaml from 'js-yaml';
import path from 'path';
import { Deployment, Environment, PrismaClient } from '@prisma/client';
import { DockerCompose, Values } from '../types';
import { DockerService, DockerServiceService, DockerStackService } from '../../docker';
import { GraphQLError } from 'graphql';
import { Injectable } from '@multiplatform.one/typegraphql';
import { InstallEnvironmentResult } from './dto';
import { Installer } from '../installer';
import { PrismaEnvironmentCreateInput } from '../../generated/type-graphql';
import { config } from '../../config';
import { transformCountFieldIntoSelectRelationsCount } from '../../generated/type-graphql/helpers';

@Injectable()
export class EnvironmentService {
  constructor(
    private readonly prisma: PrismaClient,
    private readonly dockerStackService: DockerStackService,
    private readonly dockerService: DockerService,
    private readonly dockerServiceService: DockerServiceService,
  ) {}

  async installEnvironment(data: PrismaEnvironmentCreateInput, count?: any): Promise<InstallEnvironmentResult> {
    if (!data.deployment.connect?.id) {
      throw new GraphQLError(`deployment.connect.id is required to install environment ${data.name}`);
    }
    const deployment = await this.prisma.deployment.findUnique({
      where: { id: data.deployment.connect.id },
    });
    if (!deployment) throw new GraphQLError(`environment ${data.name} is not associated with a deployment`);
    const environmentDir = path.join(config.dirs.deployments, deployment.name, 'environments', data.name);
    const runtimeDir = path.join(config.dirs.runtime, deployment.name, data.name);
    const stackName = `${deployment.name}--${data.name}`;
    if (
      await this.prisma.environment.findFirst({
        where: {
          name: data.name,
          deployment: data.deployment.connect,
        },
      })
    ) {
      throw new GraphQLError(`environment ${data.name} already exists`);
    }
    try {
      const environment = await this.prisma.environment.create({
        data,
        ...(count && transformCountFieldIntoSelectRelationsCount(count)),
      });
      await Promise.all([fs.mkdir(environmentDir, { recursive: true }), fs.mkdir(runtimeDir, { recursive: true })]);
      const installer = await this.createInstaller(deployment, environment);
      const composeFile = await installer.templateEnvironment();
      await this.ensureVolumeDirectories(composeFile);
      await this.dockerStackService.deploy({
        name: stackName,
        composeFile,
        withRegistryAuth: true,
      });
      const privateHost = installer.app.primaryService
        ? installer.host.getPrivate(installer.app.primaryService)
        : undefined;
      const publicHost = installer.app.primaryService
        ? installer.host.getPublic(installer.app.primaryService)
        : undefined;
      return {
        deployment,
        environment,
        access: {
          publicHost,
          privateHost,
        },
      };
    } catch (err) {
      const deploymentId = data.deployment.connect.id;
      if (await this.stackExists(stackName)) {
        await this.dockerStackService.rm({
          name: stackName,
        });
      }
      await Promise.all([
        fs.rm(environmentDir, { recursive: true, force: true }).catch((err) => {
          if (err.code !== 'ENOENT') throw err;
        }),
        fs.rm(runtimeDir, { recursive: true, force: true }).catch((err) => {
          if (err.code !== 'ENOENT') throw err;
        }),
      ]);
      const environment = await this.prisma.environment.findFirst({
        where: {
          name: data.name,
          deploymentId,
        },
      });
      if (environment) {
        await this.prisma.publishedPort.deleteMany({
          where: {
            environmentId: environment.id,
          },
        });
        await this.prisma.environment.delete({
          where: {
            id: environment.id,
          },
        });
      }
      throw err;
    }
  }

  async removeEnvironment(deploymentName: string, environmentName: string) {
    const deployment = await this.prisma.deployment.findUnique({
      where: {
        name: deploymentName,
      },
    });
    if (!deployment) throw new GraphQLError(`deployment ${deploymentName} does not exist`);
    const environment = await this.prisma.environment.findFirst({
      where: {
        deploymentId: deployment.id,
        name: environmentName,
      },
    });
    if (!environment) {
      throw new GraphQLError(`environment ${environmentName} does not exist for deployment ${deployment.name}`);
    }
    const stackName = `${deployment.name}--${environment.name}`;
    if (await this.stackExists(stackName)) {
      await this.dockerStackService.rm({
        name: stackName,
      });
    }
    const environmentDir = path.join(config.dirs.deployments, deployment.name, 'environments', environment.name);
    const runtimeDir = path.join(config.dirs.runtime, deployment.name, environment.name);
    await Promise.all([
      fs.rm(environmentDir, { recursive: true }).catch((err) => {
        if (err.code !== 'ENOENT') throw err;
      }),
      fs.rm(runtimeDir, { recursive: true }).catch((err) => {
        if (err.code !== 'ENOENT') throw err;
      }),
    ]);
    await this.prisma.publishedPort.deleteMany({
      where: {
        environmentId: environment.id,
      },
    });
    return this.prisma.environment.delete({
      where: {
        id: environment.id,
      },
    });
  }

  private async createInstaller(deployment: Deployment, environment: Environment, values?: Values[]) {
    return Installer.new(
      this.dockerService,
      this.dockerServiceService,
      this.prisma,
      deployment.name,
      environment.name,
      values,
      deployment,
    );
  }

  private async ensureVolumeDirectories(composeFile: string) {
    const compose = jsYaml.load((await fs.readFile(composeFile)).toString()) as DockerCompose;
    await Promise.all(
      Object.values(compose.volumes || {}).map(async (volume) => {
        if (volume.driver === 'local') {
          if (!volume.driver_opts?.device) return;
          const volumePath = path.resolve(volume.driver_opts?.device);
          try {
            await fs.access(volumePath);
          } catch {
            await fs.mkdir(volumePath, { recursive: true });
          }
        }
      }),
    );
  }

  private async stackExists(name: string) {
    try {
      await this.dockerStackService.ps({ name });
      return true;
    } catch (err) {
      return false;
    }
  }
}
