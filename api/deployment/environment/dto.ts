/*
 *  File: /deployment/environment/dto.ts
 *  Project: api
 *  File Created: 16-02-2024 15:14:44
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { PrismaDeployment, PrismaEnvironment } from '../../generated/type-graphql';
import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class InstallEnvironmentResult {
  @Field(() => PrismaDeployment)
  deployment: PrismaDeployment;

  @Field(() => PrismaEnvironment)
  environment: PrismaEnvironment;

  @Field(() => EnvironmentAccess)
  access: EnvironmentAccess;
}

@ObjectType()
export class EnvironmentAccess {
  @Field(() => String, { nullable: true })
  privateHost?: string;

  @Field(() => String, { nullable: true })
  publicHost?: string;

  @Field(() => String, { nullable: true })
  username?: string;

  @Field(() => String, { nullable: true })
  password?: string;
}
