/*
 *  File: /deployment/installer.ts
 *  Project: api
 *  File Created: 19-01-2024 14:46:31
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import ejs from 'ejs';
import fs from 'fs/promises';
import jsYaml from 'js-yaml';
import merge from 'lodash.merge';
import path from 'path';
import type {
  App,
  DeploymentPaths,
  DockerCompose,
  GetBaseUrlMethods,
  GetHostMethods,
  GetHostnameMethods,
  SafeInstaller,
  Values,
} from './types';
import type { DockerService, DockerServiceExecResult, DockerServiceService } from '../docker';
import { GraphQLError } from 'graphql';
import { Plug, SafePlug } from './plug';
import { config } from '../config';
import { overlays } from './overlays';
import {
  PrismaClient,
  Deployment as PrismaDeployment,
  Environment as PrismaEnvironment,
  PublishedPort,
} from '@prisma/client';

const logger = console;

export class Installer {
  static async new(
    docker: DockerService,
    dockerService: DockerServiceService,
    prisma: PrismaClient,
    name: string,
    environmentName = 'default',
    values?: Values[],
    deployment?: PrismaDeployment,
    environment?: PrismaEnvironment,
  ) {
    const dir = path.resolve(config.dirs.deployments, name);
    const paths: DeploymentPaths = {
      app: path.join(dir, 'app.yaml'),
      compose: path.join(dir, 'compose.yaml'),
      plugs: [],
      sockets: [],
      values: [],
    };
    try {
      await fs.stat(paths.compose);
    } catch {
      throw new Error(`compose file not found in deployment '${name}'`);
    }
    paths.app = await fs
      .stat(path.join(dir, 'app.yaml'))
      .then(() => path.join(dir, 'app.yaml'))
      .catch(() => undefined);
    paths.readme = await fs
      .stat(path.join(dir, 'README.md'))
      .then(() => path.join(dir, 'README.md'))
      .catch(() => undefined);
    const files = await fs.readdir(dir);
    paths.plugs = files.filter((file) => file.endsWith('.plug.yaml')).map((file) => path.join(dir, file));
    paths.sockets = files.filter((file) => file.endsWith('.socket.yaml')).map((file) => path.join(dir, file));
    paths.questions = await fs
      .stat(path.join(dir, 'questions.yaml'))
      .then(() => path.join(dir, 'questions.yaml'))
      .catch(() => undefined);
    paths.values = [
      await fs
        .stat(path.join(dir, 'values.yaml'))
        .then(() => path.join(dir, 'values.yaml'))
        .catch(() => undefined),
      await fs
        .stat(path.join(dir, `values.${environmentName}.yaml`))
        .then(() => path.join(dir, `values.${environmentName}.yaml`))
        .catch(() => undefined),
    ].filter(Boolean) as string[];
    if (!deployment) {
      deployment =
        (await prisma.deployment.findUnique({
          where: {
            name: this.name,
          },
        })) || undefined;
    }
    if (!deployment) throw new GraphQLError(`deployment ${this.name} does not exist`);
    if (!environment) {
      environment =
        (await prisma.environment.findFirst({
          where: {
            deploymentId: deployment.id,
            name: environmentName,
          },
        })) || undefined;
    }
    if (!environment) {
      throw new GraphQLError(`environment ${environmentName} does not exist for deployment ${this.name}`);
    }
    let app: App = {
      name: deployment.appName,
    };
    if (paths.app) {
      app = {
        ...app,
        ...(jsYaml.load((await fs.readFile(paths.app)).toString()) as Partial<App>),
      };
    }
    const loadedValues = await Installer.loadValues(environment, paths, values);
    const publishedPorts = await Installer.allocatePublishedPorts(loadedValues, prisma, environment);
    return new Installer(
      docker,
      dockerService,
      prisma,
      app,
      deployment,
      environment,
      paths,
      publishedPorts,
      loadedValues,
    );
  }

  private static async allocatePublishedPorts(values: Values, prisma: PrismaClient, environment: PrismaEnvironment) {
    const ports: Record<string, number> = {};
    for (const [serviceName, service] of Object.entries(values)) {
      for (const [portName, portValue] of Object.entries(service.networking?.ports || {})) {
        const port = await this.allocatePublishedPort(prisma, environment, serviceName, portValue, portName);
        ports[portName] = port;
      }
    }
    return ports;
  }

  private static async allocatePublishedPort(
    prisma: PrismaClient,
    environment: PrismaEnvironment,
    serviceName: string,
    port?: number | boolean,
    portName?: string,
  ) {
    if (!portName) portName = serviceName;
    let publishedPort: PublishedPort | null = null;
    if (typeof port === 'number') {
      publishedPort = await prisma.publishedPort.findUnique({
        where: {
          port,
        },
      });
    } else {
      publishedPort = await prisma.publishedPort.findFirst({
        where: {
          environmentId: environment.id,
          name: portName,
          serviceName,
        },
      });
    }
    if (!publishedPort) {
      return (
        await prisma.publishedPort.create({
          data: {
            environment: { connect: { id: environment.id } },
            name: portName,
            port: typeof port === 'number' ? port : await Installer.generateAvailablePublishedPort(prisma),
            serviceName,
          },
        })
      ).port;
    }
    if (publishedPort.environmentId !== environment.id) {
      throw new TemplateError(
        `port ${port} is already allocated to${
          publishedPort.serviceName === publishedPort.name ? '' : ` port name ${publishedPort.name} of`
        } service ${publishedPort.serviceName} in environment ${environment.name}`,
      );
    }
    if (publishedPort.serviceName !== serviceName) {
      throw new TemplateError(
        `port ${port} is already allocated to${
          publishedPort.serviceName === publishedPort.name ? '' : ` port name ${publishedPort.name} of`
        } service ${publishedPort.serviceName}`,
      );
    }
    if (publishedPort.name !== portName) {
      throw new TemplateError(`port ${port} is already allocated to port name ${publishedPort.name}`);
    }
    return publishedPort.port;
  }

  private static async generateAvailablePublishedPort(prisma: PrismaClient, start = 30000, end = 32767) {
    const allPublishedPorts = (await prisma.publishedPort.findMany()).map((port) => port.port);
    for (let i = start; i <= end; i++) {
      if (!allPublishedPorts.includes(i)) return i;
    }
    throw new GraphQLError(`no available ports in range ${start}-${end}`);
  }

  private static async loadValues(
    environment: PrismaEnvironment,
    paths: DeploymentPaths,
    values?: Values[],
  ): Promise<Values> {
    const m = merge(
      {},
      ...[
        ...(await Promise.all(
          paths.values.map(async (valuesPath) => jsYaml.load((await fs.readFile(valuesPath)).toString())),
        )),
        ...((environment?.values || []) as Values[]),
        ...(values || []),
      ].map((value) => (typeof value === 'string' ? jsYaml.load(value) : value)),
    );
    return m;
  }

  dir: string;
  config = config;
  stackName: string;

  hostname: GetHostnameMethods = {
    get: (serviceName: string) => this.hostname.getPublic(serviceName) || this.hostname.getPrivate(serviceName),
    getPublic: (serviceName: string) => this.values[serviceName]?.networking?.hostname,
    getPrivate: (serviceName: string) => `${this.stackName}_${serviceName}`,
  };

  host: GetHostMethods = {
    get: (serviceName: string, portName?: string) => {
      return this.host.getPublic(serviceName, portName) || this.host.getPrivate(serviceName, portName);
    },
    getPublic: (serviceName: string, portName?: string) => {
      const hostname = this.hostname.getPublic(serviceName);
      if (!hostname) return;
      const port = this.publishedPorts[portName || serviceName];
      return hostname + (port ? `:${port}` : '');
    },
    getPrivate: (serviceName: string, portName?: string) => {
      const containerPorts = this.app.services?.[serviceName]?.containerPorts || {};
      const port = containerPorts[portName || serviceName];
      return `${this.hostname.getPrivate(serviceName)}${port ? `:${port}` : ''}`;
    },
  };

  baseUrl: GetBaseUrlMethods = {
    get: (serviceName: string, portName?: string) => {
      return this.baseUrl.getPublic(serviceName, portName) || this.baseUrl.getPrivate(serviceName, portName);
    },
    getPublic: (serviceName: string, portName?: string) => {
      const host = this.host.getPublic(serviceName, portName);
      if (!host) return;
      return `http${this.values[serviceName]?.networking?.tls ? 's' : ''}://${host}`;
    },
    getPrivate: (serviceName: string, portName?: string) => `http://${this.host.getPrivate(serviceName, portName)}`,
  };

  constructor(
    private readonly docker: DockerService,
    private readonly dockerService: DockerServiceService,
    private readonly prisma: PrismaClient,
    public readonly app: App,
    public readonly deployment: PrismaDeployment,
    public readonly environment: PrismaEnvironment,
    public readonly paths: DeploymentPaths,
    public readonly publishedPorts: Record<string, number>,
    public readonly values: Values,
  ) {
    this.dir = path.resolve(this.config.dirs.deployments, deployment.name);
    this.stackName = `${deployment.name}--${environment.name}`;
  }

  async templateEnvironment() {
    const compose = await this.compose(Format.Yaml);
    const environmentDir = path.join(
      config.dirs.deployments,
      this.deployment.name,
      'environments',
      this.environment.name,
    );
    const composeFile = path.join(environmentDir, 'compose.yaml');
    await fs.writeFile(composeFile, compose);
    return composeFile;
  }

  compose(format?: Format.Object): Promise<DockerCompose | undefined>;
  compose(format: Format.Json | Format.Yaml): Promise<string>;
  async compose(format = Format.Object): Promise<DockerCompose | string | undefined> {
    const data = {
      // ignores compose to prevent circular reference
      ...(await this.getSafe(['compose'])),
      plugs: await this.getSafePlugs(),
    };
    let compose = jsYaml.load(
      await ejs.render((await fs.readFile(this.paths.compose)).toString(), data, {
        async: true,
        openDelimiter: '#{',
        closeDelimiter: '}#',
        beautify: false,
        escape: (str) => str,
      }),
    ) as DockerCompose;
    for (const [overlayName, overlayConfig] of Object.entries(this.app.overlays || {})) {
      const overlay = overlays[overlayName];
      if (typeof overlay === 'function') {
        compose = await overlay({ ...compose }, overlayConfig || {}, this);
      } else {
        logger.warn(`overlay ${overlayName} does not exist`);
      }
    }
    let result: string | DockerCompose | undefined;
    switch (format) {
      case Format.Json: {
        result = JSON.stringify(compose, null, 2);
        break;
      }
      case Format.Yaml: {
        result = jsYaml.dump(compose);
        break;
      }
      case Format.Object: {
        result = compose;
        break;
      }
      default: {
        throw new Error(`unsupported format: ${format}`);
      }
    }
    return result;
  }

  async inspect(serviceName: string) {
    const service = await this.service(serviceName);
    if (!service) return;
    return this.dockerService.inspect({ name: serviceName });
  }

  async env(serviceName: string, key: string): Promise<string | undefined> {
    const service = await this.service(serviceName);
    if (!service) return;
    return (await this.inspect(serviceName))?.spec?.taskTemplate?.containerSpec?.env
      ?.map((env: string) => {
        const keyValuePair = env.split('=');
        return {
          key: keyValuePair[0],
          value: keyValuePair?.[1],
        };
      })
      .find(({ key: k }) => k === key)?.value;
  }

  async service(serviceName: string) {
    return (await this.compose())?.services?.[serviceName];
  }

  async exec(
    serviceName: string,
    command: string,
    args: string[] | undefined,
    options: Omit<ExecOptions, 'all'> & { all: true },
  ): Promise<DockerServiceExecResult[]>;
  async exec(
    serviceName: string,
    command: string,
    args?: string[],
    options?: Omit<ExecOptions, 'all'> & { all?: false },
  ): Promise<DockerServiceExecResult>;
  async exec(
    serviceName: string,
    command: string,
    args: string[] = [],
    options: ExecOptions = {},
  ): Promise<DockerServiceExecResult | DockerServiceExecResult[]> {
    const service = await this.service(serviceName);
    if (!service) {
      throw new GraphQLError(`service '${serviceName}' does not exist in app '${this.app.name}'`);
    }
    const result = await this.dockerService.exec({ name: serviceName, command, args, ...options });
    if (options.all) return result;
    return result?.[0] || { stdout: '', stderr: '', exitCode: 0 };
  }

  async getSafe(ignore: string[] = []): Promise<SafeInstaller> {
    const safe: SafeInstaller = {
      ...this.values,
      ...this.deployment,
      app: this.app,
      baseUrl: this.baseUrl,
      compose: this.compose.bind(this),
      env: this.env.bind(this),
      environment: this.environment,
      exec: this.exec.bind(this),
      host: this.host,
      hostname: this.hostname,
      inspect: this.inspect.bind(this),
      paths: this.paths,
      publishedPorts: this.publishedPorts,
      service: this.service.bind(this),
      stackName: this.stackName,
      values: this.values,
    };
    ignore.forEach((key) => delete safe[key]);
    return safe;
  }

  private async getSafePlugs(): Promise<Record<string, SafePlug>> {
    const plugs: Record<string, SafePlug> = {};
    for (const plugPath of this.paths.plugs) {
      let plugName: string | undefined;
      if (plugPath.endsWith('.plug.yaml')) plugName = plugPath.split('/').pop()?.slice(0, -10);
      if (plugName) {
        plugs[plugName] = await (
          await Plug.new(this.docker, this.dockerService, this.prisma, plugName, plugPath, this)
        ).getSafe();
      }
    }
    return plugs;
  }
}

export enum Format {
  Json = 'json',
  Object = 'object',
  Yaml = 'yaml',
}

export class TemplateError extends GraphQLError {
  _templateError = true;
}

export interface ExecOptions {
  all?: boolean;
  detach?: boolean;
  env?: string[];
  interactive?: boolean;
  privileged?: boolean;
  tty?: boolean;
  user?: string;
  workdir?: string;
}
