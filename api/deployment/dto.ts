/*
 *  File: /deployment/dto.ts
 *  Project: api
 *  File Created: 22-01-2024 14:20:53
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';
import { GraphQLJSON } from 'graphql-scalars';
import { PrismaDeployment, PrismaEnvironment, PrismaEnvironmentCreatevaluesInput } from '../generated/type-graphql';
import { EnvironmentAccess } from './environment';

@ArgsType()
export class InstallAppArgs {
  @Field(() => String, { nullable: false })
  name: string;

  @Field(() => AppReference)
  app: AppReference;

  @Field(() => String, { nullable: true })
  environment?: string;

  @Field(() => [GraphQLJSON], { nullable: true })
  values?: PrismaEnvironmentCreatevaluesInput;
}

@ArgsType()
export class RemoveAppArgs {
  @Field(() => String, { nullable: false })
  name: string;
}

@InputType()
export class AppReference {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  version?: string;

  @Field(() => String, { nullable: true })
  repository?: string;

  @Field(() => String, { nullable: true })
  url?: string;
}

@ObjectType()
export class InstallAppResult {
  @Field(() => PrismaDeployment)
  deployment: PrismaDeployment;

  @Field(() => PrismaEnvironment)
  environment: PrismaEnvironment;

  @Field(() => EnvironmentAccess)
  access: EnvironmentAccess;
}
