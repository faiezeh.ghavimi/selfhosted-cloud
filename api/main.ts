/*
 *  File: /main.ts
 *  Project: api
 *  File Created: 12-01-2024 05:15:21
 *  Author: Clay Risser
 *  -----
 *  BitSpur (c) Copyright 2021 - 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dotenv/config';
import fs from 'fs/promises';
import { config } from './config';
import { Logger, createServer } from '@multiplatform.one/typegraphql';
import { listenDockerEvents } from './docker';
import { options } from './server';

(async () => {
  await Promise.all(Object.values(config.dirs).map((dir) => fs.mkdir(dir, { recursive: true })));
  const server = await createServer(options, () => {
    listenDockerEvents(new Logger(options.logger));
  });
  await server.start();
})();
